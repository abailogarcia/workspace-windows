package clases;

import java.time.LocalDate;

/**
 * 
 * @author angel bailo
 *
 */
public class Academia {
	/**
	 * Creamos el array Curso
	 */
	private Curso[] misCursos;

	/**
	 * Construimos Academia con maxCursos
	 * 
	 * @param maxCursos
	 */
	public Academia(int maxCursos) {
		this.misCursos = new Curso[maxCursos];
	}

	/**
	 * M�todo para crear curso
	 * 
	 * @param codCurso
	 * @param nombre
	 * @param fechaInicio
	 * @param fechaFin
	 * @param duracion
	 * @param modalidad
	 * @param precioAlumno
	 */

	public void crearCurso(String codCurso, String nombre, LocalDate fechaInicio, LocalDate fechaFin, int duracion,
			boolean modalidad, double precioAlumno) {
		for (int i = 0; i < misCursos.length; i++) {
			if (misCursos[i] == null) {
				misCursos[i] = new Curso(codCurso);
				// misCursos[i].setCodCurso(codCurso);
				misCursos[i].setNombre(nombre);
				misCursos[i].setFechaInicio(fechaInicio);
				misCursos[i].setFechaFin(fechaFin);
				misCursos[i].setDuracion(duracion);
				misCursos[i].setModalidad(modalidad);
				misCursos[i].setPrecioAlumno(precioAlumno);
				break;
			}
		}
	}

	/**
	 * M�todo para crear curso
	 * 
	 * @param codCurso
	 * @param nombre
	 * @param fechaInicio
	 * @param fechaFin
	 * @param duracion
	 * @param modalidad
	 * @param precioAlumno
	 * @param inscritos
	 */
	public void crearCurso(String codCurso, String nombre, LocalDate fechaInicio, LocalDate fechaFin, int duracion,
			boolean modalidad, double precioAlumno, Alumno[] inscritos) {
		for (int i = 0; i < misCursos.length; i++) {
			if (misCursos[i] == null) {
				misCursos[i] = new Curso(codCurso);
				misCursos[i].setNombre(nombre);
				misCursos[i].setFechaInicio(fechaInicio);
				misCursos[i].setFechaFin(fechaFin);
				misCursos[i].setDuracion(duracion);
				misCursos[i].setModalidad(modalidad);
				misCursos[i].setPrecioAlumno(precioAlumno);
				misCursos[i].setInscritos(inscritos);
				break;
			}
		}
	}

	/**
	 * M�todo para listar curso
	 */
	public void listarCurso() {
		for (int i = 0; i < misCursos.length; i++) {
			if (misCursos[i] != null) {
				System.out.println(misCursos[i]);
			} else {
				System.out.println("No hay cursos");
			}
		}
	}

	/**
	 * M�todo para anular curso
	 * 
	 * @param codCurso
	 */
	public void borrarCurso(String codCurso) {
		for (int i = 0; i < misCursos.length; i++) {
			if (misCursos[i] != null) {
				if (misCursos[i].getCodCurso().equals(codCurso)) {
					misCursos[i] = null;
					System.out.println("Se ha eliminado el curso " + codCurso);
				} else {
					System.out.println("El curso no existe");
				}
			} else {
				System.out.println("No hay cursos");
			}
		}
	}

	/**
	 * M�todo para buscar curso
	 * 
	 * @param codCurso
	 * @return devuelve un objeto Curso
	 */
	public Curso buscarCurso(String codCurso) {
		for (int i = 0; i < misCursos.length; i++) {
			if (misCursos[i] != null) {
				if (misCursos[i].getCodCurso().equals(codCurso)) {
					return misCursos[i];
				}
			}
		}
		return null;
	}

	
	/**
	 * M�todo para inscribir alumno en curso
	 * @param alumno
	 */
	public void inscribirAlumnoCurso(Alumno[] alumno) {
		for (int i = 0; i < misCursos.length; i++) {
			if (misCursos[i] == null) {
				misCursos[i].setInscritos(alumno);
				// System.out.println("Se ha inscrito al alumno " + alumno[i].getCodAlumno());
				break;
			}
		}

	}
	/**
	 * M�todo para listar los alumnos de un curso
	 * @param codCurso
	 */
	public void listarAlumnosPorCurso(String codCurso) {
		for (int i = 0; i < misCursos.length; i++) {
			if (misCursos[i] != null) {
				if (misCursos[i].getCodCurso().equals(codCurso)) {
					System.out.println(misCursos[i]);
				} else {
					System.out.println("El curso no existe");
				}
			} else {
				System.out.println("No hay ning�n curso");
			}
		}
	}

	/**
	 * Modificar duraci�n curso
	 * @param codCurso
	 * @param duracion
	 */
	public void modificarDuracionCurso(String codCurso, int duracion) {
		for (int i = 0; i < misCursos.length; i++) {
			if (misCursos[i] != null) {
				if (misCursos[i].getCodCurso().equals(codCurso)) {
					misCursos[i].setDuracion(duracion);
					System.out.println(
							"Se ha modificado la duraci�n del curso " + codCurso + " a " + duracion + " horas");
				} else {
					System.out.println("El curso no existe");
				}
			} else {
				System.out.println("No hay cursos");
			}
		}
	}

	/**
	 * comprobar si hay cursos dados de alta
	 * 
	 * @return si no hay cursos devuelve 0
	 */
	public int comprobarCurso() {
		int contador = misCursos.length;
		for (int i = misCursos.length - 1; i >= 0; i--) {
			if (misCursos[i] == null) {
				contador--;
			}
		}
		return contador;
	}

}
