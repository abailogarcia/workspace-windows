package clases;
//NO USAR
public class Profesor {
	//atributos
		private String nombre;
		private String apellidos;
		private String mail;
		private String telefono;
	//constructores
		public Profesor(String nombre, String apellidos, String mail, String telefono) {
			this.nombre = nombre;
			this.apellidos = apellidos;
			this.mail = mail;
			this.telefono = telefono;
		}
		public Profesor(String nombre, String apellidos) {
			this.nombre = nombre;
			this.apellidos = apellidos;
		}
		public Profesor(String nombre, String apellidos, String mail) {
			this.nombre = nombre;
			this.apellidos = apellidos;
			this.mail = mail;
		}
		public Profesor() {
		}
	//setter y getter
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getApellidos() {
			return apellidos;
		}
		public void setApellidos(String apellidos) {
			this.apellidos = apellidos;
		}
		public String getMail() {
			return mail;
		}
		public void setMail(String mail) {
			this.mail = mail;
		}
		public String getTelefono() {
			return telefono;
		}
		public void setTelefono(String telefono) {
			this.telefono = telefono;
		}
	//m�todos
		//crear profesor
		public void altaProfesor(String nombre, String apellidos, String mail, String telefono) {
			this.nombre = nombre;
			this.apellidos =apellidos;
			this.mail = mail;
			this.telefono=telefono;
		}
		//modificar profesor
		
		
		//borrar profesor
		
		//listar profesor
		@Override
		public String toString() {
			return "Datos del Profesor; \nnombre=" + nombre + "  apellidos=" + apellidos + "\nmail=" + mail + "\ntelefono="
					+ telefono;
		}
}
