package clases;

import java.time.LocalDate;
import java.util.Arrays;

/**
 * 
 * @author angel bailo
 *
 */
public class Curso {

	private String codCurso;
	private String nombre;
	private LocalDate fechaInicio;
	private LocalDate fechaFin;
	private int duracion;
	private boolean modalidad;
	private Alumno[] inscritos;
	private double precioAlumno;

	/**
	 * Constructor con el que inicializamos el objeto inscritos con el maxAlumnos
	 * 
	 * @param maxAlumnos
	 */
	public Curso(int maxAlumnos) {
		this.inscritos = new Alumno[maxAlumnos];
	}

	/**
	 * Constructor con par�metros codCurso, nombre, fechaInicio, fechaFin, duracion,
	 * modalidad, inscritos, precioAlumno
	 * 
	 * @param codCurso
	 * @param nombre
	 * @param fechaInicio
	 * @param fechaFin
	 * @param duracion
	 * @param modalidad
	 * @param inscritos
	 * @param precioAlumno
	 */

	public Curso(String codCurso, String nombre, LocalDate fechaInicio, LocalDate fechaFin, int duracion,
			boolean modalidad, Alumno[] inscritos, double precioAlumno) {
		this.codCurso = codCurso;
		this.nombre = nombre;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.duracion = duracion;
		this.modalidad = modalidad;
		this.inscritos = inscritos;
		// this.profe = profe;
		this.precioAlumno = precioAlumno;
	}

	/**
	 * Constructor con par�metro codCurso
	 * 
	 * @param codCurso
	 */
	public Curso(String codCurso) {
		this.codCurso = codCurso;
	}

	/**
	 * setter y getter
	 * 
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}

	public String getCodCurso() {
		return codCurso;
	}

	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}

	/*
	 * public Profesor getProfe() { return profe; } public void setProfe(Profesor
	 * profe) { this.profe = profe; }
	 */
	public double getPrecioAlumno() {
		return precioAlumno;
	}

	public void setPrecioAlumno(double precioAlumno) {
		this.precioAlumno = precioAlumno;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(LocalDate fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public LocalDate getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(LocalDate fechaFin) {
		this.fechaFin = fechaFin;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public boolean isModalidad() {
		return modalidad;
	}

	public void setModalidad(boolean modalidad) {
		this.modalidad = modalidad;
	}

	public Alumno[] getInscritos() {
		return inscritos;
	}

	public void setInscritos(Alumno[] inscritos) {
		this.inscritos = inscritos;
	}

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return "Curso c�digo= " + codCurso + "\nnombre=" + nombre + "\nfechaInicio=" + fechaInicio + "\nfechaFin="
				+ fechaFin + "\nduracion=" + duracion + "\nmodalidad=" + modalidad + "\ninscritos="
				+ Arrays.toString(inscritos) + "\nprecioAlumno=" + precioAlumno;
	}

	/**
	 * M�todo para dar de alta un alumno con los siguientes par�metros y devuelve un
	 * objeto alumno
	 * 
	 * @param codAlumno
	 * @param nombre
	 * @param apellidos
	 * @param mail
	 * @param telefono
	 * @return devuelve objeto alumno
	 */

	public Alumno altaAlumno(int codAlumno, String nombre, String apellidos, String mail, String telefono) {
		for (int i = 0; i < inscritos.length; i++) {
			if (inscritos[i] == null) {
				return inscritos[i] = new Alumno(codAlumno, nombre, apellidos, mail, telefono);
			}
			break;
		}
		return null;
	}

	/**
	 * M�todo para crear alumno
	 * 
	 * @param codAlumno
	 * @param nombre
	 * @param apellidos
	 * @param mail
	 * @param telefono
	 */
	public void altaAlumnoCrea(int codAlumno, String nombre, String apellidos, String mail, String telefono) {
		for (int i = 0; i < inscritos.length; i++) {
			if (inscritos[i] == null) {
				inscritos[i] = new Alumno(codAlumno, nombre, apellidos, mail, telefono);
				System.out.println(
						"Se cre� el alumno " + inscritos[i].getCodAlumno() + " " + inscritos[i].getApellidos());
				break;
			}
		}
	}

	/**
	 * M�todo para modificar nombre alumno
	 * 
	 * @param codAlumno
	 * @param mail
	 */

	public void cambiaMailAlumno(int codAlumno, String mail) {
		for (int i = 0; i < inscritos.length; i++) {
			if (inscritos[i] != null) {
				if (inscritos[i].getCodAlumno() == codAlumno) {
					inscritos[i].setMail(mail);
				} else {
					System.out.println("No existe ese alumno");
				}
			}
		}
		int contador = 0;
		for (int i = 0; i < inscritos.length; i++) {
			if (inscritos[i] == null) {
				contador++;
			}
		}
		if (contador == inscritos.length) {
			System.out.println("No hay alumnos");
		}
	}

	/**
	 * M�todo para buscar alumno
	 * 
	 * @param codAlumno
	 * @return
	 */

	public Alumno buscarAlumno(int codAlumno) {
		for (int i = 0; i < inscritos.length; i++) {
			if (inscritos[i] != null) {
				if (inscritos[i].getCodAlumno() == codAlumno) {
					return inscritos[i];
				} else {
					return null;
				}
			}
		}
		return null;
	}

	/**
	 * M�todo para borrar un alumno
	 * 
	 * @param codAlumno
	 */

	public void bajaAlumno(int codAlumno) {
		for (int i = 0; i < inscritos.length; i++) {
			if (inscritos[i] != null) {
				if (inscritos[i].getCodAlumno() == codAlumno) {
					inscritos[i] = null;
					System.out.println("Se ha borrado el alumno " + codAlumno);
				} else {
					System.out.println("No existe ese alumno");
				}
			}
		}
	}

	/**
	 * M�todo para listar alumnos. Creo un contador para que si no hay alumnos me
	 * diga que no hay ninguno
	 */

	public void listarAlumnos() {
		for (int i = 0; i < inscritos.length; i++) {
			if (inscritos[i] != null) {
				System.out.println(inscritos[i]);
			}
		}
		int contador = 0;
		for (int i = 0; i < inscritos.length; i++) {
			if (inscritos[i] == null) {
				contador++;
			}
		}
		if (contador == inscritos.length) {
			System.out.println("No hay alumnos");
		}
	}

	/**
	 * comprobar si hay alumnos dados de alta
	 * 
	 * @return si no hay alumnos devuelve 0
	 */
	public int comprobarAlumnos() {
		int contador = inscritos.length;
		for (int i = inscritos.length - 1; i >= 0; i--) {
			if (inscritos[i] == null) {
				contador--;
			}
		}
		return contador;
	}

}
