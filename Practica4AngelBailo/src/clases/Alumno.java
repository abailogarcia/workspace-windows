package clases;

/**
 * 
 * @author angel bailo
 *
 */
public class Alumno {

	/**
	 * atributos
	 */
	private int codAlumno;
	private String nombre;
	private String apellidos;
	private String mail;
	private String telefono;

	/**
	 * Constructor con parámetros codAlumno, nombre, apellidos, mail, telefono
	 * 
	 * @param codAlumno
	 * @param nombre
	 * @param apellidos
	 * @param mail
	 * @param telefono
	 */
	public Alumno(int codAlumno, String nombre, String apellidos, String mail, String telefono) {
		this.codAlumno = codAlumno;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.mail = mail;
		this.telefono = telefono;
	}

	/**
	 * Constructor con parámetros nombre, apellidos
	 * 
	 * @param nombre
	 * @param apellidos
	 */
	public Alumno(String nombre, String apellidos) {
		this.nombre = nombre;
		this.apellidos = apellidos;
	}

	/**
	 * Constructor con parámetros nombre, apellidos, mail
	 * 
	 * @param nombre
	 * @param apellidos
	 * @param mail
	 */
	public Alumno(String nombre, String apellidos, String mail) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.mail = mail;
	}

	/**
	 * Constructor sin parámetros
	 */
	public Alumno() {
	}

	/**
	 * setter y getter
	 * 
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getCodAlumno() {
		return codAlumno;
	}

	public void setCodAlumno(int codAlumno) {
		this.codAlumno = codAlumno;
	}

	/**
	 * método para crear alumno
	 * 
	 * @param nombre
	 * @param apellidos
	 * @param mail
	 * @param telefono
	 */

	public void altaAlumno(String nombre, String apellidos, String mail, String telefono) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.mail = mail;
		this.telefono = telefono;
	}

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return "Datos del Alumno: \ncodigo=" + codAlumno + "\nnombre=" + nombre + " apellidos=" + apellidos + "\nmail="
				+ mail + "\ntelefono=" + telefono;
	}

}
