package programa;

import java.time.LocalDate;
import java.util.Scanner;

import clases.Academia;
import clases.Alumno;
import clases.Curso;

/**
 * @Override
 * @author angel bailo
 *
 */
public class main {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		/**
		 * Creo un �nico curso con 2 alumnos para facilitar la entrada de datos
		 */
		Curso alumnos = new Curso(2);
		Academia curso1 = new Academia(1);
		/**
		 * Creaci�n de men� con while y switch
		 */
		boolean salir = false;
		int opcion;
		while (!salir) {

			System.out.println("1.- Crear curso");
			System.out.println("2.- Crear alumno");
			System.out.println("3.- Listar curso");
			System.out.println("4.- Listar  alumnos");
			/*
			 * Decidimos no hacer esta parte porque se excede de la pr�ctica
			 * System.out.println("5.- Listar alumnos por curso");
			 */
			System.out.println("6.- Borrar alumno");
			System.out.println("7.- Borrar curso");
			System.out.println("8.- Modificar mail alumno");
			System.out.println("9.- Modificar la duraci�n del curso");
			System.out.println("10.- Salir");
			System.out.println("Selecciona una de las opciones");
			opcion = input.nextInt();
			input.nextLine();
			switch (opcion) {
			case 1:
				System.out.println("Introduce el c�digo del curso alfanum�rico");
				String codCurso = input.nextLine();
				System.out.println("Introduce el nombre del curso");
				String nombre = input.nextLine();
				System.out.println("Introduce la fecha de inicio del curso");
				String fechaInicio = input.nextLine();
				System.out.println("Introduce la fecha de fin del curso");
				String fechaFin = input.nextLine();
				System.out.println("Introduce la duraci�n del curso en horas");
				int duracion = input.nextInt();
				input.nextLine();
				System.out.println("Introduce true para presencial y false para online");
				boolean modalidad = input.nextBoolean();
				System.out.println("Introduce el precio por alumno");
				double precio = input.nextDouble();
				curso1.crearCurso(codCurso, nombre, LocalDate.parse(fechaInicio), LocalDate.parse(fechaFin), duracion,
						modalidad, precio);
				curso1.listarCurso();
				break;
			case 2:
				System.out.println("Introduce el c�digo del alumno");
				int codAlumno = input.nextInt();
				input.nextLine();
				System.out.println("Introduce el nombre del alumno");
				String nombreAlumno = input.nextLine();
				System.out.println("Introduce los apellidos");
				String apellidos = input.nextLine();
				System.out.println("Introduce el mail del alumno");
				String mail = input.nextLine();
				System.out.println("Intrroduce el tel�fono del alumno");
				String telefono = input.nextLine();
				alumnos.altaAlumnoCrea(codAlumno, nombreAlumno, apellidos, mail, telefono);
				/* esto no funciona
				curso1.inscribirAlumnoCurso(alumnos.altaAlumno(codAlumno, nombreAlumno,
				apellidos, mail, telefono));*/

				alumnos.listarAlumnos();
				// me falla inscribir los alumnos en el curso
				break;
			case 3:
				curso1.listarCurso();
				break;
			case 4:
				alumnos.listarAlumnos();
				break;
			case 5:
				/*
				 * System.out.
				 * println("Introduce el c�digo del curso del que quieres listar los alumnos");
				 * String codigoCurso= input.nextLine();
				 * curso1.listarAlumnosPorCurso(codigoCurso);
				 */
				System.out.println("Introduce otra opci�n");
				break;
			case 6:
				if (alumnos.comprobarAlumnos() > 0) {
					System.out.println("Introduce el c�digo del alumno a borrar");
					int codAlumnoBorrar = input.nextInt();
					if (alumnos.buscarAlumno(codAlumnoBorrar) != null) {
						alumnos.bajaAlumno(codAlumnoBorrar);
					} else {
						System.out.println("Ese alumno no existe");
					}
				} else {
					System.out.println("No hay alumnos");
				}
				break;
			case 7:
				if (curso1.comprobarCurso() > 0) {
					System.out.println("Introduce el c�digo del curso a borrar");
					String codCursoBorrar = input.nextLine();
					curso1.borrarCurso(codCursoBorrar);
				} else {
					System.out.println("No hay cursos");
				}
				break;
			case 8:
				if (alumnos.comprobarAlumnos() > 0) {
					System.out.println("Introduce el c�digo del alumno a modificar");
					int codigoAlumno = input.nextInt();
					System.out.println("Introduce su nuevo mail");
					input.nextLine();
					String nuevoMail = input.nextLine();
					alumnos.cambiaMailAlumno(codigoAlumno, nuevoMail);
					if (alumnos.buscarAlumno(codigoAlumno) != null) {
						alumnos.listarAlumnos();
					}
				} else {
					System.out.println("No hay alumnos");
				}
				break;
			case 9:
				if (curso1.comprobarCurso() > 0) {
					System.out.println("Introduce el c�digo del curso a modificar");
					String codigoCursoModificar = input.nextLine();
					System.out.println("Introduce la nueva duraci�n del curso");
					int nuevaDuracion = input.nextInt();
					curso1.modificarDuracionCurso(codigoCursoModificar, nuevaDuracion);
					curso1.listarCurso();
				} else {
					System.out.println("No hay cursos");
				}
				break;
			case 10:
				salir = true;
				System.out.println("Se acab�");
				break;
			default:
				System.out.println("Solo n�meros entre 1 y 10");

			}

		}

		input.close();
	}

}
