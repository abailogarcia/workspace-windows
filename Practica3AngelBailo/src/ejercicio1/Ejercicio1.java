package ejercicio1;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		// Crear un programa que me pide un pueblo y nos muestra su
		// comarca.
		// a) (0.5 ptos) El programa leer� mediante teclado un pueblo como String.
		// b) (0.5 ptos) El programa crear� un m�todo mostrarComarca que recibir� un
		// String con el
		// pueblo le�do.
		// c) (1 pto) El programa mostrar� la comarca de cada pueblo.
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un pueblo");
		String pueblo = input.nextLine().toLowerCase();
		comarcaPueblo(pueblo);

		input.close();

	}

	private static void comarcaPueblo(String pueblo) {
		switch (pueblo) {
		case "biota":
			
		case "tauste":
			System.out.println("El pueblo " + pueblo + " pertenece a la comarca Ejea de los Caballeros");
			break;
		case "fabara":
			
		case "anento":
			
		case "gallocanta":
			System.out.println("El pueblo " + pueblo + " pertenece a la comarca Daroca");
			break;
		case "calatorao":
			
		case "codos":
			System.out.println("El pueblo " + pueblo + " pertenece a la comarca La Almunia de Do�a Godina");
			break;
		case "gallur":
			
		case "tarazona":
			System.out.println("El pueblo " + pueblo + " pertenece a la comarca Comarca Borja");
			break;
		case "cadrete":
		
		case "pedrola":
			
		case "quinto":
			
		case "zuera":
			System.out.println("El pueblo " + pueblo + " pertenece a la comarca Zaragoza");
			break;
		case "ateca":
			
		case "calatayud":
			
		case "jaraba":
			System.out.println("El pueblo " + pueblo + " pertenece a la comarca Calatayud");
			break;
		default:
			System.out.println("El pueblo no coincide con nuestros datos");
			break;

		}
	}

}
