package ejercicio2;

public class Ejercicio2 {

	public static void main(String[] args) {
		// Crear un programa que me muestre un listado de
		// pueblos y sus comarcas.
		// a) (0.5 ptos) El programa crear� un vector de String pueblos. El programa
		// crear� un
		// vector de String comarcas. Ambos del mismo tama�o.�
		// pueblos = "Biota","Tauste","Gallur","Tarazona","Trasmoz",
		// "Ateca","Jaraba","Anento","Gallocanta","Belchite","Cadrete",
		// "Gelsa"
		// comarcas ="Ejea de los Caballeros","Ejea de los Caballeros",
		// "Borja","Borja","Borja","Calatayud","Calatayud","Daroca",
		// "Daroca","Zaragoza","Zaragoza","Zaragoza"
		// b) (1 pto) El programa crear� un m�todo mostrarPueblos que recibir� los dos
		// vectores
		// creados e imprimir� cada pueblo con su comarca.
		// c) (0.5 ptos) El main llamar� al m�todo mostrarPueblos
		// String[] pueblos = new String[12];
		String[] pueblos = { "Biota", "Tauste", "Gallur", "Tarazona", "Trasmoz", "Ateca", "Jaraba", "Anento",
				"Gallocanta", "Belchite", "Cadrete", "Gelsa" };
		String[] comarcas = { "Ejea de los Caballeros", "Ejea de los Caballeros", "Borja", "Borja", "Borja",
				"Calatayud", "Calatayud", "Daroca", "Daroca", "Zaragoza", "Zaragoza", "Zaragoza" };
		mostrarComarca(pueblos, comarcas);

	}

	private static void mostrarComarca(String[] pueblo, String[] comarca) {
		for (int i = 0; i < pueblo.length; i++) {
			System.out.print("El pueblo " + pueblo[i] + " pertenece a la comarca " + comarca[i]);
			System.out.println();
		}

	}

}
