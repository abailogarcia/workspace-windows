package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		// Crear un programa que me muestre si un pueblo est�
		// dentro de un vector.
		// a) (1 pto) El programa crear� un vector de pueblos y solicitar� una por
		// teclado, que ser�
		// el pueblo que queremos encontrar en el vector.
		// b) (1 pto) El programa crear� un m�todo encontrado que recibir� un pueblo y
		// un vector.
		// El m�todo devolver� un booleano que indica si la palabra est� encontrada.
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un pueblo");
		String pueblo = input.nextLine().toLowerCase();
		String[] pueblos = { "biota", "tauste", "gallur", "tarazona", "trasmoz", "ateca", "jaraba", "anento",
				"gallocanta", "belchite", "cadrete", "Gelsa" };
		boolean comprueba = compruebaPueblo(pueblo, pueblos);
		if (comprueba) {
			System.out.println("El pueblo est� dentro del array");
		} else {
			System.out.println("El pueblo no est� dentro del array");
		}
		input.close();
	}

	private static boolean compruebaPueblo(String pueblo, String[] pueblos) {
		boolean cierto = false;
		for (int i = 0; i < pueblos.length; i++) {
			if (pueblos[i].equals(pueblo)) {
				cierto = true;
			}
		}
		return cierto;
	}
}
