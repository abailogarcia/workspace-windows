package ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Crear un programa que lea un vector de habitantes
		// de pueblos de tipo int y calcule la suma, el m�ximo, m�nimo y media de
		// habitantes.
		// a) (1 pto) El programa crear� un vector de int de tama�o 10 que se
		// introducir�n
		// mediante un m�todo llamado leerHabitantes.
		// b) (1 pto) El programa llamar� a un m�todo mediaHabitantes que recibir� el
		// vector de
		// habitantes le�do y calcular� su media.
		// c) (1 pto) El programa llamar� a un m�todo maximoHabitantes que recibir� el
		// vector de
		// habitantes le�do y calcular� el m�ximo.
		// d) (1 pto) El programa llamar� a un m�todo minimoHabitantes que recibir� el
		// vector de
		// habitantes le�do y calcular� su minimo
		int[] poblacion = new int[10];
		poblacion = leerHabitantes(poblacion);
		System.out.println("La suma de habitantes es: " + sumaHabitantes(poblacion));
		System.out.println("La media de habitantes es: " + mediaHabitantes(poblacion));
		System.out.println("El m�ximo de habitantes es: " + maximoHabitantes(poblacion));
		System.out.println("El m�nimo de habitantes es: " + minimoHabitantes(poblacion));
		input.close();
	}

	private static int[] leerHabitantes(int[] poblacion) {
		for (int i = 0; i < poblacion.length; i++) {
			System.out.println("Introduce una poblaci�n");
			poblacion[i] = input.nextInt();
		}
		return poblacion;
	}

	private static int sumaHabitantes(int[] poblacion) {
		int suma = 0;
		for (int i = 0; i < poblacion.length; i++) {
			suma += poblacion[i];
		}
		return suma;
	}

	private static double mediaHabitantes(int[] poblacion) {
		double media = sumaHabitantes(poblacion) / poblacion.length;
		return media;
	}

	private static int maximoHabitantes(int[] poblacion) {
		int maximo = poblacion[0];
		for (int i = 0; i < poblacion.length; i++) {
			if (poblacion[i] > maximo) {
				maximo = poblacion[i];
			}
		}
		return maximo;
	}

	private static int minimoHabitantes(int[] poblacion) {
		int minimo = poblacion[0];
		for (int i = 0; i < poblacion.length; i++) {
			if (poblacion[i] < minimo) {
				minimo = poblacion[i];
			}
		}
		return minimo;
	}

}
