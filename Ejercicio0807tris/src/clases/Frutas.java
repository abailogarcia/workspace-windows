package clases;

public class Frutas {
	//atributos
	private String codigoFruta;
	private String tipoFruta;
	double precio;
	String fruteria;
	Proveedor proveedor;
	
	public Frutas(String codigoFruta) {
		this.codigoFruta=codigoFruta;
	}

	/**
	 * @return the codigoFruta
	 */
	public String getCodigoFruta() {
		return codigoFruta;
	}

	/**
	 * @param codigoFruta the codigoFruta to set
	 */
	public void setCodigoFruta(String codigoFruta) {
		this.codigoFruta = codigoFruta;
	}

	/**
	 * @return the tipoFruta
	 */
	public String getTipoFruta() {
		return tipoFruta;
	}

	/**
	 * @param tipoFruta the tipoFruta to set
	 */
	public void setTipoFruta(String tipoFruta) {
		this.tipoFruta = tipoFruta;
	}

	/**
	 * @return the precio
	 */
	public double getPrecio() {
		return precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(double precio) {
		this.precio = precio;
	}

	/**
	 * @return the fruteria
	 */
	public String getFruteria() {
		return fruteria;
	}

	/**
	 * @param fruteria the fruteria to set
	 */
	public void setFruteria(String fruteria) {
		this.fruteria = fruteria;
	}

	/**
	 * @return the proveedor
	 */
	public Proveedor getProveedor() {
		return proveedor;
	}

	/**
	 * @param proveedor the proveedor to set
	 */
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Frutas [codigoFruta=" + codigoFruta + ", tipoFruta=" + tipoFruta + ", precio=" + precio + ", fruteria="
				+ fruteria + ", proveedor=" + proveedor + "]";
	}
	
}
