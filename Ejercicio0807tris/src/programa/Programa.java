package programa;

import clases.Fruteria;
import clases.Proveedor;

public class Programa {

	public static void main(String[] args) {
		Fruteria miFruteria = new Fruteria(4,3);
		Proveedor proveedor1=new Proveedor("Proveedor frutas");
		proveedor1.setTelefono("64646464");
		Proveedor proveedor2=new Proveedor("Proveedor verduras");
		proveedor2.setTelefono("96476141");
		
		miFruteria.altaFruta("SDF", "naranja", 12.3, "fruteria1", proveedor1);
		miFruteria.altaVerdura("BBBB", "Brocoli", 0.2, "Fruteria1", proveedor2);
	}

}
