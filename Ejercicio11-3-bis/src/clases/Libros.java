package clases;

import java.io.Serializable;

public class Libros extends Articulos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public final static int PUNTOS = 5;
	private String autor;
	private String editorial;
	
	public Libros(String isbn, String titulo, String autor, String editorial) {
		super(isbn, titulo);
		this.autor = autor;
		this.editorial = editorial;
	}
	
	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}


	@Override
	public int compareTo(Articulos arg0) {
			//	return getIsbn().compareTo(arg0.getIsbn());
		return this.getIsbn().compareTo(arg0.getIsbn());
	}

	@Override
	public int calcularPuntos() {
		int totalPuntos=0;
		totalPuntos+=PUNTOS;
		return totalPuntos;
	}

	@Override
	public String toString() {
		return "Libros [autor=" + autor + ", editorial=" + editorial + ", toString()=" + super.toString() + "]";
	}

	

}
