package clases;

import java.io.Serializable;
import java.time.LocalDate;

public class Socios implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idSocio;
	private String nombre;
	private LocalDate fechaAlta;
	
	public Socios(int idSocio, String nombre, LocalDate fechaAlta) {
		this.idSocio = idSocio;
		this.nombre = nombre;
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the idSocio
	 */
	public int getIdSocio() {
		return idSocio;
	}

	/**
	 * @param idSocio the idSocio to set
	 */
	public void setIdSocio(int idSocio) {
		this.idSocio = idSocio;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the fechaAlta
	 */
	public LocalDate getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta the fechaAlta to set
	 */
	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Socios [idSocio=" + idSocio + ", nombre=" + nombre + ", fechaAlta=" + fechaAlta + "]";
	}
	
}

