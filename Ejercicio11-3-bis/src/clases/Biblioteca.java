package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class Biblioteca implements Serializable {

	private static final long serialVersionUID = -1910194051487995434L;
	public static final int DIAS_PRESTAMO = 15;

	private ArrayList<Socios> listaSocios;
	private ArrayList<Articulos> listaArticulos;
	private ArrayList<Prestamos> listaPrestamos;

	public Biblioteca() {
		this.listaSocios = new ArrayList<Socios>();
		this.listaArticulos = new ArrayList<Articulos>();
		this.listaPrestamos = new ArrayList<Prestamos>();
	}

	public void altaSocio(String nombre) {
		listaSocios.add(new Socios(listaSocios.size() + 1, nombre, LocalDate.now()));
	}

	public void altaArticulo(String isbn, String titulo, String autor, String editorial) {
		listaArticulos.add(new Libros(isbn, titulo, autor, editorial));
		Collections.sort(listaArticulos);
	}

	public void altaArticulo(String isbn, String titulo, String editorial, boolean online) {
		listaArticulos.add(new Revistas(isbn, titulo, editorial, online));
		Collections.sort(listaArticulos);
	}

	public void listarArticulos() {
		System.out.println("Articulos: ");
		for (Articulos articulo : listaArticulos) {
			System.out.println(articulo);
		}
	}

	public boolean socioExiste(int idSocio) {
		for (Socios socio : listaSocios) {
			if (socio.getIdSocio() == idSocio) {
				return true;
			}
		}
		return false;
	}

	public Socios devuelveSocio(int idSocio) {
		for (Socios socio : listaSocios) {
			if (socio.getIdSocio() == idSocio) {
				return socio;
			}
		}
		return null;
	}

	public void altaPrestamo(int idSocio, String isbn, int idPrestamo) {
		if (socioExiste(idSocio)) {
			if (articuloExiste(isbn))
				listaPrestamos.add(new Prestamos(listaPrestamos.size() + 1, LocalDate.now(),
						LocalDate.now().plusDays(DIAS_PRESTAMO), devuelveSocio(idSocio)));
			buscarPrestamo(idPrestamo).listaArticulos.add(devuelveArticulo(isbn));
		}

	}

	public Prestamos buscarPrestamo(int idPrestamo) {
		for (Prestamos prestamo : listaPrestamos) {
			if (prestamo.getIdPrestamo() == idPrestamo) {
				return prestamo;
			}
		}
		return null;
	}

	public void listarPrestamos() {
		for (Prestamos prestamo : listaPrestamos) {
			System.out.println(prestamo);
		}
	}

	public boolean articuloExiste(String isbn) {
		for (Articulos articulo : listaArticulos) {
			if (articulo.getIsbn().equals(isbn)) {
				return true;
			}
		}
		return false;
	}

	public Articulos devuelveArticulo(String isbn) {
		for (Articulos articulo : listaArticulos) {
			if (articulo.getIsbn().equals(isbn)) {
				return articulo;
			}
		}
		return null;
	}

	public void mostarPrestamosSocio(int idSocio) {
		for (Prestamos prestamo : listaPrestamos) {
			if (prestamo.getSocio().equals(devuelveSocio(idSocio))) {
				System.out.println(prestamo);
			}
		}
	}

	public void introducirArticuloPrestamo(int idPrestamo, String isbn) {
		if(buscarPrestamo(idPrestamo)!=null) {
			if(articuloExiste(isbn)) {
				buscarPrestamo(idPrestamo).getListaArticulos().add(devuelveArticulo(isbn));
			}else {
				System.out.println("El articulo no existe");
		}
		}else {
			System.out.println("El prestamo no existe");
		}
	}
	

	// cargar datos
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream escritor = new ObjectInputStream(new FileInputStream(new File("src/datos.dat")));
			listaPrestamos = (ArrayList<Prestamos>) escritor.readObject();
			listaArticulos = (ArrayList<Articulos>) escritor.readObject();
			listaSocios = (ArrayList<Socios>) escritor.readObject();
			escritor.close();
		} catch (IOException e) {
			System.out.println("Error de entrada/salida");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	// guardar datos
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datos.dat")));
			escritor.writeObject(listaArticulos);
			escritor.writeObject(listaPrestamos);
			escritor.writeObject(listaSocios);
			escritor.close();
		} catch (IOException e) {
			System.out.println("Error de entrada/salida");
		}
	}
}
