package ejercicioprevio4;

public class Principal {

	public static void main(String[] args) {
		// creamos un ordenador
		Ordenador miOrdenador = new Ordenador();
		//mostramos los valores de los atributos
		System.out.println("Modelo procesador " +miOrdenador.modeloProcesador);
		System.out.println("Tipo memoria " +miOrdenador.tipoMemoria);
		System.out.println("Tama�o disco duro" +miOrdenador.tamDiscoDuro);
		System.out.println("Tama�o memoria  " +miOrdenador.tamMemoria);
		//cambiamos los valores de los atributos
		System.out.print("Cambio tama�o memoria ");
		miOrdenador.cambiarTama�oMemoria(25);
		System.out.print("Cambio tipo memoria ");
		miOrdenador.cambiarTipoMemoria("asdfas68");
		System.out.print("Cambio tama�o disco duro ");
		miOrdenador.cambiarTamDiscoDuro(354254);
		System.out.print("cambio modelo procesador ");
		miOrdenador.cambiarModeloProcesador("sdfgs98fg7");

	}

}
