package ejercicioprevio4;

public class Ordenador {
	//atributos
	int tamMemoria;
	String tipoMemoria;
	int tamDiscoDuro;
	String modeloProcesador;
	//constructor
	public Ordenador() {
		tamMemoria=63;
		tipoMemoria="ddr4";
		tamDiscoDuro=1500;
		modeloProcesador="ADF453";
		
	}
	//métodos
	public void cambiarTamañoMemoria(int tamMemoria1) {
		tamMemoria=tamMemoria1;
		System.out.println(tamMemoria);
	}
	public void cambiarTipoMemoria(String tipoMemoria1) {
		tipoMemoria=tipoMemoria1;
		System.out.println(tipoMemoria);
	}
	public void cambiarTamDiscoDuro(int tamDiscoDuro1) {
		tamDiscoDuro=tamDiscoDuro1;
		System.out.println(tamDiscoDuro);
	}
	public void cambiarModeloProcesador(String modeloProcesador1) {
		modeloProcesador=modeloProcesador1;
		System.out.println(modeloProcesador);
	}
}
