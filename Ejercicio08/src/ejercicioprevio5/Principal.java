package ejercicioprevio5;

public class Principal {

	public static void main(String[] args) {
		// Creamos un coche
		Coche miCoche =new Coche();
		//mostramos los valores de los atributos
		System.out.println("El modelo es " + miCoche.modelo);
		System.out.println("La marca es " + miCoche.marca);
		System.out.println("La velocidad m�xima es " + miCoche.velocidad);
		System.out.println("El tama�o de rueda es " + miCoche.tamRueda);
		//cambiamos los valores de los atributos
		System.out.print("Cambio el modelo ");
		miCoche.cambiarModelo("Octavia");
		System.out.print("Cambio la marca ");
		miCoche.cambiarMarca("Skoda");
		System.out.print("Cambio la velocidad ");
		miCoche.cambiarVelocidad(230);
		System.out.print("Cambio la rueda ");
		miCoche.cambiarTamRueda(225.4);

	}

}
