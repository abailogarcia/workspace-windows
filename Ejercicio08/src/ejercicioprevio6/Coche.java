package ejercicioprevio6;

public class Coche {
	//atributos
	String marca;
	String modelo;
	int velocidad;
	double tamRueda;
	//getter and setter
	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}

	public double getTamRueda() {
		return tamRueda;
	}

	public void setTamRueda(double tamRueda) {
		this.tamRueda = tamRueda;
	}
	//constructor
	public Coche(String marca, String modelo, int velocidad, double tamRueda) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.velocidad = velocidad;
		this.tamRueda = tamRueda;
	}
	public Coche() {
		this.marca = "Renault";
		this.modelo = "Laguna";
		this.velocidad= 216;
		this.tamRueda=205.5;
	}
	//m�todos para cambiar los atributos

	public void cambiarMarca(String marca1)  {
		marca =marca1;
		System.out.println(marca);
	}
	public void cambiarModelo(String modelo1)  {
		modelo =modelo1;
		System.out.println(modelo);
	}
	public void cambiarVelocidad(int velocidad1)  {
		velocidad =velocidad1;
		System.out.println(velocidad);
	}
	public void cambiarTamRueda(double tamRueda1)  {
		tamRueda =tamRueda1;
		System.out.println(tamRueda);
	}
}
