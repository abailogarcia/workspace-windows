package ejercicio02;
import java.util.Scanner;

public class Alumno {
	static Scanner input = new Scanner(System.in);
	//atributos
	private String nombre;
	private String apellidos;
	private double notaMedia;
	
	//constructor sin parámetros
	public Alumno() {
		this.nombre="";
		this.apellidos="";
		this.notaMedia=0.0;
		
	}
	
	//setter y getter
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidos() {
		return apellidos;
	}


	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}


	public double getNotaMedia() {
		return notaMedia;
	}


	public void setNotaMedia(double notaMedia) {
		this.notaMedia = notaMedia;
	}
	//métodos

	public void promociona(double nota) {
		if (nota>= 5) {
			System.out.println("Has aprobado");
		} else {
			System.out.println("Has suspendido");
		}
	}
	//public boolean promociona(doble notaMedai){
	//if(notaMedia>=5){
		//return true;
		//}
		//return false;
	public void rellenarAlumno() {
		System.out.println("Dime el nombre");
		nombre=input.nextLine();
		//guardo el nombre del alumno
		this.setNombre(nombre);
		System.out.println("Dime los apellidos");
		apellidos =input.nextLine();
		this.setApellidos(apellidos);
		System.out.println("Dame la nota");
		notaMedia=input.nextDouble();
		this.setNotaMedia(notaMedia);
		input.nextLine();
		
		//guardamos los datos
		//necesitamos el set
		//obAlumno.setNombre(nombre);
		//obAlumno.setApellidos(apellidos);
		//obAlumno.setNotaMedia(notaMedia);
	
	}
	public void visualizarAlumno(Alumno obAlumno) {
		System.out.println("El nombre es " + nombre);
		System.out.println("Sus apellidos son " + apellidos);
		System.out.println("La nota media es " + notaMedia);
	}
}
