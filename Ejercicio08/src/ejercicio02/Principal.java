package ejercicio02;

public class Principal {

	public static void main(String[] args) {
		//creamos un alumno
		Alumno obAlumno1 = new Alumno() ;
		Alumno obAlumno2 = new Alumno() ;

		//rellenar alumno
		obAlumno1.rellenarAlumno();
		obAlumno2.rellenarAlumno();

		//visualizar alumno
		obAlumno1.visualizarAlumno(obAlumno1);
		obAlumno1.promociona(obAlumno1.getNotaMedia());
		obAlumno2.visualizarAlumno(obAlumno1);
		//promociona
		
		obAlumno2.promociona(obAlumno2.getNotaMedia());
		
		
	}

}
