package ejercicioprevio1;

public class Principal {

	public void imprimirInfo(Naranja oNaranja) {
		//El m�todo imprimirInfo recibe una naranja
				//oNaranja es un objeto naranja
				//String cadena -> cadena es un objeto String
		System.out.println("Fruta Naranja");
		System.out.println("Nombre " + oNaranja.nombre);
		System.out.println("Caracteristicas " + oNaranja.caracteristicas);
	}
	public static void main(String[] args) {
		//creo un objeto Naranja
		Naranja oNaranja = new Naranja();
		//Naranja() es un constructor por defecto cuando no he definnido yo uno
		//creo un objeto Principal
		Principal miObjeto = new Principal();
		
		//cuando un m�todo no es static, necesita crear un objeto de la clase
		//llamo al m�todo imprimirInfo (recibe una naranja)
		miObjeto.imprimirInfo(oNaranja);

	}

}
