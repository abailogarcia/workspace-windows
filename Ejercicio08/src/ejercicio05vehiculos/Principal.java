package ejercicio05vehiculos;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		//pedir datos
		Scanner input= new Scanner(System.in);
		System.out.println("Dame el modelo");
		String modelo = input.nextLine();
		System.out.println("Dame la marca");
		String marca = input.nextLine();
		System.out.println("Dame la autonomia");
		int autonomia = input.nextInt();
		System.out.println("Dame el kilometraje");
		int kilometraje = input.nextInt();
		
		//crear vehiculo con los datos introducidos por teclado
		Vehiculo miCoche=new Vehiculo(modelo,marca,autonomia,kilometraje);
		
		//mostrar con getter
		System.out.println("Marca " + miCoche.getMarca() );
		System.out.println("Modelo " + miCoche.getModelo());
		System.out.println("Autonom�a " + miCoche.getAutonomia());
		System.out.println("Kilometraje " +miCoche.getKilometraje() );
		
		System.out.println("Es seguro?");
		
		System.out.println(miCoche.esSeguro(miCoche.getKilometraje(),miCoche.getAutonomia()));
		
input.close();
	}

}
