package ejercicio05vehiculos;

public class Vehiculo {
	//atributos
	private String marca;
	private String modelo;
	private int autonomia;
	private int kilometraje;
	//constructor
	public Vehiculo() {
		this.marca="";
		this.modelo="";
		this.autonomia=0;
		this.kilometraje=0;
	}
	public Vehiculo(String marca,String modelo,int autonomia,int kilometraje) {
		this.marca=marca;
		this.modelo=modelo;
		this.autonomia=autonomia;
		this.kilometraje=kilometraje;
	}
	
	//getter y setter
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public int getAutonomia() {
		return autonomia;
	}
	public void setAutonomia(int autonomia) {
		this.autonomia = autonomia;
	}
	public int getKilometraje() {
		return kilometraje;
	}
	public void setKilometraje(int kilometraje) {
		this.kilometraje = kilometraje;
	}
	//m�todos
	boolean esSeguro(int kilometro, int autonomia) {
		if( kilometro>autonomia) {
			return false;
		}
		return true;
	}
}
