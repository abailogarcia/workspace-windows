package ejercicio01;

public class Profesor {
	//Crea una clase b�sica de nombre
//	Profesor.java que contenga los atributos siguientes: nombre, apellidos y ciclo que imparte
//	(ASIR � DAM). 
	//atributos
	private String nombre;
	private String apellidos;
	private String ciclo;
	
	//se recomienda atributos privados con m�todos setter y getter p�blicos
	//setter y getter
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
}
