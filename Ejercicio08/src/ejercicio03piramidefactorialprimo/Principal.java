package ejercicio03piramidefactorialprimo;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un n�mero");
		Numero numerico=new Numero(input.nextInt());
		if(numerico.primo(numerico.getNumero())) {
			System.out.println("Es primo");
			}else {
				System.out.println("No es primo");
			}
		System.out.println("El factorial de " +numerico.getNumero() + " es " + numerico.factorial(numerico.getNumero()));
		input.close();
		numerico.piramide1(numerico.getNumero());
		numerico.piramide2(numerico.getNumero());
	}

}
