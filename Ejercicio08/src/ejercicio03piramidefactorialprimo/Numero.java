package ejercicio03piramidefactorialprimo;

public class Numero {
	//atributos
		private int numero;
	//constructor sin parámetros
		public Numero() {
			this.numero=0;
		}
	//constructor con parámetros
		public Numero(int numero1) {
			this.numero = numero1;
		}
	//setter y getter
		public int getNumero() {
			return numero;
		}
		public void setNumero(int numero) {
			this.numero = numero;
		}
		
	//métodos
		public boolean primo(int num) {
			for(int i =2;i<num;i++) {
				if(num%i==0) {
					return false;
				}
			}
			return true;
		}
		public long factorial(int num) {
			long total=1;
			for (int i =1;i<=num;i++) {
				total*=i;
			}
			return total;
		}
		public void piramide1(int num) {
			for(int i = 1; i <= num; i++){
				
				//Calcular el numero de espacios, en funcion de la altura
				int numEspacios = num - i; 
				for(int j = numEspacios; j > 0 ; j--){
					System.out.print(" ");
				}
				
				//calcular el numero de asteriscos, en funcion de la altura
				int numAsteriscos = i * 2 - 1;
				for(int k = 0; k < numAsteriscos; k++){
					System.out.print("*");
				}
				
				System.out.println();
			
			}
		}
		public void piramide2(int num) {
			for(int i = num; i >0; i--){
				
				//Calcular el numero de espacios, en funcion de la altura
				int numEspacios = num - i; 
				for(int j = 0; j < numEspacios;  j++){
					System.out.print(" ");
				}
				
				//calcular el numero de asteriscos, en funcion de la altura
				int numAsteriscos = i * 2 - 1;
				for(int k = numAsteriscos;  k > 0;k--){
					System.out.print("*");
				}
				
				System.out.println();
			
			}
		}

}
