package ejercicio04libros;

public class Principal {

	public static void main(String[] args) {
		// creo 3 libros
		Libro libro1=new Libro("titulo1", "autor1",2002,"editorial1", 54);
		Libro libro2=new Libro("titulo2", "autor2",2003,"editorial2", 20);
		Libro libro3=new Libro("titulo3", "autor3",2006,"editorial3", 150);
		//muestro los datos de cada libro
		System.out.println("Datos del libro 1");
		System.out.println("T�tulo " + libro1.getTitulo());
		System.out.println("Autor " + libro1.getAutor());
		System.out.println("A�o de publicaci�n " + libro1.getAnyo());
		System.out.println("Editorial "+ libro1.getEditorial());
		System.out.println("Precio " + libro1.getPrecio());
		System.out.println("Datos del libro 2");
		System.out.println("T�tulo " + libro2.getTitulo());
		System.out.println("Autor " + libro2.getAutor());
		System.out.println("A�o de publicaci�n " + libro2.getAnyo());
		System.out.println("Editorial "+ libro2.getEditorial());
		System.out.println("Precio " + libro2.getPrecio());
		System.out.println("Datos del libro 3");
		System.out.println("T�tulo " + libro3.getTitulo());
		System.out.println("Autor " + libro3.getAutor());
		System.out.println("A�o de publicaci�n " + libro3.getAnyo());
		System.out.println("Editorial "+ libro3.getEditorial());
		System.out.println("Precio " + libro3.getPrecio());
		
		//mostramos el precio con iva
		final float IVA=21F;
		System.out.println("Mostramos los precios con IVA");
		System.out.println("Precio con IVA " + Libro.precioConIva(libro1.getPrecio(), IVA));
		System.out.println("Precio con IVA " + Libro.precioConIva(libro2.getPrecio(), IVA));
		System.out.println("Precio con IVA " + Libro.precioConIva(libro3.getPrecio(), IVA));
		
		
	}

}
