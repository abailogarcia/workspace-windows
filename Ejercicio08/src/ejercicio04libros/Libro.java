package ejercicio04libros;

public class Libro {
	//atributos
	private String titulo;
	private String autor;
	private int anyo;
	private String editorial;
	private double precio;
	//constructor sin parámetros
	public Libro() {
		this.titulo="";
		this.autor="";
		this.anyo=0;
		this.editorial="";
		this.precio=0.0;
	}
	//constructor con parámetros
	
	
	
	public Libro(String titulo, String autor, int anyo, String editorial, double precio) {
		this.titulo = titulo;
		this.autor = autor;
		this.anyo = anyo;
		this.editorial = editorial;
		this.precio = precio;
	}
	//setter y getter
	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getAnyo() {
		return anyo;
	}

	public void setAnyo(int anyo) {
		this.anyo = anyo;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	static double precioConIva(double precio, double  iva) {
		double precioCon=precio+(precio*(iva)/100);
		return precioCon;
		
	}
	// no hay método rellenar y visualizar
}
