package clases;

public class Profesor extends Persona{
	//atributos
	
	private String ciclo;
	private String idProfesor;

	//constructores
	public Profesor(String nombre, String apellidos, int edad) {
		super(nombre, apellidos, edad);
	}

	public Profesor(String nombre, String apellidos, int edad, String ciclo) {
		super(nombre, apellidos, edad);
		this.ciclo = ciclo;
	}

	//setter y getter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getIdProfesor() {
		return idProfesor;
	}

	public void setIdProfesor(String idProfesor) {
		this.idProfesor = idProfesor;
	}
	@Override
	public String toString() {
		return "Nombre y apellidos " + nombre + " "+apellidos + 
				"\nEdad " + edad + 
				"\nCiclo " + ciclo;
	}

	public void mostrarDatosProfesor(Profesor profesor) {
		System.out.println("Los datos del profesor son ");
		System.out.println(profesor.toString());
	}

	/* (non-Javadoc)
	 * @see clases.Persona#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Profesor) {
			Profesor tmpProfesor = (Profesor) obj;
			if (super.equals(tmpProfesor) 
					&& this.idProfesor.equals(tmpProfesor.idProfesor))
					 {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	
	}
	

}
