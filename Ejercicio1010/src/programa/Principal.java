package programa;

import clases.Persona;
import clases.Profesor;

public class Principal {

	public static void main(String[] args) {
		// creamos profesores
		Profesor profesor1 = new Profesor("Juan","Fernandez",33, "DAM");
		profesor1.setIdProfesor("Prof 22-387-11");
		Profesor profesor2 = new Profesor("Juan","Fernandez",33, "DAM");
		profesor2.setIdProfesor("Prof 22-387-11");
		Profesor profesor3 = new Profesor("Juan","Fernandez",33, "DAM");
		profesor3.setIdProfesor("Prof 33-387-33");
		Persona persona1 = new  Persona("Jos�", "Hern�ndez L�pez", 28);
		Persona persona2 = new  Persona("Jos�", "Hern�ndez L�pez", 28);
		Persona persona3 = new  Persona("Ramiro", "Su�rez Rodrguez", 19);
		
		System.out.println("Son iguales persona1 y persona2 " + persona1.equals(persona2));
		System.out.println("Son iguales persona1 y persona3 " + persona1.equals(persona3));
		System.out.println("Son iguales profesor1 y profesor2 " + profesor1.equals(profesor2));
		System.out.println("Son iguales profesor1 y profesor3 " + profesor1.equals(profesor3));
	}

}
