package ejercicioprevio001;

import java.util.ArrayList;

public class ArrayList1 {

	public static void main(String[] args) {
		System.out.println("Creamos un ArrayList");
		ArrayList<String> nombres = new ArrayList<String>();
		System.out.println("A�adimos elementos");
		nombres.add("Ana");
		nombres.add("Luisa");
		nombres.add("Felipe");
		System.out.println("Mostramos");
		System.out.println(nombres);
		System.out.println("A�adimos uno m�s");
		nombres.add(1, "Pablo");
		System.out.println(nombres);
		System.out.println("Borramos el 0");
		nombres.remove(0);
		System.out.println(nombres);
		System.out.println("Ponemos en la posici�n 0 un dato");
		nombres.set(0, "Alfonso");
		System.out.println("Mostramos la parte final del ArrayList");
		String s=nombres.get(1);
		String ultimo =nombres.get(nombres.size()-1);
		System.out.println("Mostramos");
		System.out.println(s+ " " + ultimo);

	}

}
