package ejercicio0903;

import java.util.ArrayList;
import java.util.Iterator;

public class Ejercicio0903 {

	public static void main(String[] args) {
		// Crear un arrayList de elementos tipo Integer con n�meros enteros generados
		// aleatoriamente a partir de Math.random(). Los n�meros aleatorios debes estar
		// entre 1 y
		// 20.
		// Al arraylist se a�adir�n 100 n�meros Integer, y los mostrar�. Posteriormente
		// borrar� de
		// ese array todos los n�meros que est�n entre 10 y 15, y volver� a mostrar el
		// arraylist

		ArrayList<Integer> lista = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			lista.add((int) Math.round((Math.random() * (20 - 1) + 1)));
		}
		for (int numero : lista) {
			System.out.print(numero + " ");
		}
		for (int i = lista.size()-1; i >= 0; i--) {
			if (lista.get(i) >= 10 && lista.get(i) <= 15) {
				lista.remove(i);
			}
		}
		/*
		 * for(int numero:lista) { if(lista.get(numero)>=10&&lista.get(numero)<=15) {
		 * lista.remove(numero); } }
		 */
		// eliminamos con bucle while
		int contador = 0;
		while (contador < lista.size()) {
			if (lista.get(contador) >= 10 && lista.get(contador) <= 15) {
				lista.remove(contador);

			} else {
				contador++;
			}
		}
		// eliminamos con la clase iterator
		Iterator<Integer> it = lista.iterator();
		while (it.hasNext()) {
			Integer numero = it.next();
			if (numero >= 10 && numero <= 15) {
				it.remove();
			}
		}

		System.out.println("Listo los n�meros borrados");
		for (int numero : lista) {
			System.out.print(numero + " ");
		}
		System.out.println("Listo los n�meros borrados con iterador");
		System.out.println();
		Iterator<Integer> iterador = lista.iterator();
		while (iterador.hasNext()) {
			System.out.print(iterador.next() + " ");
		}
	}

}
