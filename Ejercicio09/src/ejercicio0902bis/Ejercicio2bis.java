package ejercicio0902bis;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio2bis {

	public static void main(String[] args) {
		//Por consola iremos indicando que se introduzca un string
		//y se almacenar� en el arraylist. Se pedir�n Strings hasta introducir la cadena �fin�.
		//Posteriormente se mostrar�n todos los Strings del array list.
		Scanner input = new Scanner(System.in);
		ArrayList<String> lista = new ArrayList<>();
		String cadena;
		do {
			System.out.println("Introduce cadenas hasta que pongas fin");
			cadena=input.nextLine();
			lista.add(cadena);
		}while(!cadena.equalsIgnoreCase("fin"));
		listar(lista);
		System.out.println("Introduce la cadena a borrar");
		String borrar = input.nextLine();
		borrarCadena(borrar,lista);
		listar(lista);
		
		
		input.close();

	}
	//Crearemos un m�todo est�tico borrarCadena(String cadena, ArrayList lista) que borra
	//dicha cadena del arraylist. Si la cadena no existe no se podr� borrar nada. Llamaremos a
	//ese m�todo para borrar una cadena. 
	
	public static void borrarCadena(String cadena, ArrayList lista) {
		lista.remove(cadena);
	}
	public static void listar(ArrayList<String> lista) {
		for(String cadena:lista) {
			System.out.println(cadena);
		}
	}

}
