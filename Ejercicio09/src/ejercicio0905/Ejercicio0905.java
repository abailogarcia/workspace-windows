package ejercicio0905;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Ejercicio0905 {

	public static void main(String[] args) {
		/*Crear un ArrayList de Strings, y a�adir varias cadenas directamente para probar el
siguiente m�todo. M�todo est�tico borrarCadenas(String cadena, ArrayList lista). Debo
borrar del arraylist toda aparici�n de la cadena recibida como par�metro. Revisar la
incidencia de las distintas formas de iterar la lista a la hora de borrar elementos.*/
		ArrayList<String> cadenas = new ArrayList<>();
		cadenas.add("Mi casa");
		cadenas.add("me gusta");
		cadenas.add("mucho gracias");
		cadenas.add("Hola");
		cadenas.add("Hola");
		Scanner input=new Scanner(System.in);
		//System.out.println("Introduce la cadena a borrar");
		//String cadena = input.nextLine();
		for(String cadena2:cadenas) {
			System.out.println(cadena2);
		}
		borrarCadenas("Hola",cadenas);
		
		
		for(String cadena2:cadenas) {
			System.out.println(cadena2);
		}
		input.close();

	}
	public static void borrarCadenas(String cadenaABorrar, ArrayList<String> cadenas) {
		Iterator<String> iterador =cadenas.iterator();
		while(iterador.hasNext()) {
			String cadena =iterador.next();
			if(cadena.equals(cadenaABorrar)) {
				iterador.remove();
			}
		}
	}

}
