package ejercicio0902;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		// Se pedir�n Strings hasta introducir la cadena �fin�.
		// Posteriormente se mostrar�n todos los Strings del array list.
		Scanner input = new Scanner(System.in);
		ArrayList<String> lista = new ArrayList<>();
		System.out.println("Introduce una cadena y escribe 'fin' para terminar");
		String cadena;
		do {
			System.out.println("Introduce una cadena");
			cadena = input.nextLine();
			lista.add(cadena);

		} while (!cadena.equalsIgnoreCase("fin"));
			/*for (String palabra : lista) {
				System.out.println(palabra);
			}*/
		listar(lista);
		System.out.println("Dame la cadena a borrar");
		cadena=input.nextLine();
		borrarCadena(cadena,lista);
		listar(lista);

		// Crearemos un m�todo est�tico borrarCadena(String cadena, ArrayList lista) que
		// borra
		// dicha cadena del arraylist. Si la cadena no existe no se podr� borrar nada.
		// Llamaremos a
		// ese m�todo para borrar una cadena.

		input.close();
	}

	public static void borrarCadena(String palab, ArrayList listilla) {
		listilla.remove(palab);
	}
	public static void listar(ArrayList<String> lista) {
		for (String s: lista) {
			System.out.println(s);
		}
	}
}
