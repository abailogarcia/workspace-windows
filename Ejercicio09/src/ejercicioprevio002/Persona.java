package ejercicioprevio002;

public class Persona {
//atributos
	private int idPersona;
	private String nombre;
	private double altura;
	
	public Persona(int idPersona, String nombre, double altura) {
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.altura = altura;
	}
	/**
	 * @return the idPersona
	 */
	public int getIdPersona() {
		return idPersona;
	}
	/**
	 * @param idPersona the idPersona to set
	 */
	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the altura
	 */
	public double getAltura() {
		return altura;
	}
	/**
	 * @param altura the altura to set
	 */
	public void setAltura(double altura) {
		this.altura = altura;
	}


	@Override
	public String toString() {
		return "Persona [idPersona=" + idPersona + ", nombre=" + nombre + ", altura=" + altura + "]";
	}
	
}
