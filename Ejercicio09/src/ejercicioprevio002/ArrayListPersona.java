package ejercicioprevio002;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListPersona {

	public static void main(String[] args) {
		System.out.println("Creamos objetos y atributos");
		String nombre;
		double altura;
		Scanner leer = new Scanner(System.in);
		//arrayList de personas
		ArrayList<Persona> personitas = new ArrayList<Persona>();
		System.out.println("Pedimos datos");
		System.out.println("Dime el n�mero de personas a introducir");
		int n = leer.nextInt();
		for(int i =1; i<=n;i++) {
			System.out.println("Dame el nombre");
			nombre=leer.next();
			System.out.println("Dame la altura");
			altura=leer.nextDouble();
			//para a�adir las personas al arraylist
			//tengo que crear personas con un constructor
			//add a�ade al arraylist y con new y el constructor creo objetos
			//new Persona(i,nombre,altura) -> creo la persona
			//personitas.add-> a�ado la persona
			personitas.add(new Persona(i,nombre,altura));
			
		}
		System.out.println("Mostrar el ArrayList");
		System.out.println(personitas);
		System.out.println("Calculo la media de la altura");
		double suma=0;
		for(Persona i: personitas) {
			suma=suma+i.getAltura();
		}
		System.out.println("Suma " + suma);
		System.out.println("Media " + suma/personitas.size());
		leer.close();

	}

}
