package ejercicio0901bis;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio0901bis {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<String> lista =new ArrayList<>();
		System.out.println("Introduce el n�mero de cadenas a introducir");
		int numero = input.nextInt();
		input.nextLine();
		for(int i =0; i<numero;i++) {
			System.out.println("Introduce una cadena");
			String cadena=input.nextLine();
			lista.add(cadena);
			
		}
		for(String cadena:lista) {
			System.out.println(cadena);
		}
		System.out.println("Introduce la posici�n del elemento a borrar");
		int borrar=input.nextInt();
		lista.remove(borrar);
		for(String cadena:lista) {
			System.out.println(cadena);
		}
		input.close();
	}

}
