package clases;

public class Barco extends Vehiculo{

	private boolean vela;
	private String nombreCapitan;
	
	public Barco(String matricula, String marca, int plazas, boolean vela, String nombreCapitan) {
		super(matricula, marca, plazas);
	
		this.vela = vela;
		this.nombreCapitan = nombreCapitan;
	}
	/*
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public int getPlazas() {
		return plazas;
	}
	public void setPlazas(int plazas) {
		this.plazas = plazas;
	}
	*/
	public boolean isVela() {
		return vela;
	}
	public void setVela(boolean vela) {
		this.vela = vela;
	}
	public String getNombreCapitan() {
		return nombreCapitan;
	}
	public void setNombreCapitan(String nombreCapitan) {
		this.nombreCapitan = nombreCapitan;
	}
	@Override
	public String toString() {
		return "Barco [vela=" + vela + ", nombreCapitan=" + nombreCapitan + ", matricula=" + matricula + ", marca="
				+ marca + ", plazas=" + plazas + "]";
	}
	
	
	
	
	
}
