package clases;

public class Subclase extends Superclase {
	private int cantidad;

	public Subclase(String nombre, int cantidad) {
		super(nombre);
		this.cantidad = cantidad;
	}
	

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	@Override
	public String toString() {
		return "Subclase [cantidad=" + cantidad + ", getNombre()=" + getNombre() + ", toString()=" + super.toString()
				+ "]";
	}
	
}
