package clases;

public class Superclase {
	private String nombre;
	public Superclase(String nombre) {
		this.nombre = nombre;
		}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Superclase [nombre=" + nombre + "]";
	}

	
	
}
