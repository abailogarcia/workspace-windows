package clases;
//se a�ade final para que no pueda ser heredada por otra
public final class Directivo extends Empleado{
	public Directivo(String nombre) {
		super(nombre);
	}

	@Override
	public String toString() {
		return "[" + super.toString() + "] -> Director";
	}
}
