package clases;

//la clase Tecnico no puede ser heredada por otra mas
//he a�adido final
public final class Tecnico extends Operario{
	public Tecnico(String nombre) {
		super(nombre);
	}

	@Override
	public String toString() {
		return "[" + super.toString() + "] -> Tecnico";
	}
	
	
}
