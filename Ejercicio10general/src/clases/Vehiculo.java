package clases;

public class Vehiculo {
	protected String matricula;
	protected String marca;
	protected int plazas;
	
	protected static int cantidadInstancias;

	public Vehiculo(String matricula, String marca, int plazas) {
		this.matricula = matricula;
		this.marca = marca;
		this.plazas = plazas;
		Vehiculo.cantidadInstancias++;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getPlazas() {
		return plazas;
	}

	public void setPlazas(int plazas) {
		this.plazas = plazas;
	}
//getter static
	public static int getCantidadInstancias() {
		return cantidadInstancias;
	}
	//getter no static
	public int getCantidadInstanciasStatic() {
		return cantidadInstancias;
	}
	/*no hay setter
	 * public static void setCantidadInstancias(int cantidadInstancias) {
		Vehiculo.cantidadInstancias = cantidadInstancias;
	}*/

	@Override
	public String toString() {
		return "Vehiculo [matricula=" + matricula + ", marca=" + marca + ", plazas=" + plazas + "]";
	}
	
	
}
