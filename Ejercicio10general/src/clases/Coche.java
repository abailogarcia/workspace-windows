package clases;

public class Coche extends Vehiculo {
	
	private float Km;
	
	public Coche(String matricula, String marca, int plazas, float km) {
		super(matricula, marca, plazas);
		this.Km = km;
	}
/*
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getPlazas() {
		return plazas;
	}

	public void setPlazas(int plazas) {
		this.plazas = plazas;
	}
*/
	public float getKm() {
		return Km;
	}

	public void setKm(float km) {
		Km = km;
	}

	public void trucarKm() {
		this.Km=0;
	}
	@Override
	public String toString() {
		return "Coche [Km=" + Km + ", toString()=" + super.toString() + "]";
	}

	
	
}
