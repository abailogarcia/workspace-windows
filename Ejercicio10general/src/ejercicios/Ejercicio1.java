package ejercicios;

import clases.Subclase;
import clases.Superclase;

public class Ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Superclase padre = new Superclase("Pepe");
		Subclase hijo = new Subclase("Pepe",15);
		
		System.out.println(padre);
		System.out.println(hijo);
		
	}

}
