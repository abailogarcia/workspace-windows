package ejercicios;

import clases.Avion;
import clases.Barco;
import clases.Coche;
import clases.Vehiculo;

public class Ejercicio3 {

	public static void main(String[] args) {
		Coche renault = new Coche("z1234","renault",5, 2500);
		Avion harrier = new Avion("b345354","harrier",2, 8);
		Barco mibarco= new Barco("c34223", "marvin", 7, true,  "Chanquete");
		
		System.out.println(renault);
		System.out.println(harrier);
		System.out.println(mibarco);
		System.out.println(mibarco.getNombreCapitan());
		System.out.println(Vehiculo.getCantidadInstancias());
		System.out.println(Barco.getCantidadInstancias());

	}

}
