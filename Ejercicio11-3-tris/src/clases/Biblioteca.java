package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Biblioteca implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int DIAS_PRESTAMO = 15;

	private ArrayList<Articulos> listaArticulos;
	private ArrayList<Socios> listaSocios;
	private ArrayList<Prestamos> listaPrestamos;

	public Biblioteca() {
		listaArticulos = new ArrayList<Articulos>();
		listaSocios = new ArrayList<Socios>();
		listaPrestamos = new ArrayList<Prestamos>();
	}

	public void altaSocio(String nombre) {
		listaSocios.add(new Socios(listaSocios.size() + 1, nombre, LocalDate.now()));
	}

	public void altaArticulo(String isbn, String titulo, String editorial, String autor) {
		listaArticulos.add(new Libros(isbn, titulo, editorial, autor));
		Collections.sort(listaArticulos);
	}

	public void altaArticulo(String isbn, String titulo, String editorial, boolean online) {
		listaArticulos.add(new Revistas(isbn, titulo, editorial, online));
		Collections.sort(listaArticulos);
	}

	public void listarArticulos() {
		System.out.println("Lista articulos: ");
		for (Articulos articulo : listaArticulos) {
			System.out.println(articulo);
		}
	}

	public void borrarPrestamo(int idPrestamo) {
		Iterator<Prestamos> iterador = listaPrestamos.iterator();
		while (iterador.hasNext()) {
			Prestamos prestamo = (Prestamos) iterador.next();
			if (prestamo.getIdPrestamo() == idPrestamo) {
				iterador.remove();
			}
		}
	}

	public void borrarArticulo(String isbn) {
		Iterator<Articulos> iterador = listaArticulos.iterator();
		while (iterador.hasNext()) {
			Articulos articulo = (Articulos) iterador.next();
			if (articulo.getIsbn().equals(isbn)) {
				iterador.remove();
			}
		}
	}

	public void borrarSocio(int idSocio) {
		Iterator<Socios> iterador = listaSocios.iterator();
		while (iterador.hasNext()) {
			Socios socio = (Socios) iterador.next();
			if (socio.getIdSocio() == idSocio) {
				iterador.remove();
			}
		}
	}

	public void borrarOtroPrestamo(int idPrestamo) {
		Iterator<Prestamos> iterador = listaPrestamos.iterator();
		while (iterador.hasNext()) {
			Prestamos prestamo = (Prestamos) iterador.next();
			if (prestamo.getIdPrestamo() == idPrestamo) {
				iterador.remove();
			}
		}
	}

	public void borrarOtroPrestamoMas(int idPrestamo) {
		Iterator<Prestamos> iterador = listaPrestamos.iterator();
		while (iterador.hasNext()) {
			Prestamos prestamo = (Prestamos) iterador.next();
			if (prestamo.getIdPrestamo() == idPrestamo) {
				iterador.remove();
			}
		}
	}

	public Socios devuelveSocio(int idSocio) {
		for (Socios socio : listaSocios) {
			if (socio.getIdSocio() == idSocio) {
				return socio;
			}
		}
		return null;
	}

	public void crearPrestamoSocio(int idSocio) {
		if (devuelveSocio(idSocio) != null) {
			listaPrestamos.add(new Prestamos(listaPrestamos.size() + 1, LocalDate.now(),
					LocalDate.now().plusDays(DIAS_PRESTAMO), devuelveSocio(idSocio)));
		} else {
			System.out.println("El socio no existe");
		}
	}

	public void mostrarPrestamosSocio(int idSocio) {
		for (Prestamos prestamo : listaPrestamos) {
			if (prestamo.getSocio().equals(devuelveSocio(idSocio))) {
				System.out.println(prestamo);
			}
		}
	}

	public void introducirArticuloPrestamo(int idPrestamo, String isbn) {
		if (devuelvePrestamo(idPrestamo) != null) {
			if (devuelveArticulo(isbn) != null) {
				devuelvePrestamo(idPrestamo).listaArticulos.add(devuelveArticulo(isbn));
			} else {
				System.out.println("El art�culo no existe");
			}
		} else {
			System.out.println("El pr�stamo no existe");
		}
	}

	public Prestamos devuelvePrestamo(int idPrestamo) {
		for (Prestamos prestamo : listaPrestamos) {
			if (prestamo.getIdPrestamo() == idPrestamo) {
				return prestamo;
			}
		}
		return null;
	}

	public Articulos devuelveArticulo(String isbn) {
		for (Articulos articulo : listaArticulos) {
			if (articulo.getIsbn().equals(isbn)) {
				return articulo;
			}
		}
		return null;
	}

	public void listarPrestamos() {
		for (Prestamos prestamo : listaPrestamos) {
			System.out.println(prestamo);
		}
	}

	// guardar datos
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datos.dat")));
			escritor.writeObject(listaArticulos);
			escritor.writeObject(listaPrestamos);
			escritor.writeObject(listaSocios);
			escritor.close();
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
		}
	}
	//cargar datos
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream escritor = new ObjectInputStream(new FileInputStream(new File("src/datos.dat")));
			listaPrestamos = (ArrayList<Prestamos>) escritor.readObject();
			listaSocios=(ArrayList<Socios>) escritor.readObject();
			listaArticulos=(ArrayList<Articulos>) escritor.readObject();
			escritor.close();
		}catch ( IOException e) {
			System.out.println("Error de entrada salida");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
