package clases;

import java.io.Serializable;

public class Libros extends Articulos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int PUNTOS = 5;
	
	private String editorial;
	private String autor;

	public Libros(String isbn, String titulo, String editorial, String autor) {
		super(isbn, titulo);
		this.editorial = editorial;
		this.autor = autor;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	
	@Override
	public int compareTo(Articulos o) {
		return getIsbn().compareTo(o.getIsbn());
	}

	@Override
	public int calcularPuntos() {
		int totalPuntos=0;
		totalPuntos+=PUNTOS;
		return totalPuntos;
	}

	@Override
	public String toString() {
		return "Libros [editorial=" + editorial + ", autor=" + autor + ", toString()=" + super.toString() + "]";
	}

	
}
