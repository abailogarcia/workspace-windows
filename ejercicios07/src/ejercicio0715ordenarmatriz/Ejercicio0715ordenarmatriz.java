package ejercicio0715ordenarmatriz;

import java.util.Scanner;

public class Ejercicio0715ordenarmatriz {

	public static void main(String[] args) {
		// Programa que me pedir� el n�mero de filas y despu�s el de columnas de una matriz y me
	//	crear� una matriz de ese tama�o que rellenar� con n�meros aleatorios de -50 a 49 (100
	//			n�meros). Esta matriz se mostrar� por pantalla. Posteriormente se llamar� a un m�todo
	//			que ordenar� esa matriz de menor a mayor. El programa principal, mostrar� despu�s esta
	//			matriz ordenada
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el n�mero de filas de la matriz");
		int filas= input.nextInt();
		System.out.println("Introduce el n�mero de columnas de la matriz");
		int columnas= input.nextInt();
		int[][] matriz = new int[filas][columnas];
		for (int i =0; i<matriz.length; i++) {
			for ( int j =0; j<matriz[i].length; j++) {
				matriz[i][j] = (int)(Math.random()*(100+1)-50);
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		//int[][] matrizOrdenada=new int[columnas][filas];
		ordenarMatriz(matriz);
		for (int i =0; i<matriz.length; i++) {
			for ( int j =0; j<matriz[i].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
		input.close();
	}
	
	public static void  ordenarMatriz(int[][] matriz){
		for (int i =0 ; i<matriz.length; i++) {
			for (int j=0; j<matriz[i].length;j++) {
				for(int k =0; k<matriz.length;k++) {
					for (int l=0; l<matriz[k].length;l++) {
			// si encuentro uno menor lo cambio			
						if(matriz[k][l]>matriz[i][j]) {
							int aux = matriz[i][j];
							matriz[i][j] = matriz[k][l];
							matriz[k][l]=aux;
						}
						
					}
				}
				
			}
			
		}
	}

}
