package ejercicio0705estadisticasnumeros;

import java.util.Scanner;

public class Ejercicio0705estadisticasnumeros {

	public static void main(String[] args) {
		// Crear un programa que pida 10 n�meros enteros y los guarde en un array de 10
		//elementos. Posteriormente se llamar� a un m�todo que recibe dicho array, y nos mostrar�
		//por pantalla una serie de estad�sticas: cu�l es el mayor, y cu�l es el menor, la media de los
		//positivos y de los negativos, y la media de todos.
		Scanner input = new Scanner(System.in);
		int[] array = new int[10];
		for(int i =0; i<array.length; i++) {
			System.out.println("Introduce la posici�n " + i + "del vector");
			array[i]=input.nextInt();
		}
		estadisticas(array);
		input.close();

	}
	public static void estadisticas(int[] vector) {
		int suma =0;
		int sumaPositivos=0;
		int sumaNegativos=0;
		int cuentaPositivos=0;
		int cuentaNegativos=0;
		int max=0;
		int min=0;
		for(int i =0; i<vector.length; i++) {
			suma+=vector[i];
			if ( i==0) {
				max = vector[0];  //ponemos el valor a 0 y luego le damos el primer valor del vector para ahorrar errores
				min = vector[0];
			}
			if (vector[i] >=0) {
				sumaPositivos+=vector[i];
				cuentaPositivos++;
			}
			if (vector[i] <0) {
				sumaNegativos+=vector[i];
				cuentaNegativos++;
			}
			if (vector[i]>max) {
				max=vector[i];
			}
			if(vector[i]<min) {
				min=vector[i];
			}
			
		}
		System.out.println("La media de todos los n�meros es " + (double)(suma/vector.length));
		System.out.println("El valor m�ximo es " + max);
		System.out.println("El valor m�nimo es " + min);
		if(cuentaPositivos>0) {
		System.out.println("La media de los n�meros positivos es " +(double)(sumaPositivos/cuentaPositivos));
		}
		if(cuentaNegativos>0) {
		System.out.println("La media de los n�meros negativos es " + (double)(sumaNegativos/cuentaNegativos));
		}
	}

}
