package ejercicio0706sumavectores;

import java.util.Scanner;

public class Ejercicio0706sumavectores {

	public static void main(String[] args) {
		// Programa que pida por teclado n�meros y rellene dos arrays de enteros de 10 elementos
		//(20 n�meros). Posteriormente llamar� a un m�todo que recibe ambos arrays y que crear�
		//y devolver� un array de 10 elementos organizados de la siguiente forma: El elemento 0 es
		//la suma de los elementos 0 de ambos arrays, el elemento 1 es la suma de los elementos 1
		//de ambos arrays, etc. Para obtener el array con la suma de los otros 2 debo usar un bucle.
		Scanner input = new Scanner(System.in);
		int[] array1=new int[10];
		int[] array2=new int[10];
		for(int i =0; i<array1.length; i++) {
			System.out.println("Introduce la posici�n " + i + "del primer vector");
			array1[i]=input.nextInt();
		}
		for(int  j=0; j<array2.length; j++) {
			System.out.println("Introduce la posici�n " + j + "  del segundo vector");
			array2[j]=input.nextInt();
		}
		sumaVectores(array1,array2);
		input.close();
	}
	public static void sumaVectores(int[] vector1, int[] vector2) {
		int[] suma= new int[vector1.length];
		for (int i =0; i<vector1.length; i++) {
			suma[i]= vector1[i] + vector2[i];
			System.out.print(suma[i] + " ");
		}
	}

}
