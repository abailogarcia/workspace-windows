package ejercicio0713buscarnumeroenmatriz;

import java.util.Scanner;

public class Ejercicio0713buscarnumeroenmatriz {

	public static void main(String[] args) {
		// Programa que rellene una matriz de 10 x 10 con n�meros aleatorios entre 0 y 49, ambos
		//incluidos. Tambi�n me pedir� un n�mero para buscarlo dentro de la matriz
		//Posteriormente llamar a un m�todo que recibe dicha matriz, y el n�mero pedido. Este
		//m�todo debe buscar en la matriz el n�mero que yo he introducido, y me debe mostrar por
		//pantalla el lugar donde se encuentra cada vez que aparezca [fila, columna]. Finalmente me
		//debe decir el n�mero de veces que lo ha encontrado. Si no lo ha encontrado ninguna vez,
		//l�gicamente no me mostrar� ninguna coordenada
		Scanner input = new Scanner(System.in);
		int[][] matriz = new int[10][10];
		for (int i =0 ; i<matriz.length;i++) {
			for (int j =0; j<matriz[0].length;j++) {
				matriz[i][j]=(int)(Math.random()*(49-0+1)+0);
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println("Introduce un n�mero a buscar");
		int numero = input.nextInt();
		buscarNumero(matriz,numero);
		input.close();
		
	}
	public static void buscarNumero(int[][] matriz, int numero) {
		int contador =0;
		for (int i =0 ; i<matriz.length;i++) {
			for (int j =0; j<matriz[0].length;j++) {
					if (matriz[i][j] == numero ) {
						contador++;
						System.out.println("El n�mero " + numero + " est� en la posici�n [" + i + "][" + j + "]");
					}
			}
		}
		System.out.println("Se han encontrado " + contador + " coincidencias");
	}
	
}
