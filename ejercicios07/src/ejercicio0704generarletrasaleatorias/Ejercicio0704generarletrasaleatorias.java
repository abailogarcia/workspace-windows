package ejercicio0704generarletrasaleatorias;

import java.util.Scanner;

import ejercicio0703metodocreavectorconletrasconsecutivas.Ejercicio0703metodocreavectorconletrasconsecutivas;

public class Ejercicio0704generarletrasaleatorias {

	public static void main(String[] args) {
		//Crear un programa parecido al anterior. El programa desde el m�todo main debe pedirnos
		//por teclado el n�mero de elementos del array, y posteriormente construye un array y lo
		//rellene con letras aleatorias de la �a� a la �z�. Mostrar� el array por pantalla. Despu�s,
		//debemos llamar al m�todo del ejercicio anterior (debe ser p�blico) que modificar� nuestro
		//array y nos lo devolver�. Posteriormente lo mostraremos por pantalla.
		Scanner input = new Scanner (System.in);
		System.out.println("Dime cu�ntas letras quieres");
		int longitud = input.nextInt();
		char[] texto = new char[longitud];
		for (int i =0; i < longitud; i++) {
			texto[i] = (char)(Math.random()*('z'-'a'+1)+'a');
			System.out.print(texto[i] + " ");
		}
		System.out.println("");
		texto=Ejercicio0703metodocreavectorconletrasconsecutivas.sustituyeVocales(texto);
		for (int i =0; i < longitud; i++) {
			System.out.print(texto[i] + " ");
		}
		input.close();
	}

}
