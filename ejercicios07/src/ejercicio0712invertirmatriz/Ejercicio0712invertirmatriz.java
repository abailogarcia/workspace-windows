package ejercicio0712invertirmatriz;

import java.util.Scanner;

public class Ejercicio0712invertirmatriz {

	public static void main(String[] args) {
		// Crear un programa que pida un n�mero para crear una matriz cuadrada (3 ->3x3, 4 ->4x4,
		//5 ->5x5, etc). Despu�s llamar� a un m�todo que recibe la matriz y el objeto Scanner y que
	//nos pedir� n�meros para rellenar dicha matriz . Dicho m�todo la muestra tambi�n.
	//Posteriormente se llamar� a un m�todo que recibe la matriz y me crea otra que es el
	//resultado de intercambiar las filas por las columnas. Posteriormente el programa principal
	//recibe esta matriz modificada y la muestra por pantalla
		
		//private static void rellenarMatriz(int[][] matriz, Scanner input)
		Scanner input =new Scanner(System.in);
		int tama�o=0;
		System.out.println("Introduce el tama�o de la matriz");
		tama�o= input.nextInt();
		int[][] matriz = new int[tama�o][tama�o];
		int[][]matriz2=creaMatriz(matriz);
		int[][] matrizInvertida= invierteMatriz(matriz2);
		System.out.println("");
		for ( int i =0; i<matrizInvertida.length; i++) {
			for(int j=0; j<matrizInvertida[i].length;j++) {
				System.out.print(matrizInvertida[i][j] + " ");			
			}
			System.out.println();
		}
		
		input.close();

	}
	public static int[][] creaMatriz(int[][] matriz){
		Scanner input =new Scanner(System.in);
		for ( int i =0; i<matriz.length; i++) {
			for(int j=0; j<matriz[i].length;j++) {
				System.out.println("Introduce el valor [" + i + "][" + j +"]");
				matriz[i][j]=input.nextInt();
				
			}
		
		}
		for ( int i =0; i<matriz.length; i++) {
			for(int j=0; j<matriz[i].length;j++) {
				System.out.print(matriz[i][j] + " ");			
			}
				System.out.println();
		}
		input.close();
		return matriz;
	}
	
	public static int[][] invierteMatriz(int[][] matriz) {
		for ( int i =0; i<matriz.length; i++) {
			for(int j=0; j<matriz[i].length;j++) {
				matriz[i][j]=matriz[j][i];	
			}
		}
		return matriz;
	}

}
