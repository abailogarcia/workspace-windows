package ejercicio0701pedirnumerosenunmetodo;

import java.util.Scanner;

public class Ejercicio0701pedirnumerosenunmetodo {

	public static void main(String[] args) {
		// Crear una aplicaci�n que pida 10 n�meros enteros, y posteriormente muestre esos
		//n�meros en una l�nea, mostrando desde el �ltimo al primero, en ese orden. Se crear� un
		//m�todo que construye un array, pide los n�meros y los guarda en �l. Posteriormente dicho
		//m�todo devolver� el array, y ser� el programa principal quien los muestre en orden
		//inverso (1� la �ltima celda del array, y as� sucesivamente).
		int[] elVector=creaVector();
		for (int i = elVector.length-1; i>=0;i--) {
			System.out.print(" " + elVector[i] + " ");
		}

	}

	public static int[] creaVector() {
		Scanner input = new Scanner (System.in);
		int[] miVector=new int[10];
		for (int i=0; i<miVector.length; i++) {
			System.out.println("Introduce la componente " + i + " del vector");
			miVector[i]=input.nextInt();
		}
		input.close();
		return miVector;
	}
}
