package ejercicioprevio;

public class Ejercicioprevio01 {

	public static void main(String[] args) {
		//creo un vector de int de tama�o 5 llamado faltas
		//tipodatos[] nombre = new tipodatos[tama�o]
		int[] faltas = new int[5];
		//creo un vector de doubles de tama�o 3 llamado notas
		double[] notas = new double[3];
		//creo un vector de string llamado nombre y lo inicializo de tama�o 4
		String[] nombres;
		nombres = new String[4];
		//creo un vector de String llamado pa�ses con 4 valores ya
		String[] paises = {"Espa�a", "Francia" , "B�lgica",  "Italia" };
		//el �ndice del vector (posici�n) comienza en 0
		//mostrar una componente del vector paises
		System.out.println("La posici�n cero contiene " + paises[0]);
		System.out.println("La posici�n cero contiene " + paises[1]);
		System.out.println("La posici�n cero contiene " + paises[2]);
		System.out.println("La posici�n cero contiene " + paises[3]);
		

	}

}
