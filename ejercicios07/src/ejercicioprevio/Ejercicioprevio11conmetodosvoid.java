package ejercicioprevio;

import java.util.Scanner;

public class Ejercicioprevio11conmetodosvoid {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		 int[] miVector =new int[4];
		 
		 for (int i =0; i<miVector.length; i++) {
			 System.out.println("Dame la componente " + i + " del vector");
			 miVector[i]=input.nextInt();
		 }
		 //en las llamadas no se ponen corchetes
		 System.out.println("La suma del vector es " + sumaVector(miVector));
		 System.out.println("La suma del vector es " + promedioVector(miVector));
		 System.out.println("El m�ximo del vector es " + maximoVector(miVector));
		 System.out.println("El m�nimo del vector es " + minimoVector(miVector));
		 
		 input.close();

	}
	//m�todo suma recibe un vector y calcula su suma
	//en los par�metros y valores de devoluci�n (cabecera) los vectores llevan corchetes
	// en el return no lleva corchetes
	public static int sumaVector(int[] vector) {
		int suma=0;
		for (int i =0; i<vector.length; i++) {
			suma+=vector[i];
		}
		return suma;
	}
	
	
	//m�todo promeo recibe un vector calcula su promedio
	public static double promedioVector(int[] vector) {
		int suma=0;
		for (int i =0; i<vector.length; i++) {
			suma+=vector[i];
		}
		double promedio=suma/vector.length;
		return promedio;
	}
	
	//m�todo m�ximmo recibe un vector y calcula su m�ximo
	public static int maximoVector(int[] vector) {
		int max = vector[0];
		for (int i =0; i<vector.length; i++) {
			if ( vector[i] > max) {
				max=vector[i];
			}
		}
		return max;
	}
	
	//m�todo m�nimo recibe un vector y calcula su m�nimo
	public static int minimoVector(int[] vector) {
		int min = vector[0];
		for (int i =0; i<vector.length; i++) {
			if ( vector[i] < min) {
				min=vector[i];
			}
		}
		return min;
	}

}
