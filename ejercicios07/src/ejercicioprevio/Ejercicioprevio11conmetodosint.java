package ejercicioprevio;

import java.util.Scanner;

public class Ejercicioprevio11conmetodosint {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		 int[] miVector =new int[4];
		 
		 for (int i =0; i<miVector.length; i++) {
			 System.out.println("Dame la componente " + i + " del vector");
			 miVector[i]=input.nextInt();
		 }
		 //en las llamadas no se ponen corchetes
		 sumaVector(miVector);
		 promedioVector(miVector);
		 maximoVector(miVector);
		 minimoVector(miVector);
		 
		 input.close();

	}
	//m�todo suma recibe un vector y calcula su suma
	//en los par�metros y valores de devoluci�n (cabecera) los vectores llevan corchetes
	// en el return no lleva corchetes
	public static void sumaVector(int[] vector) {
		int suma=0;
		for (int i =0; i<vector.length; i++) {
			suma+=vector[i];
		}
		System.out.println("La suma del vector es " + suma);
	}
	
	
	//m�todo promeo recibe un vector calcula su promedio
	public static void promedioVector(int[] vector) {
		int suma=0;
		for (int i =0; i<vector.length; i++) {
			suma+=vector[i];
		}
		double promedio=suma/vector.length;
		System.out.println("La suma del vector es " + promedio);
	}
	
	//m�todo m�ximmo recibe un vector y calcula su m�ximo
	public static void maximoVector(int[] vector) {
		int max = vector[0];
		for (int i =0; i<vector.length; i++) {
			if ( vector[i] > max) {
				max=vector[i];
			}
		}
		System.out.println("El m�ximo del vector es " + max);
	}
	
	//m�todo m�nimo recibe un vector y calcula su m�nimo
	public static void minimoVector(int[] vector) {
		int min = vector[0];
		for (int i =0; i<vector.length; i++) {
			if ( vector[i] < min) {
				min=vector[i];
			}
		}
		System.out.println("El m�nimo del vector es " + min);
	}

}
