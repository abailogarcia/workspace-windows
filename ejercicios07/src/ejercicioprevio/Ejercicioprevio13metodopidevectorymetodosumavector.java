package ejercicioprevio;

import java.util.Scanner;

public class Ejercicioprevio13metodopidevectorymetodosumavector {

	public static void main(String[] args) {
		// llamamos a leer datos
		double[] miVector=crearVector();
		// llamamos a sumarDatos
		System.out.println("La suma del vector es " + sumaVector(miVector));
	}

	// m�todo para leer un vector de doubles leerDatos
	public static double[] crearVector() {
		//para indiciar que va a devolver un vector de double s� se ponen corchetes
		Scanner input = new Scanner(System.in);
		System.out.println("Dame el n�mero de datos que quieres introducir");
		int numero = input.nextInt();
		double[] vector = new double[numero];
		for (int i = 0; i < vector.length; i++) {
			System.out.println("Dame la componente " + i  + " del vector");
			vector[i] = input.nextDouble();
		}
		input.close();
		return vector;
		//cuando devuelvo un vector no lleva corchetes
	}

	// m�todo para calcular la suma de los datos sumarDatos que reciba el vector
	public static double sumaVector(double[] vector) {
		double suma=0;
		for (int i =0; i< vector.length; i++) {
			suma+=vector[i];
		}
		return suma;
	}
	
}
