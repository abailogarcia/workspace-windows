package ejercicioprevio;

import java.util.Scanner;

public class Ejercicioprevio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		// creamos un vector de int llamado enteros de tama�o 4
		int[] enteros = new int[4];
		//pedimos datos por teclado
		for (int i =0; i<enteros.length; i++) {
			System.out.println("Dame un n�mero entero");
			enteros[i]= input.nextInt();
		}
		//mostramos los datos
		for (int i =0; i<enteros.length;i++) {
			System.out.println(enteros[i]);
		}
		
		input.close();

	}

}
