package ejercicio0710ordenarcaracteresdeunarray;

import java.util.Scanner;

public class Ejercicio0710ordenarcaracteresdeunarray {

	public static void main(String[] args) {
		//Programa que pida caracteres (o n�meros) por teclado y rellene un array de 10 elementos.
		//Posteriormente mostrar� dicho array. Acto seguido llamar� a un m�todo que ordenar� el
		//array de menor a mayor. Finalmente, el programa principal muestra de nuevo el array.
	Scanner input = new Scanner(System.in);
	char[] caracteres = new char[10];
	
	//relleno vector caracteres
	for ( int i =0; i<caracteres.length; i++) {
		System.out.println("Dame un car�cter");
		caracteres[i] = input.nextLine().charAt(0);
	}
	//muestro el contenido del array
	for ( int i =0; i<caracteres.length; i++) {
		System.out.print(caracteres[i] + " ");
	}
	System.out.println(" ");
	//ordenar
	ordenaCaracteres(caracteres);
	System.out.println("");
	// mostrar ordenado
	for ( int i =0; i<caracteres.length; i++) {
		System.out.print(caracteres[i] + " ");
	}
	
	
	input.close();

	}
	public static void ordenaCaracteres(char[] array) {
		//cada vez que me sit�o en una celda
		//recorro con el segundo bucle las celdas siguientes
		//en busca del valor m�s peque�o, que ir� colocando en la celda actual
		// as� sucesivamente a lo largo de todas las celdas
	//	char valorAnterior;
		
		for (int i =0;i <array.length-1; i++) {
			for (int  j= i+1; j<array.length;j++) {
				if (array[j]<array[i]) {
					char aux=array[i];
					array[i] = array[j];
					array[j]=aux;
				}
			}
			
		}
	}

}
