package ejercicio0702metodoconnumerosaleatoriosentredos;

public class Ejercicio0702metodoconnumerosaleatoriosentredos {

	public static void main(String[] args) {
		// Programa que muestra 10 n�meros almacenados en un array. En este ejercicio desde el
	//	m�todo main creamos y rellenamos un array de 10 elementos con n�meros aleatorios de 4
	//	a 15 (incluyendo ambos). Posteriormente se llamar� a un m�todo, que recibir� dicho array
	//	y solamente lo mostrar� por consola (el m�todo no devuelve nada).
		int[] aleatorios = new int[10];
		for (int i =0;i<aleatorios.length;i++) {
			aleatorios[i]=(int)(Math.random()*(15-4+1)+4);
		}
		muestraVector(aleatorios);

	}
	public static void muestraVector(int[] vector) {
		for ( int i =0; i<vector.length; i++) {
			System.out.print(vector[i] + " ");
		}
	}

}
