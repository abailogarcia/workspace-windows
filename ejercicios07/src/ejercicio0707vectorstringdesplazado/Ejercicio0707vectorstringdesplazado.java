package ejercicio0707vectorstringdesplazado;

import java.util.Scanner;

public class Ejercicio0707vectorstringdesplazado {

	public static void main(String[] args) {
		// Programa que pide un tama�o de array al usuario y posteriormente rellena ese array con
	//	Strings pidi�ndolas al usuario. Despu�s llama al m�todo desplazar(String[] array, int
	//			desplazamiento) y mueve cada string tantas posiciones adelante como indica
	//			desplazamiento. Por ejemplo, si desplazamiento es 3, el string de la posici�n 0 pasar� a la
	//			posici�n 3, y as� sucesivamente. Cuando nos pasamos de la longitud del array, debemos
	//			colocarlos al principio. Despu�s el programa principal lo debe mostrar por pantalla.
			Scanner input = new Scanner(System.in);
			System.out.println("Introduce el tama�o del vector");
			int longitud=input.nextInt();
			input.nextLine();
			String[] texto = new String[longitud];
			for ( int i =0; i<texto.length; i++) {
				System.out.println("Introduce la palabra " + i);
				texto[i] = input.nextLine();
			}
			desplazar(texto, 2);
			input.close();		
	}
	public static void desplazar(String[] cadenas, int desplazamiento) {
		desplazamiento = desplazamiento % cadenas.length;
		//String[] desplazado= new String[cadenas.length];
		for ( int i =0; i <desplazamiento; i++) {
			String aux;
			aux=cadenas[cadenas.length-1];
			//empiezo desde el final del array
			//moviendo el pen�ltimo elemento a la casilla del 1
			for ( int j = cadenas.length -1; j>0; j--) {
				cadenas[j]=cadenas[j-1];
			}
			cadenas[0] = aux;
		}
		for ( int i =0 ; i <cadenas.length; i++) {
				System.out.println(cadenas[i]);
			}
	}

}
