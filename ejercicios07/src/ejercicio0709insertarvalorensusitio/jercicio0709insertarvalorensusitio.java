package ejercicio0709insertarvalorensusitio;

import java.util.Scanner;

public class jercicio0709insertarvalorensusitio {

	public static void main(String[] args) {
		// Programa que rellene un array de 10 elementos con n�meros ordenados. (No es necesario
		//pedirlos al usuario, se pueden indicar desde el c�digo: 10 n�meros por ejemplo de 10 en
		//10). Posteriormente el programa pedir� un n�mero al usuario, y este debe insertarse en la
		//posici�n del array exacta, para que el array siga ordenado. Finalmente se muestra el
		//contenido del array
		Scanner input = new Scanner (System.in);
		int[] array = new int[10];
		for (int i =0; i<array.length; i++) {
			array[i]=i*2;
			System.out.print(array[i] + " ");
		}
		System.out.println("");
		System.out.println("Introduce el valor a insertar");
		int valor = input.nextInt();
		insertaValor(array, valor);
		for (int i=0; i<array.length;i++) {
			System.out.print(array[i] + " ");
		}
		input.close();

	}
	public static void insertaValor(int[] array, int valor) {
		//int contador=0;
		for (int i =0; i<array.length;i++) {
			if(array[i]>=valor ) {
				array[i] = valor;
				//contador++;
				return;
			}
			
			//System.out.print(array[i] + " ");
		}
		//Si no lo he insertado lo inserto en la �ltima posici�n
			array[array.length-1]=valor;
	}

}
