package ejercicio0714nombreynota;

import java.util.Scanner;

public class Ejercicio0714nombreynota {
	static Scanner input = new Scanner (System.in);
	public static void main(String[] args) {
		// Crear una aplicación que me pida el nombre de un alumno y su edad (5 alumnos). Se
		//deben almacenar en una matriz de String de 2x5. Posteriormente me mostrará los datos
		//de cada alumno y su edad.
		String[][] notas = new String[5][2];
		for (int i =0; i <notas.length; i++) {
			for (int j=0; j<notas[i].length; j++) {
				if(j ==0) {
				notas[i][j]=meteNombre();
				}
				if(j >0) {
					notas[i][j]=meteEdad();
					}
			}
		}
		for (int i =0; i <notas.length; i++) {
			for (int j=0; j<notas[i].length; j++) {
				System.out.print(notas[i][j] + " ");
			}
			System.out.println();
		}
		input.close();
	}
	public static String meteNombre(){
		
		//input.nextLine();
		System.out.println("Introduce un nombre ");
		String nombre= input.nextLine();
	
		return nombre;
	}
	public static String meteEdad(){
	
		//input.nextLine();
		System.out.println("Introduce su edad ");
		String edad= input.nextLine();
		
		return edad;
	}
}
