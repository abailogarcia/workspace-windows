package ejercicio0703metodocreavectorconletrasconsecutivas;

public class Ejercicio0703metodocreavectorconletrasconsecutivas {

	public static void main(String[] args) {
		// Crear un programa que muestra un array de caracteres. Desde el m�todo main se
		// construye y rellena un array con las letras desde la �a� a la �k�. No
		// aleatoriamente, sino que
		// la posici�n 0 del array tendr� la �a�, la posici�n 1 tendr� la �b�, y as�
		// sucesivamente hasta la
		// �k�. Posteriormente se llamar� a un m�todo que recibe el array como
		// par�metro, modifica
		// cada vocal por el car�cter �*� y lo devuelve al programa principal. Despu�s
		// el programa
		// principal mostrar� el contenido del array que ha devuelto el m�todo.
		char[] letras = new char[11];
		char caracter ='a';
		for (int i =0; i< letras.length; i++) {
			letras[i]=caracter++;
		}
		
		letras=sustituyeVocales(letras);
		
		for (int i =0; i<letras.length; i++) {
			System.out.print(letras[i] + " ");
			
		}
	}
					

	public static char[] sustituyeVocales(char[] letras) {
		
		for (int i = 0; i < letras.length; i++) {
			if (letras[i]=='a' || letras[i]=='e' || letras[i]=='i' || letras[i]=='o' || letras[i]=='u') {
				letras[i] = '*';
			}
		}
		return letras;
	}
}
