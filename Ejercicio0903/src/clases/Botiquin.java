package clases;

import java.util.ArrayList;
import java.util.Scanner;

public class Botiquin {
	private String nombre;
	private String ubicacion;
	private ArrayList<Producto> productos;
	
	public static Scanner input = new Scanner(System.in);

	public Botiquin(String nombre, String ubicacion, ArrayList<Producto> productos) {
		this.nombre = nombre;
		this.ubicacion = ubicacion;
		this.productos = productos;
	}
	public Botiquin() {
		this.nombre = "";
		this.ubicacion = "";
		this.productos = new ArrayList<Producto>();
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	public ArrayList<Producto> getProductos() {
		return productos;
	}
	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}
	public static Scanner getInput() {
		return input;
	}
	public static void setInput(Scanner input) {
		Botiquin.input = input;
	}
	
	@Override
	public String toString() {
		return "Botiquin [nombre=" + nombre + ", ubicacion=" + ubicacion + ", productos=" + productos + "]";
	}
	public void rellenarBotiquin() {
		int precioProducto;
		Producto producto = new Producto();
		System.out.println("Introduce el nombre del botiqu�n");
		this.setNombre(input.nextLine());
		
		while (true) {
			System.out.println("Por favor, introduzca el nombre del producto (* para salir)");
			String nombreProducto=input.nextLine();
			producto.setNombre(nombreProducto);
			System.out.println(producto.getNombre());
			if (producto.getNombre().equalsIgnoreCase("*")) {
				break;
			}
			//while (precioProducto != 0) {
				try {
					System.out.println("Introduce el precio del producto");
					precioProducto = input.nextInt();
					producto.setPrecio(precioProducto);
					System.out.println(producto.getPrecio());
					input.nextLine();
				} catch (Exception e) {
					System.out.println("Se ha producido un error al introducir el precio");
					precioProducto = input.nextInt();
					producto.setPrecio(precioProducto);
					input.nextLine();
				}
			//}
		}
	}
}
