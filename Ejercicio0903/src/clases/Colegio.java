package clases;

import java.util.ArrayList;
import java.util.Scanner;

public class Colegio {
	public static Scanner input = new Scanner(System.in);
	private String nombre;
	private ArrayList<Botiquin> botiquines;
	public Colegio() {
		this.nombre = "";
		this.botiquines = new ArrayList<Botiquin>();
	}
	public Colegio(String nombre, ArrayList<Botiquin> botiquines) {
		this.nombre = nombre;
		this.botiquines = botiquines;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public ArrayList<Botiquin> getBotiquines() {
		return botiquines;
	}
	public void setBotiquines(ArrayList<Botiquin> botiquines) {
		this.botiquines = botiquines;
	}
	
	@Override
	public String toString() {
		return "Colegio [nombre=" + nombre + ", botiquines=" + botiquines + "]";
	}
	public void visualizarColegio() {
		int contadorBotiquin=1;
		int contadorProducto=0;
		System.out.println();
		System.out.println("Nombre del colegio " + this.nombre);
		for(Botiquin b: this.botiquines) {
			System.out.println("Botiqu�n n�mero " + contadorBotiquin);
			System.out.println("____________________________");
			//System.out.println();
			System.out.println("Nombre " + b.getNombre());
			System.out.println("Ubicaci�n " + b.getUbicacion());
			System.out.println("Lista de productos ");
			for (Producto p: b.getProductos()) {
				System.out.print("Nombre " + p.getNombre());
				System.out.println(" Precio " + p.getPrecio());
				contadorProducto++;
			}
			System.out.println();
			contadorBotiquin++;
		}
	}
	/*public void listarProductos() {
		String nombreBot;
		for(Botiquin bot:botiquines) {
			System.out.println(bot.getNombre());
			System.out.println(bot.getProductos());
		}
	}*/
	public void generarColegio() {
		String seleccion;
		String nombreColegio;
		System.out.println("Introduzca el nombre del colegio");
		nombreColegio = input.nextLine();
		ArrayList<Botiquin> botiquines =new ArrayList<Botiquin>();
		while(true) {
			Botiquin botiquin = new Botiquin();
			botiquin.rellenarBotiquin();
			botiquines.add(botiquin);
			System.out.println(botiquin);
			System.out.println("Desea introducir m�s botiquines? si/no");
			seleccion=input.nextLine();
			if(seleccion.equals("no")) {
				break;
			}
		}
		this.setNombre(nombreColegio);
		this.setBotiquines(botiquines);
		
	}
	public void modificarBotiquin() {
		String nombreBotiquin;
		String nombreProducto;
		System.out.println("Introduzca el nombre del botiqu�n que quiere modificar");
		nombreBotiquin=input.nextLine();
		System.out.println("Introduzca el nombre del producto que quiere modificar");
		nombreProducto=input.nextLine();
		for (Botiquin b: this.getBotiquines()) {
			if(b.getNombre().equals(nombreBotiquin)) {
				for(Producto p: b.getProductos()) {
					if(nombreProducto.equals(p.getNombre())) {
						System.out.println("Introduce el nuevo nombre de producto");
						p.setNombre(input.nextLine());
						System.out.println("Dame el nuevo precio del producto " + nombreProducto);
						p.setPrecio(input.nextInt());
						input.nextLine();
						break;
					}
				}
				break;
			}
		}
	}
	public void ordenarDesBotiquin() {
		ArrayList<Botiquin> botiquines=this.getBotiquines();
		Botiquin botiquinaux = new Botiquin();
		for(int i =0; i<botiquines.size()-1;i++) {
			for(int j=0; j<botiquines.size()-1-i;j++) {
				if(botiquines.get(j).getNombre().compareToIgnoreCase(botiquines.get(j+1).getNombre())<0) {
					botiquinaux=botiquines.get(j);
					botiquines.set(j, botiquines.get(j+1));
					botiquines.set(j+1, botiquinaux);
				}
			}
		}
	}
}
