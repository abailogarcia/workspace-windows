package clases;

import java.time.LocalDate;

public class Aula {
	private Alumno[] alumnos;
	
	public Aula(int maxAlumnos) {
		this.alumnos=new Alumno[maxAlumnos]];
	}
	
	public void altaAlumno(String codAlumno, String nombre, String apellidos, String codAula, double nota) {
		for (int i =0; i<alumnos.length;i++) {
			if(alumnos[i]==null) {
				alumnos[i]=new Alumno(codAlumno);
				alumnos[i].setNombre(nombre);
				alumnos[i].setApellidos(apellidos);
				alumnos[i].setCodAula(codAula);
				alumnos[i].setNota(nota);
				alumnos[i].setFechaMatricula(LocalDate.now());
				break;
			}
		}
	}
	public Alumno buscarAlumno(String codAlumno) {
		for (int i =0; i<alumnos.length;i++) {
			if(alumnos[i]!=null) {
				if(alumnos[i].getCodAlumno().equals(codAlumno)) {
					return alumnos[i];
				}
			}
		}
		return null;
	}
	public void listarAlumnoPorAula(String codAula) {
		for (int i =0; i<alumnos.length;i++) {
			if(alumnos[i]!=null) {
				if(alumnos[i].getCodAula().equals(codAula)) {
					System.out.println(alumnos[i]);
				}
			}
		}
	}
	public void contarAlumnosAprobados() {
		int contador =0;
		for (int i =0; i<alumnos.length;i++) {
			if(alumnos[i]!=null) {
				if(alumnos[i].getNota()>=5) {
					contador++;
				}
			}
		}
		System.out.println("El total de aprobador es " + contador);
	}
	public void notasAlumnos(){
		double suma =0;
		double max=0;
		double min=10;
		for (int i =0; i<alumnos.length;i++) {
			if(alumnos[i]!=null) {
				suma+=alumnos[i].getNota();			
				if(alumnos[i].getNota()>max) {
					max=alumnos[i].getNota();
				}
				if(alumnos[i].getNota()<min) {
					min=alumnos[i].getNota();
				}
			}
		}
		System.out.println("La nota m�xima es " + max);
		System.out.println("La nota m�nima es " + min);
		System.out.println("La nota media es " + suma/alumnos.length);
	}
}
