package clases;

import java.time.LocalDate;

public class Alumno {
	private String codAlumno;
	private String nombre;
	String apellidos;
	String codAula;
	double nota;
	LocalDate fechaMatricula;
	 public Alumno(String codAlumno) {
		 this.codAlumno=codAlumno;
	 }
	/**
	 * @return the codAlumno
	 */
	public String getCodAlumno() {
		return codAlumno;
	}
	/**
	 * @param codAlumno the codAlumno to set
	 */
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the apellidos
	 */
	public String getApellidos() {
		return apellidos;
	}
	/**
	 * @param apellidos the apellidos to set
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	/**
	 * @return the codAula
	 */
	public String getCodAula() {
		return codAula;
	}
	/**
	 * @param codAula the codAula to set
	 */
	public void setCodAula(String codAula) {
		this.codAula = codAula;
	}
	/**
	 * @return the nota
	 */
	public double getNota() {
		return nota;
	}
	/**
	 * @param nota the nota to set
	 */
	public void setNota(double nota) {
		this.nota = nota;
	}
	/**
	 * @return the fechaMatricula
	 */
	public LocalDate getFechaMatricula() {
		return fechaMatricula;
	}
	/**
	 * @param fechaMatricula the fechaMatricula to set
	 */
	public void setFechaMatricula(LocalDate fechaMatricula) {
		this.fechaMatricula = fechaMatricula;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Alumno [codAlumno=" + codAlumno + ", nombre=" + nombre + ", apellidos=" + apellidos + ", codAula="
				+ codAula + ", nota=" + nota + ", fechaMatricula=" + fechaMatricula + "]";
	}
	
}
