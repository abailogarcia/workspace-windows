package programa;

import clases.TallerVehiculos;

public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("1.- Crear una instancia de llamada con 4 albaranes");
		int maxVehiculos=4;
		TallerVehiculos taller = new TallerVehiculos(maxVehiculos);
		System.out.println("Instancia creada");
		
		System.out.println("2.- Dar de alta 4 alabaranes distintos");
		System.out.println("Doy de alta A1-A2-A3-A4");
		taller.altaAlbaran("A1", 20, "V1");
		taller.altaAlbaran("A2", 23.10, "V1");
		taller.altaAlbaran("A3", 25.99, "V1");
		taller.altaAlbaran("A4", 16.99, "V2");
		
		System.out.println("3.- Listar los albaranes");
		taller.listarAlbaranes();
		
		System.out.println("4.- Buscar albar�n");
		System.out.println("Buscamos el albar�n A2");
		System.out.println(taller.buscarAlbaran("A2"));
		
		System.out.println("5.- Eliminar albar�n");
		taller.eliminarAlbaran("A2");
		taller.listarAlbaranes();
		
		System.out.println("6.- Dar de alta un nuevo albar�n");
		System.out.println("Doy de alta el A5");
		taller.altaAlbaran("A5", 27.33, "V2");
		taller.listarAlbaranes();
		
		System.out.println("7.- Modificar el codVehiculo de un albar�n");
		System.out.println("Al albar�n A4 le ponemos el veh�culo V8");
		taller.cambiarAlbaran("A4", "V8");
		taller.listarAlbaranes();
		
		System.out.println("8.- Listar los albaranes de un veh�culo");
		System.out.println("Listamos los albaranes del veh�culo V1");
		taller.listarAlbaranPorVehiculo("V1");
		
	}

}
