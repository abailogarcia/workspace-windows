package clases;

import java.time.LocalDate;

public class TallerVehiculos {
	//atributos
	private Albaran[] albaranes;
	
	//constructor
	public TallerVehiculos(int maxAlbaranes) {
		this.albaranes=new Albaran[maxAlbaranes];
	}
	//m�todos
	//alta albar�n
	public void altaAlbaran(String codAlbaran, double precio, String codVehiculo) {
		for(int i=0; i<albaranes.length;i++) {
			if(albaranes[i]==null) {
				albaranes[i]=new Albaran(codAlbaran);
				albaranes[i].setPrecio(precio);
				albaranes[i].setCodVehiculo(codVehiculo);
				albaranes[i].setFecha(LocalDate.now());
				break;
			}
		}
	}
	//listar albaranes
	public void listarAlbaranes() {
		for (int i=0; i<albaranes.length;i++) {
			if(albaranes[i]!=null) {
				System.out.println(albaranes[i]);
			}
		}
			
	}
	//eliminar albar�n
	public void eliminarAlbaran(String codAlbaran) {
		for (int i =0; i<albaranes.length;i++) {
			if(albaranes[i]!=null) {
					if(albaranes[i].getCodAlbaran().equals(codAlbaran)) {
						albaranes[i]=null;
						System.out.println("Se ha eliminado el albar�n " + codAlbaran);
					}
			}
		}
	}
	//buscar albar�n
	public Albaran buscarAlbaran(String codAlbaran) {
		for (int i =0; i<albaranes.length;i++) {
			if(albaranes[i]!=null) {
				if(albaranes[i].getCodAlbaran().equals(codAlbaran)) {
					return albaranes[i];
				}
				
			}
		}
		return null;
	}
	//modificar codVehiculo de un albaran
	public void cambiarAlbaran(String codAlbaran, String codVehiculo2) {
		Albaran encontrado=buscarAlbaran(codAlbaran);
		encontrado.setCodVehiculo(codVehiculo2);
	}
	//Listar los albaranes de un veh�culo
	public void listarAlbaranPorVehiculo(String codVehiculo) {
		for (int i=0; i<albaranes.length;i++) {
			if(albaranes[i]!=null) {
				if (albaranes[i].getCodVehiculo().equals(codVehiculo)) {
					System.out.println(albaranes[i]);
				}
				
			}
		}
	}
	
}
