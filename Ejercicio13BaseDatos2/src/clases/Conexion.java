package clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
	static final String SERVIDOR = "jdbc:mysql://localhost:3306/transporte3ev";
	static final String USER = "root";
	static final String PASS = "";
	static Connection conexion;
	
	public static Connection getConnection() throws SQLException {
		conexion = DriverManager.getConnection(SERVIDOR, USER, PASS);
		return conexion;
	}
}
