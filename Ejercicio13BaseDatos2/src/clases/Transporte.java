package clases;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class Transporte implements Comparable<Transporte>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private String codigo;
	private String empresa;
	private String fechaTransporte;
	private Conductor conductor;
	protected ArrayList<Carretillero> listaCarretilleros;
	
	public Transporte(String codigo, String empresa, String fechaTransporte, Conductor conductor) {
		this.codigo = codigo;
		this.empresa = empresa;
		this.fechaTransporte = fechaTransporte;
		this.conductor = conductor;
		listaCarretilleros = new ArrayList<Carretillero>();
	}
	/*public Transporte(String codigo, String empresa, String fechaTransporte, String dniConductor) {
		this.codigo = codigo;
		this.empresa = empresa;
		this.fechaTransporte = fechaTransporte;
		listaCarretilleros = new ArrayList<Carretillero>();
	}*/
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getFechaTransporte() {
		return fechaTransporte;
	}

	public void setFechaTransporte(String fechaTransporte) {
		this.fechaTransporte = fechaTransporte;
	}

	public Conductor getConductor() {
		return conductor;
	}

	public void setConductor(Conductor conductor) {
		this.conductor = conductor;
	}

	public ArrayList<Carretillero> getListaCarretilleros() {
		return listaCarretilleros;
	}

	public void setListaCarretilleros(ArrayList<Carretillero> listaCarretilleros) {
		this.listaCarretilleros = listaCarretilleros;
	}

	@Override
	public int compareTo(Transporte o) {
		return getFechaTransporte().compareTo(o.getFechaTransporte());
	}

	@Override
	public String toString() {
		return "Transporte [codigo=" + codigo + ", empresa=" + empresa + ", fechaTransporte=" + fechaTransporte
				+ ", conductor=" + conductor + ", listaCarretilleros=" + listaCarretilleros + "]";
	}
	
}
