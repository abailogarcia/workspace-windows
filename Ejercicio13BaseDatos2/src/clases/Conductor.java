package clases;

import java.io.Serializable;

public class Conductor extends Personal implements Serializable{

	private static final long serialVersionUID = 1L;

	private String carne;
	
	public Conductor(String dni, String nombre, String carne) {
		super(dni, nombre);
		this.carne = carne;
	}

	public String getCarne() {
		return carne;
	}

	public void setCarne(String carne) {
		this.carne = carne;
	}

	@Override
	public int compareTo(Personal o) {
		return getDni().compareTo(o.getDni());
	}
	
	@Override
	public String toString() {
		return "Conductor [carne=" + carne + ", toString()=" + super.toString() + "]";
	}
}
