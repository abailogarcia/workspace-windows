package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

//import com.mysql.jdbc.Connection;

public class EmpresaTransporte {
	private ArrayList<Personal> listaPersonal;
	private ArrayList<Transporte> listaTransporte;

	public EmpresaTransporte() {
		listaPersonal = new ArrayList<Personal>();
		listaTransporte = new ArrayList<Transporte>();
	}

	public ArrayList<Personal> getListaPersonal() {
		return listaPersonal;
	}

	public void setListaPersonal(ArrayList<Personal> listaPersonal) {
		this.listaPersonal = listaPersonal;
	}

	public ArrayList<Transporte> getListaTransporte() {
		return listaTransporte;
	}

	public void setListaTransporte(ArrayList<Transporte> listaTransporte) {
		this.listaTransporte = listaTransporte;
	}

	/*
	 * public void altaPersonal(String dni, String nombre) { Personal persona = new
	 * Personal(dni, nombre); listaPersonal.add(persona); }
	 */
	public void altaConductor(String dni, String nombre, String carne) {
		Conductor conductor = new Conductor(dni, nombre, carne);
		listaPersonal.add(conductor);
	}

	public Conductor buscaConductor(String dni) {
		for (Personal persona : listaPersonal) {
			if (persona != null && persona instanceof Conductor && persona.getDni().equals(dni)) {
				return (Conductor) persona;
			}
		}
		return null;
	}
	public ArrayList<Conductor> buscaConductores() {
		ArrayList<Conductor> listaConductores = new ArrayList<Conductor>();
		//for(Conductor persona: listaPersonal) {
		for (Personal persona : listaPersonal) {
			if (persona != null && persona instanceof Conductor) {
				//persona = (Conductor)persona;
				listaConductores.add((Conductor)persona);
			}
		}
		return listaConductores;
	}
	public Carretillero buscaCarretillero(String dni) {
		for (Personal persona : listaPersonal) {
			if (persona != null && persona instanceof Carretillero && persona.getDni().equals(dni)) {
				return (Carretillero) persona;
			}
		}
		return null;
	}
	public boolean existeConductor(String dni) {
		for (Personal persona : listaPersonal) {
			if (persona != null && persona instanceof Conductor&& persona.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean existeCarretillero(String dni) {
		for (Personal persona : listaPersonal) {
			if (persona != null && persona instanceof Carretillero&& persona.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}
	public void altaCarretillero(String dni, String nombre, double costeHora) {
		Carretillero carretillero = new Carretillero(dni, nombre, costeHora);
		listaPersonal.add(carretillero);
	}

	public void listarPersonal() {
		for (Personal persona : listaPersonal) {
			if (persona != null) {
				System.out.println(persona);
			}
		}
	}

	public void listarConductores() {
		for (Personal persona : listaPersonal) {
			if (persona != null && persona instanceof Conductor) {
				System.out.println(persona);
			}
		}
	}

	public void listarCarretilleros() {
		for (Personal persona : listaPersonal) {
			if (persona != null && persona instanceof Carretillero) {
				System.out.println(persona);
			}
		}
	}

	public void altaTransporte(String codigo, String empresa, String fechaTransporte, String dniConductor) {
		if (!existeTransporte(codigo)) {
			if (existeConductor(dniConductor)) {
				Transporte transporte = new Transporte(codigo, empresa, fechaTransporte, buscaConductor(dniConductor));
				listaTransporte.add(transporte);
			}else {
				System.out.println("Ese no es un conductor");
			}
		}else {
			System.out.println("Ese transporte ya existe");
		}
	}
	
	public void asignarCarretilleroTransporte(String codigo, String dni) {
		if(existeTransporte(codigo)) {
			if(existeCarretillero(dni)) {
				buscaTransporte(codigo).getListaCarretilleros().add(buscaCarretillero(dni));
			}else {
				System.out.println("El carretillero no existe");
			}
		}else {
			System.out.println("El transporte no existe");
		}
	}
	
	public Transporte buscaTransporte(String codigo) {
		for (Transporte transporte : listaTransporte) {
			if (transporte != null && transporte.getCodigo().equals(codigo)) {
				return transporte;
			}
		}
		return null;
	}

	public boolean existeTransporte(String codigo) {
		for (Transporte transporte : listaTransporte) {
			if (transporte != null && transporte.getCodigo().equals(codigo)) {
				return true;
			}
		}
		return false;
	}
	public void listarTransportes() {
		for (Transporte transporte : listaTransporte) {
			if (transporte != null ) {
				System.out.println(transporte);;
			}
		}
	}
	//Guardar datos en fichero
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datos.dat")));
			escritor.writeObject(listaPersonal);
			escritor.writeObject(listaTransporte);
			escritor.close();
		}catch(IOException e) {
			System.out.println("Error de entrada/salida");
		}
	}
	//cargar datos de fichero
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream lector = new ObjectInputStream(new FileInputStream("src/datos.dat"));
			listaPersonal = (ArrayList<Personal>) lector.readObject();
			listaTransporte= (ArrayList<Transporte>) lector.readObject();
			lector.close();
		}catch(IOException e) {
			System.out.println("Error de entrada/salida");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	//guardar conductores en la tabla conductores
	public void guardarConductoresBD() throws SQLException {
		Connection con=null;
		PreparedStatement ps=null;
		try {
			con=Conexion.getConnection();
			if(con==null) {
				System.out.println("Error de conexi�n con la base de datos");
				return;
			}
			con.setAutoCommit(false);
			ps=con.prepareStatement("INSERT INTO conductores (ID, DNI, NOMBRE, CARNE) VALUES (?,?,?,?)");
			for(Conductor conduc: buscaConductores()) {
				ps.setString(2, conduc.getDni());
				ps.setString(3, conduc.getNombre());
				ps.setString(4, conduc.getCarne());
				ps.executeUpdate();
				con.commit();
			}
			
		}catch(SQLException e) {
			throw e;
		}finally {
			ps.close();
			con.close();
		}
	}
	
	@SuppressWarnings("null")
	public void cargarConductoresBD() throws SQLException {
		Connection con =null;
		PreparedStatement ps = null;
		ResultSet resultado=null;
		try {
			ps=con.prepareStatement("SELECT * FROM conductores" );
			resultado = ps.executeQuery();
			while(resultado.next()) {
				altaConductor(resultado.getString(2), resultado.getString(3), resultado.getString(4));
			}
		}catch(SQLException e) {
				throw e;
		}finally {
				ps.close();
				con.close();
				resultado.close();
		}
	}
}
