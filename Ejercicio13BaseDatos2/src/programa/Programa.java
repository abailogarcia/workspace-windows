package programa;
import java.sql.SQLException;

import clases.EmpresaTransporte;
public class Programa {

	public static void main(String[] args) throws SQLException {
		EmpresaTransporte empresa = new EmpresaTransporte();
	//empresa.cargarDatos();
		empresa.listarPersonal();
		System.out.println("Creamos conductores y carretilleros");
		empresa.altaCarretillero("ca1", "Paco", 2.5);
		empresa.altaCarretillero("ca2", "Juan", 2);
		empresa.altaCarretillero("ca3", "Ana", 2.7);
		/*empresa.altaConductor("co1", "Mario", "b1");
		empresa.altaConductor("co2", "Carla", "b2");
		empresa.altaConductor("co3", "Raul", "b1");*/
		empresa.listarPersonal();
		System.out.println();
		System.out.println("Listamos solo conductores");
		empresa.listarConductores();
		System.out.println();
		System.out.println("Listamos solo carretilleros");
		empresa.listarCarretilleros();
		System.out.println();
		System.out.println("Creamos transportes");
		empresa.altaTransporte("t1", "saica", "2022-12-01", "co1");
		empresa.altaTransporte("t2", "opel", "2022-03-05", "co2");
		empresa.listarTransportes();
		System.out.println();
		System.out.println("Asignamos carretilleros a transporte");
		empresa.asignarCarretilleroTransporte("t1", "ca1");
		empresa.asignarCarretilleroTransporte("t1", "ca2");
		empresa.asignarCarretilleroTransporte("t2", "ca3");
		empresa.listarTransportes();
		System.out.println("");
		System.out.println("Guardamos datos");
		//empresa.guardarDatos();
		//empresa.guardarConductoresBD();
		empresa.cargarConductoresBD();
		empresa.listarConductores();
	}

}
