package programa;

import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.Scanner;
import clases.GestorCursos;

/**
 * 
 * @author angel
 *
 */
public class Menu {
	static private Scanner input = new Scanner(System.in);
	static private GestorCursos academia = new GestorCursos();

	/**
	 * crearCurso controlamos el int de la duraci�n con try catch
	 */
	public static void crearCurso() {
		try {
			System.out.println("Introduce el c�digo del curso alfanum�rico");
			String codigo = input.nextLine();
			if (!academia.comprobarCurso(codigo)) {
				System.out.println("Introduce el nombre del curso");
				String nombre = input.nextLine();
				System.out.println("Introduce la duraci�n del curso en horas");
				int duracion = input.nextInt();
				input.nextLine();

				System.out.println("�Es un curso con ordenador? S/N");
				char tipo = input.nextLine().charAt(0);
				if (tipo == 's' || tipo == 'S') {
	/**
	* Submen� con enum
	*/
					boolean salir = false;
					int opcionOrdenador;
					while (!salir) {
						System.out.println("Seleccione una de las siguientes opciones");
						System.out.println("1.- Ordenadores con Windows");
						System.out.println("2.- Ordenadores con Linux");
						System.out.println("3.- Ordenadores con Apple");
						System.out.println("4.- Volver al men� principal");
						opcionOrdenador = input.nextInt();
						input.nextLine();
						switch (opcionOrdenador) {
						case 1:
							academia.altaCursoConOrdenadorWindows(codigo, nombre, duracion);
							salir = true;
							break;
						case 2:
							academia.altaCursoConOrdenadorLinux(codigo, nombre, duracion);
							salir = true;
							break;
						case 3:
							academia.altaCursoConOrdenadorApple(codigo, nombre, duracion);
							salir = true;
							break;
						case 4:
							salir = true;
							break;
						default:
							System.out.println("Introduce un n�mero entre 1 y 4");
						}
						System.out.println("Curso con ordenador creado");
						System.out.println(academia.buscarCursoConOrdenador(codigo));
					}
				} else if (tipo == 'n' || tipo == 'N') {
					academia.altaCursoSinOrdenador(codigo, nombre, duracion);
					System.out.println("Curso sin ordenador creado");
					System.out.println(academia.buscarCursoSinOrdenador(codigo));
				} else {
					System.out.println("Error, introduce S o N");
				}
			} else {
				System.out.println("El curso ya existe");
				System.out.println();
			}
		} catch (InputMismatchException e1) {
			System.err.println("Tienes que introducir un entero");
		}

	}

	/**
	 * crearAlumno controlamos la fecha de nacimiento con try catch
	 */
	public static void crearAlumno() {
		try {
			System.out.println("Introduce el dni del alumno");
			String dni = input.nextLine();
			if (!academia.comprobarAlumno(dni)) {
				System.out.println("Introduce el nombre del alumno");
				String nombreAlumno = input.nextLine();
				System.out.println("Intrroduce la fecha de nacimiento del alumno");
				String fechaNacimiento = input.nextLine();
				academia.altaAlumno(dni, nombreAlumno, fechaNacimiento);
				System.out.println("Alumno creado");
				System.out.println(academia.buscarAlumnoConFor(dni));
			} else {
				System.out.println("El alumno ya existe");
				System.out.println();
			}
		} catch (DateTimeParseException e2) {
			System.err.println("Fecha mal introducida. El formato correcto es AAAA-MM-DD");
		}
	}

	/**
	 * inscribirAlumno
	 */
	public static void inscribirAlumno() {
		System.out.println("Introduce el dni del alumno");
		String dni = input.nextLine();
		if (academia.comprobarAlumno(dni)) {
			System.out.println("Introduce el c�digo del curso alfanum�rico");
			String codigo = input.nextLine();
			if (academia.comprobarCurso(codigo)) {
				if (academia.comprobarAlumnoCurso(dni, codigo)) {
					academia.inscribirAlumnoCurso(dni, codigo);
					System.out.println("Se ha inscrito al alumno " + dni + " en el curso " + codigo);
					System.out.println(academia.buscarCurso(codigo));
				} else {
					System.out.println("El alumno " + dni + " ya est� inscrito en el curso " + codigo);
					System.out.println();
				}
			} else {
				System.out.println("El curso no existe");
				System.out.println();
			}
		} else {
			System.out.println("El alumno no existe");
			System.out.println();
		}
	}

	/**
	 * desinscribirAlumno
	 */
	public static void desinscribirAlumno() {
		System.out.println("Introduce el dni del alumno");
		String dni = input.nextLine();
		if (academia.comprobarAlumno(dni)) {
			System.out.println("Introduce el c�digo del curso alfanum�rico");
			String codigo = input.nextLine();
			if (academia.comprobarCurso(codigo)) {
				if (!academia.comprobarAlumnoCurso(dni, codigo)) {
					academia.desinscribirAlumnoCurso(dni, codigo);
					System.out.println("Se ha desinscrito al alumno " + dni + " del curso " + codigo);
					System.out.println(academia.buscarCurso(codigo));
				} else {
					System.out.println("El alumno " + dni + " ya no estaba inscrito en el curso " + codigo);
					System.out.println();
				}
			} else {
				System.out.println("El curso no existe");
				System.out.println();
			}
		} else {
			System.out.println("El alumno no existe");
			System.out.println();
		}
	}

	/**
	 * gestionarCursos
	 */
	public static void gestionarCursos() {
		boolean salir = false;
		int opcionCurso;
		while (!salir) {
			System.out.println("Seleccione una de las siguientes opciones");
			System.out.println("1.- Crear un curso");
			System.out.println("2.- Modificar un curso");
			System.out.println("3.- Borrar un curso");
			System.out.println("4.- Listar un curso");
			System.out.println("5.- Volver al men� principal");
			opcionCurso = input.nextInt();
			input.nextLine();
			switch (opcionCurso) {
			case 1:
				crearCurso();
				break;
			case 2:
				boolean exit = false;
				/**
				 * submen� de modificar curso
				 */
				while (!exit) {
					System.out.println("Seleccione una de las siguientes opciones");
					System.out.println("1.- Cambiar el c�digo del curso");
					System.out.println("2.- Cambiar el nombre del curso");
					System.out.println("3.- Cambiar la duraci�n del curso");
					System.out.println("4.- Listar un curso");
					System.out.println("5.- Volver al men� anterior");
					opcionCurso = input.nextInt();
					input.nextLine();
					switch (opcionCurso) {
					case 1:
						System.out.println("Introduce el c�digo del curso a modificar el c�digo");
						String codigo = input.nextLine();
						if (academia.comprobarCurso(codigo)) {
							System.out.println("Introduce el nuevo c�digo del curso");
							String nuevoCodigo = input.nextLine();
							if (!academia.comprobarCurso(nuevoCodigo)) {
								academia.cambiarCodigoCurso(codigo, nuevoCodigo);
								System.out.println("El nuevo c�digo del curso es " + nuevoCodigo);
							} else {
								System.out.println("Ese curso ya existe, tendr�s que pensar otro c�digo de curso");
								System.out.println();
								break;
							}
						} else {
							System.out.println("Ese curso no existe");
							System.out.println();
							break;
						}
						break;
					case 2:
						System.out.println("Introduce el c�digo del curso a modificar el nombre");
						codigo = input.nextLine();
						if (academia.comprobarCurso(codigo)) {
							System.out.println("Introduce el nuevo nombre del curso " + codigo);
							String nuevoNombre = input.nextLine();
							academia.cambiarNombreCurso(codigo, nuevoNombre);
							System.out.println("El nuevo nombre del curso " + codigo + " es " + nuevoNombre);
						} else {
							System.out.println("Ese curso no existe");
							System.out.println();
							break;
						}
						break;
					case 3:
						System.out.println("Introduce el c�digo del curso a modificar la duraci�n");
						codigo = input.nextLine();
						if (academia.comprobarCurso(codigo)) {
							System.out.println("Introduce la nueva duraci�n del curso " + codigo);
							int nuevaDuracion = input.nextInt();
							academia.cambiarDuracionCurso(codigo, nuevaDuracion);
							System.out.println("La nueva duracion del curso " + codigo + " es " + nuevaDuracion);
						} else {
							System.out.println("Ese curso no existe");
							System.out.println();
							break;
						}
						break;
					case 4:
						System.out.println("Introduce el codigo del curso a listar");
						codigo = input.nextLine();
						academia.listarUnCurso(codigo);
						break;
					case 5:
						exit = true;
						break;
					default:
						System.out.println("Introduce un n�mero entre 1 y 5");
					}
				}
				break;
			case 3:
				System.out.println("Introduce el c�digo del curso a borrar");
				String codigo = input.nextLine();
				if (academia.comprobarCurso(codigo)) {
					academia.eliminarCurso(codigo);
					System.out.println("Se ha eliminado el curso " + codigo);
				} else {
					System.out.println("Ese curso no existe");
					System.out.println();
					break;
				}
				break;
			case 4:
				System.out.println("Introduce el codigo del curso a listar");
				codigo = input.nextLine();
				academia.listarUnCurso(codigo);
				break;
			case 5:
				salir = true;
				break;
			default:
				System.out.println("Introduce un n�mero entre 1 y 5");
			}
		}
	}

	/**
	 * gestionarAlumnos
	 */
	public static void gestionarAlumnos() {
		boolean salir = false;
		int opcionAlumno;
		while (!salir) {
			System.out.println("Seleccione una de las siguientes opciones");
			System.out.println("1.- Crear un alumno");
			System.out.println("2.- Modificar un alumno");
			System.out.println("3.- Borrar un alumno");
			System.out.println("4.- Inscribir un alumno en un curso");
			System.out.println("5.- Desinscribir un alumno de un curso");
			System.out.println("6.- Listar un alumno");
			System.out.println("7.- Volver al men� principal");
			opcionAlumno = input.nextInt();
			input.nextLine();
			switch (opcionAlumno) {
			case 1:
				crearAlumno();
				break;
			case 2:
	/**
	* submen� de modificar alumno
	*/
				boolean exit = false;
				while (!exit) {
					System.out.println("Seleccione una de las siguientes opciones");
					System.out.println("1.- Cambiar el dni del alumno");
					System.out.println("2.- Cambiar el nombre del alumno");
					System.out.println("3.- Cambiar la fecha de nacimiento del alumno");
					System.out.println("4.- Listar un alumno");
					System.out.println("5.- Volver al men� anterior");
					opcionAlumno = input.nextInt();
					input.nextLine();
					switch (opcionAlumno) {
					case 1:
						System.out.println("Introduce el dni del alumno a modificar el dni");
						String dni = input.nextLine();
						if (academia.comprobarAlumno(dni)) {
							System.out.println("Introduce el nuevo dni del alumno");
							String nuevoDni = input.nextLine();
							if (!academia.comprobarAlumno(nuevoDni)) {
								academia.cambiarAlumnoDni(dni, nuevoDni);
								System.out.println("El nuevo dni del alumno es " + nuevoDni);
							} else {
								System.out.println(
										"Ese dni ya est� en el sistema, comprueba que el nuevo dni es correcto");
								System.out.println();
								break;
							}
						} else {
							System.out.println("Ese alumno no existe");
							System.out.println();
							break;
						}
						break;
					case 2:
						System.out.println("Introduce el dni del alumno a modificar el nombre");
						dni = input.nextLine();
						if (academia.comprobarAlumno(dni)) {
							System.out.println("Introduce el nuevo nombre del alumno " + dni);
							String nuevoNombre = input.nextLine();
							academia.cambiarAlumnoNombre(dni, nuevoNombre);
							System.out.println("El nuevo nombre del alumno " + dni + " es " + nuevoNombre);
						} else {
							System.out.println("Ese alumno no existe");
							System.out.println();
							break;
						}
						break;
					case 3:
						System.out.println("Introduce el dni del alumno a modificar la fecha de nacimiento");
						dni = input.nextLine();
						if (academia.comprobarAlumno(dni)) {
							System.out.println("Introduce la nueva fecha de nacimiento del alumno " + dni);
							String nuevaFecha = input.nextLine();
							academia.cambiarAlumnoFechaNacimiento(dni, nuevaFecha);
							System.out.println("La nueva fecha de nacimiento del alumno " + dni + " es " + nuevaFecha);
						} else {
							System.out.println("Ese alumno no existe");
							System.out.println();
							break;
						}
						break;
					case 4:
						System.out.println("Introduce el dni del alumno a listar");
						dni = input.nextLine();
						academia.listarUnAlumno(dni);
						break;
					case 5:
						exit = true;
						break;
					default:
						System.out.println("Introduce un n�mero entre 1 y 5");
					}
				}
				break;
			case 3:
				System.out.println("Introduce el dni del alumno a borrar");
				String dni = input.nextLine();
				if (academia.comprobarAlumno(dni)) {
					academia.eliminarAlumno(dni);
					System.out.println("Se ha eliminado el alumno " + dni);
				} else {
					System.out.println("Ese alumno no existe");
					System.out.println();
					break;
				}
				break;
			case 4:
				inscribirAlumno();
				break;
			case 5:
				desinscribirAlumno();
				break;
			case 6:
				System.out.println("Introduce el dni del alumno a listar");
				dni = input.nextLine();
				academia.listarUnAlumno(dni);
				break;
			case 7:
				salir = true;
				break;
			default:
				System.out.println("Introduce un n�mero entre 1 y 6");
			}
		}
	}
/**
 * listados
 */
	public static void listados() {
		boolean salir = false;
		int opcionListado;
		while (!salir) {
			System.out.println("Seleccione una de las siguientes opciones");
			System.out.println("1.- Listar todos los cursos");
			System.out.println("2.- Listar un curso");
			System.out.println("3.- Listar todos los cursos con ordenador");
			System.out.println("4.- Listar todos los cursos sin ordenador");
			System.out.println("5.- Listar todos los alumnos");
			System.out.println("6.- Listar un alumno");
			System.out.println("7.- Listar  alumnos por curso");
			System.out.println("8.- Listar  cursos de un alumno");
			System.out.println("9.- Listar  cursos por tipo de aula");
			System.out.println("10.- Volver al men� principal");
			opcionListado = input.nextInt();
			input.nextLine();
			switch (opcionListado) {
			case 1:
				System.out.println("Listado de todos los cursos de la academia");
				academia.listarCursos();
				System.out.println();
				break;
			case 2:
				System.out.println("Introduce el codigo del curso a listar");
				String codigo = input.nextLine();
				if (academia.comprobarCurso(codigo)) {
					academia.listarUnCurso(codigo);
					System.out.println();
				} else {
					System.out.println("El curso " + codigo + " no existe");
				}
				break;
			case 3:
				System.out.println("Listado de todos los cursos con ordenador de la academia");
				academia.listarCursosCon();
				System.out.println();
				break;
			case 4:
				System.out.println("Listado de todos los cursos sin ordenador de la academia");
				academia.listarCursosSin();
				System.out.println();
				break;
			case 5:
				System.out.println("Listado de todos los alumnos de la academia");
				academia.listarAlumnos();
				System.out.println();
				break;
			case 6:
				System.out.println("Introduce el dni del alumno a listar");
				String dni = input.nextLine();
				if (academia.comprobarAlumno(dni)) {
					academia.listarUnAlumno(dni);
					System.out.println();
				} else {
					System.out.println("El alumno " + dni + " no existe");
				}
				break;
			case 7:
				System.out.println("Introduce el codigo del curso a listar sus alumnos");
				codigo = input.nextLine();
				if (academia.comprobarCurso(codigo)) {
					academia.listarAlumnosPorCurso(codigo);
				} else {
					System.out.println("El curso " + codigo + " no existe");
				}
				break;
			case 8:
				System.out.println("Introduce el dni del alumno a listar sus cursos");
				dni = input.nextLine();
				if (academia.comprobarAlumno(dni)) {
					academia.listaCursosAlumno(dni);
				} else {
					System.out.println("El alumno " + dni + " no existe");
				}
				break;
			case 9:
				System.out.println("Introduce el tipo de ordenador (windows, linux, apple)");
				String tipo = input.nextLine();
				academia.listarCursoPorTipoAula(tipo);
				System.out.println();
				break;
			case 10:
				salir = true;
				break;
			default:
				System.out.println("Introduce un n�mero entre 1 y 10");
			}
		}
		// no puedo cerrar el Scanner porque si no me da
		// java.util.NoSuchElementException
		// input.close();
	}

}
