package programa;

import java.util.Scanner;
import clases.Curso;
import clases.GestorCursos;
import programa.Menu;
/**
 * 
 * @author angel
 *
 */
public class Programa {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		//GestorCursos academia = new GestorCursos();
		/**
		 * Creaci�n de men� con while y switch
		 */
		boolean salir = false;
		int opcion;
		while (!salir) {

			System.out.println("1.- Crear curso");
			System.out.println("2.- Crear alumno");
			System.out.println("3.- Inscribir  alumno en curso");
			System.out.println("4.- Gestionar cursos");
			System.out.println("5.- Gestionar  alumnos");
			System.out.println("6.- Listados");
			System.out.println("7.- Salir");
			System.out.println("Selecciona una de las opciones");
			opcion = input.nextInt();
			input.nextLine();
			switch (opcion) {
			case 1:
				Menu.crearCurso();
				break;
			case 2:
				Menu.crearAlumno();
				break;
			case 3: 
				Menu.inscribirAlumno();
				break;
			case 4:
				Menu.gestionarCursos();
				break;
			case 5:
				Menu.gestionarAlumnos();
				break;
			case 6:
				Menu.listados();
				break;
			case 7:
				salir=true;
				System.out.println("Se acab�");
				break;
			default:
				System.out.println("Introduce un n�mero entre 1 y 7");
				break;
			}

		}
		input.close();
	}
}
