package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * 
 * @author angel
 *
 */
public class GestorCursos {
	/**
	 * declaro los arraylist
	 */
	private ArrayList<Alumno> listaAlumnos;
	private ArrayList<Curso> listaCursos;

	/**
	 * @param listaAlumnos
	 * @param listaCursos
	 * @param listaCursosSin
	 * @param listaCursosCon
	 */
	/**
	 * 
	 */
	public GestorCursos() {
		listaAlumnos = new ArrayList<Alumno>();
		listaCursos = new ArrayList<Curso>();
		// listaCursosSin = new ArrayList<CursoSinOrdenador>();
		// listaCursosCon = new ArrayList<CursoConOrdenador>();
	}

	/**
	 * alta alumno
	 * 
	 * @param dni
	 * @param nombre
	 * @param fechaNacimiento
	 */
	public void altaAlumno(String dni, String nombre, String fechaNacimiento) {
		Alumno nuevoAlumno = new Alumno(dni, nombre);
		nuevoAlumno.setFechaNacimiento(LocalDate.parse(fechaNacimiento));
		listaAlumnos.add(nuevoAlumno);
	}

	/**
	 * buscar alumno
	 * 
	 * @param dni
	 * @return
	 */
	public Alumno buscarAlumno(String dni) {
		for (Alumno alum : listaAlumnos) {
			if (alum != null && alum.getDni().equals(dni)) {
				return alum;
			}
		}
		return null;
	}

	/**
	 * buscar alumno con for
	 * 
	 * @param dni
	 * @return
	 */
	public Alumno buscarAlumnoConFor(String dni) {
		for (int i = 0; i < listaAlumnos.size(); i++) {
			if (listaAlumnos.get(i) != null && listaAlumnos.get(i).getDni().equals(dni)) {
				return listaAlumnos.get(i);
			}
		}
		return null;
	}

	/**
	 * comprobar si ya existe un alumno
	 * 
	 * @param dni
	 * @return
	 */
	public boolean comprobarAlumno(String dni) {
		Alumno alumno = buscarAlumno(dni);
		if (alumno != null) {
			return true;
		}
		return false;
	}

	/**
	 * modificar dni de alumno
	 * 
	 * @param dniViejo
	 * @param dniNuevo
	 */
	public void cambiarAlumnoDni(String dniViejo, String dniNuevo) {
		Alumno alumno = buscarAlumno(dniViejo);
		alumno.setDni(dniNuevo);
	}

	/**
	 * modificar nombre de alumno
	 * 
	 * @param dniViejo
	 * @param nombreNuevo
	 */
	public void cambiarAlumnoNombre(String dniViejo, String nombreNuevo) {
		Alumno alumno = buscarAlumno(dniViejo);
		alumno.setNombre(nombreNuevo);
	}

	/**
	 * modificar fecha de nacimiento de alumno
	 * 
	 * @param dniViejo
	 * @param fechaNueva
	 */
	public void cambiarAlumnoFechaNacimiento(String dniViejo, String fechaNueva) {
		Alumno alumno = buscarAlumno(dniViejo);
		alumno.setFechaNacimiento(LocalDate.parse(fechaNueva));
	}

	/**
	 * alta curso
	 * 
	 * @param codigo
	 * @param nombre
	 * @param duracion
	 */
	public void altaCurso(String codigo, String nombre, int duracion) {
		Curso nuevoCurso = new Curso(codigo, nombre, duracion);
		listaCursos.add(nuevoCurso);
	}

	/**
	 * alta cursoSinOrdenador
	 * 
	 * @param codigo
	 * @param nombre
	 * @param duracion
	 */
	public void altaCursoSinOrdenador(String codigo, String nombre, int duracion) {
		CursoSinOrdenador nuevoCurso = new CursoSinOrdenador(codigo, nombre, duracion);
		listaCursos.add(nuevoCurso);
	}

	/**
	 * alta cursoConOrdenador
	 * 
	 * @param codigo
	 * @param nombre
	 * @param duracion
	 */
	public void altaCursoConOrdenador(String codigo, String nombre, int duracion) {
		CursoConOrdenador nuevoCurso = new CursoConOrdenador(codigo, nombre, duracion, Ordenadores.LINUX);
		listaCursos.add(nuevoCurso);
	}

	/**
	 * alta cursoConOrdenadorWindows
	 * 
	 * @param codigo
	 * @param nombre
	 * @param duracion
	 */
	public void altaCursoConOrdenadorWindows(String codigo, String nombre, int duracion) {
		CursoConOrdenador nuevoCurso = new CursoConOrdenador(codigo, nombre, duracion, Ordenadores.WINDOWS);
		listaCursos.add(nuevoCurso);
	}

	/**
	 * alta cursoConOrdenadorLinux
	 * 
	 * @param codigo
	 * @param nombre
	 * @param duracion
	 */
	public void altaCursoConOrdenadorLinux(String codigo, String nombre, int duracion) {
		CursoConOrdenador nuevoCurso = new CursoConOrdenador(codigo, nombre, duracion, Ordenadores.LINUX);
		listaCursos.add(nuevoCurso);
	}

	/**
	 * alta cursoConOrdenadorApple
	 * 
	 * @param codigo
	 * @param nombre
	 * @param duracion
	 */
	public void altaCursoConOrdenadorApple(String codigo, String nombre, int duracion) {
		CursoConOrdenador nuevoCurso = new CursoConOrdenador(codigo, nombre, duracion, Ordenadores.APPLE);
		listaCursos.add(nuevoCurso);
	}

	/**
	 * buscar Curso
	 * 
	 * @param codigo
	 * @return
	 */
	public Curso buscarCurso(String codigo) {
		for (Curso cursico : listaCursos) {
			if (cursico != null && cursico.getCodigo().equals(codigo)) {
				return cursico;
			}
		}
		return null;
	}

	/**
	 * comprobar si ya existe un curso
	 * 
	 * @param codigo
	 * @return
	 */
	public boolean comprobarCurso(String codigo) {
		Curso cursico = buscarCurso(codigo);
		if (cursico != null) {
			return true;
		}
		return false;
	}

	/**
	 * buscar CursoConOrdenador
	 * 
	 * @param codigo
	 * @return
	 */
	public CursoConOrdenador buscarCursoConOrdenador(String codigo) {
		for (Curso cursico : listaCursos) {
			if (cursico != null && cursico instanceof CursoConOrdenador && cursico.getCodigo().equals(codigo)) {
				return (CursoConOrdenador) cursico;
			}
		}
		return null;
	}

	/**
	 * buscar CursoSinOrdenador
	 * 
	 * @param codigo
	 * @return
	 */
	public CursoSinOrdenador buscarCursoSinOrdenador(String codigo) {
		for (Curso cursico : listaCursos) {
			if (cursico != null && cursico instanceof CursoSinOrdenador && cursico.getCodigo().equals(codigo)) {
				return (CursoSinOrdenador) cursico;
			}
		}
		return null;
	}

	/**
	 * modificar c�digo de curso
	 * 
	 * @param codigoViejo
	 * @param codigoNuevo
	 */
	public void cambiarCodigoCurso(String codigoViejo, String codigoNuevo) {
		Curso cursico = buscarCurso(codigoViejo);
		cursico.setCodigo(codigoNuevo);
	}

	/**
	 * modificar nombre curso
	 * 
	 * @param codigo
	 * @param nombre
	 */
	public void cambiarNombreCurso(String codigo, String nombre) {
		Curso cursico = buscarCurso(codigo);
		cursico.setNombre(nombre);
	}

	/**
	 * modificar duraci�n curso
	 * 
	 * @param codigo
	 * @param duracion
	 */
	public void cambiarDuracionCurso(String codigo, int duracion) {
		Curso cursico = buscarCurso(codigo);
		cursico.setDuracion(duracion);
	}

	/**
	 * eliminar Alumno y se desinscribe de los cursos en los que est� inscrito
	 * 
	 * @param dni
	 */
	public void eliminarAlumno(String dni) {
		for (Curso cursico : listaCursos) {
			Iterator<Alumno> iteradorAlum = cursico.getAlumnos().iterator();
			while (iteradorAlum.hasNext()) {
				Alumno alum = iteradorAlum.next();
				if (alum.getDni().equals(dni)) {
					iteradorAlum.remove();
				}
			}
		}

		Iterator<Alumno> iteradorAlumno = listaAlumnos.iterator();
		while (iteradorAlumno.hasNext()) {
			Alumno alumno = iteradorAlumno.next();
			if (alumno.getDni().equals(dni)) {
				iteradorAlumno.remove();
			}
		}
	}

	/**
	 * eliminar Curso
	 * 
	 * @param codigo
	 */
	public void eliminarCurso(String codigo) {
		Iterator<Curso> iteradorCurso = listaCursos.iterator();
		while (iteradorCurso.hasNext()) {
			Curso cursico = iteradorCurso.next();
			if (cursico.getCodigo().equals(codigo)) {
				iteradorCurso.remove();
			}
		}
	}

	/**
	 * eliminar CursoConOrdenador
	 * 
	 * @param codigo
	 */
	public void eliminarCursoCon(String codigo) {
		Iterator<Curso> iteradorCursoCon = listaCursos.iterator();
		while (iteradorCursoCon.hasNext()) {
			CursoConOrdenador cursico = (CursoConOrdenador) iteradorCursoCon.next();
			if (cursico.getCodigo().equals(codigo)) {
				iteradorCursoCon.remove();
			}
		}
	}

	/**
	 * eliminar CursoSinOrdenador
	 * 
	 * @param codigo
	 */
	public void eliminarCursoSin(String codigo) {
		Iterator<Curso> iteradorCursoSin = listaCursos.iterator();
		while (iteradorCursoSin.hasNext()) {
			CursoSinOrdenador cursico = (CursoSinOrdenador) iteradorCursoSin.next();
			if (cursico.getCodigo().equals(codigo)) {
				iteradorCursoSin.remove();
			}
		}
	}

	/**
	 * comprobar si un alumno est� inscrito en un curso
	 * 
	 * @param dni
	 * @param codigo
	 * @return
	 */
	public boolean comprobarAlumnoCurso(String dni, String codigo) {
		// Alumno alumno = buscarAlumno(dni);
		Curso curso = buscarCurso(codigo);
		for (Alumno alum : curso.getAlumnos()) {
			if (alum.getDni().equals(dni)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * inscribir alumno en curso
	 * 
	 * @param dni
	 * @param codigo
	 */
	public void inscribirAlumnoCurso(String dni, String codigo) {
		Alumno alumno = buscarAlumno(dni);
		Curso curso = buscarCurso(codigo);
		curso.altaNuevoAlumno(alumno);
	}

	/**
	 * inscribir alumno en cursoConOrdenador
	 * 
	 * @param dni
	 * @param codigo
	 */
	public void inscribirAlumnoCursoCon(String dni, String codigo) {
		Alumno alumno = buscarAlumno(dni);
		CursoConOrdenador curso = (CursoConOrdenador) buscarCurso(codigo);
		listaAlumnos.add(alumno);
		curso.altaNuevoAlumno(alumno);
	}

	/**
	 * inscribir alumno en cursoSinOrdenador
	 * 
	 * @param dni
	 * @param codigo
	 */
	public void inscribirAlumnoCursoSin(String dni, String codigo) {
		Alumno alumno = buscarAlumno(dni);
		CursoSinOrdenador curso = (CursoSinOrdenador) buscarCurso(codigo);
		listaAlumnos.add(alumno);
		curso.altaNuevoAlumno(alumno);
	}

	/**
	 * desinscribir alumno de un curso
	 * 
	 * @param dni
	 * @param codigo
	 */
	public void desinscribirAlumnoCurso(String dni, String codigo) {
		Curso curso = buscarCurso(codigo);
		for (Curso cursico : listaCursos) {
			Iterator<Alumno> iteradorAlumno = cursico.getAlumnos().iterator();
			while (iteradorAlumno.hasNext()) {
				Alumno alumno = iteradorAlumno.next();
				if (alumno.getDni().equals(dni)) {
					iteradorAlumno.remove();
				}
			}
		}
	}

	/**
	 * convertir curso en cursoConOrdenador
	 * 
	 * @param codigo
	 */
	public void convertirCursoEnCursoCon(String codigo) {
		CursoConOrdenador curso = new CursoConOrdenador(codigo);
		listaCursos.add(curso);
	}

	/**
	 * convertir curso en cursoSinOrdenador
	 * 
	 * @param codigo
	 */
	public void convertirCursoEnCursoSin(String codigo) {
		CursoSinOrdenador curso = new CursoSinOrdenador(codigo);
		listaCursos.add(curso);
	}

	/**
	 * convertir curso en curso con ordenador windows
	 * 
	 * @param codigo
	 */
	public void asignarOrdenadorWindows(String codigo) {
		CursoConOrdenador curso = new CursoConOrdenador(codigo, Ordenadores.WINDOWS);
		listaCursos.add(curso);
	}

	/**
	 * convertir curso en curso con ordenador Linux
	 * 
	 * @param codigo
	 */
	public void asignarOrdenadorLinux(String codigo) {
		CursoConOrdenador curso = new CursoConOrdenador(codigo, Ordenadores.LINUX);
		listaCursos.add(curso);
	}

	/**
	 * convertir curso en curso con ordenador Apple
	 * 
	 * @param codigo
	 */
	public void asignarOrdenadorApple(String codigo) {
		CursoConOrdenador curso = new CursoConOrdenador(codigo, Ordenadores.APPLE);
		listaCursos.add(curso);
	}

	/**
	 * listar alumnos
	 */
	public void listarAlumnos() {
		for (Alumno alum : listaAlumnos) {
			if (alum != null) {
				System.out.println(alum);
			}
		}
	}

	/**
	 * Listar un s�lo alumno
	 * 
	 * @param dni
	 */
	public void listarUnAlumno(String dni) {
		if (comprobarAlumno(dni)) {
			System.out.println(buscarAlumno(dni));
		} else {
			System.out.println("El alumno " + dni + " no existe");
		}
	}

	/**
	 * listar alumnos por Curso
	 * 
	 * @param codigo
	 */
	public void listarAlumnosPorCurso(String codigo) {
		for (Curso cursico : listaCursos) {
			if (cursico != null && cursico.getCodigo().equals(codigo)) {
				System.out.println("Listado de alumnos del curso " + cursico.getCodigo() + " " + cursico.getNombre());
				for (Alumno alumno : cursico.getAlumnos()) {
					if (alumno != null) {
						System.out.println(alumno);
					}
				}
			}
		}
	}

	/**
	 * listar alumnos por CursoConOrdenador
	 * 
	 * @param codigo
	 */
	public void listarAlumnosPorCursoCon(String codigo) {
		for (Curso cursico : listaCursos) {
			if (cursico != null && cursico instanceof CursoConOrdenador && cursico.getCodigo().equals(codigo)) {
				System.out.println("Listado de alumnos del curso con ordenador " + cursico.getCodigo() + " "
						+ cursico.getNombre());
				for (Alumno alumno : cursico.getAlumnos()) {
					if (alumno != null) {
						System.out.println(alumno);
					}
				}
			}
		}
	}

	/**
	 * listar alumnos por CursoSinOrdenador
	 * 
	 * @param codigo
	 */
	public void listarAlumnosPorCursoSin(String codigo) {
		for (Curso cursico : listaCursos) {
			if (cursico != null && cursico instanceof CursoSinOrdenador && cursico.getCodigo().equals(codigo)) {
				System.out.println("Listado de alumnos del curso sin ordenador " + cursico.getCodigo() + " "
						+ cursico.getNombre());
				for (Alumno alumno : cursico.getAlumnos()) {
					if (alumno != null) {
						System.out.println(alumno);
					}
				}
			}
		}
	}

	/**
	 * listar cursos de un alumno
	 * 
	 * @param dni
	 */
	public void listaCursosAlumno(String dni) {
		System.out.println("Listado de los cursos en los que est� inscrito el alumno " + dni);
		for (Curso cursico : listaCursos) {
			for (Alumno alumno : cursico.getAlumnos()) {
				if (alumno != null && alumno.getDni().equals(dni)) {
					System.out.println(cursico.getCodigo() + " " + cursico.getNombre());
				}
			}
		}
		System.out.println();
	}

	/**
	 * listar todos los cursos
	 */
	public void listarCursos() {
		for (Curso cursico : listaCursos) {
			if (cursico != null) {
				System.out.println(cursico);
				System.out.println("Total alumnos " + cursico.getAlumnos().size());
				System.out.println();
			}

		}
	}

	/**
	 * Listar un s�lo curso
	 * 
	 * @param codigo
	 */
	public void listarUnCurso(String codigo) {
		if (comprobarCurso(codigo)) {
			System.out.println(buscarCurso(codigo));
		} else {
			System.out.println("El curso " + codigo + " no existe");
		}
	}

	/**
	 * listar CursoSinOrdenador
	 */
	public void listarCursosSin() {
		for (Curso cursico : listaCursos) {
			if (cursico != null && cursico instanceof CursoSinOrdenador) {
				System.out.println("Listado del curso " + cursico.getNombre());
				System.out.println(cursico);
				System.out.println("Total alumnos " + cursico.getAlumnos().size());
				System.out.println();
			}

		}

	}

	/**
	 * listar CursoConOrdenador
	 */
	public void listarCursosCon() {
		for (Curso cursico : listaCursos) {
			if (cursico != null && cursico instanceof CursoConOrdenador) {
				System.out.println("Listado del curso " + cursico.getNombre());
				System.out.println(cursico);
				System.out.println("Total alumnos " + cursico.getAlumnos().size());
				System.out.println();
			}

		}
	}

	/**
	 * listar Curso por tipo de aula
	 * 
	 * @param tipo
	 */
	public void listarCursoPorTipoAula(String tipo) {
		System.out.println("Listado de cursos " + tipo);
		for (Curso cursico : listaCursos) {
			if (cursico != null && cursico instanceof CursoConOrdenador) {
				CursoConOrdenador cursoCon = (CursoConOrdenador) cursico;
				if (cursoCon.getOrdenador().name().equals(tipo.toUpperCase())) {
					System.out.println(cursoCon.getCodigo() + " " + cursoCon.getNombre());
				}
			}
		}
	}
}
