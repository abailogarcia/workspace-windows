package clases;

public class CursoConOrdenador extends Curso {
	/**
	 * Atributo propio de CursoConOrdenador
	 */
	Ordenadores ordenador;

	/**
	 * @param ordenador
	 */
	public CursoConOrdenador(Ordenadores ordenador) {
		super();
		this.ordenador = ordenador;
	}
	/**
	 * @param codigo
	 * @param nombre
	 * @param duracion
	 * @param ordenador
	 */
	public CursoConOrdenador(String codigo, String nombre, int duracion, Ordenadores ordenador) {
		super(codigo, nombre, duracion);
		this.ordenador = ordenador;
	}
	/**
	 * @param codigo
	 * @param ordenador
	 */
	public CursoConOrdenador(String codigo, Ordenadores ordenador) {
		super(codigo);
		this.ordenador = ordenador;
	}
	/**
	 * @param codigo
	 */
	public CursoConOrdenador(String codigo) {
		super(codigo);
	}
	/**
	 * @return the ordenador
	 */
	public Ordenadores getOrdenador() {
		return ordenador;
	}
	/**
	 * @param ordenador the ordenador to set
	 */
	public void setOrdenador(Ordenadores ordenador) {
		this.ordenador = ordenador;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CursoConOrdenador [ordenador=" + ordenador + ", toString()=" + super.toString() + "]";
	}
}
