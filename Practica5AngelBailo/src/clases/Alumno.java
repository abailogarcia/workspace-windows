package clases;

import java.time.LocalDate;
/**
 * 
 * @author angel
 *
 */
public class Alumno {
	//atributos
	private String dni;
	private String nombre;
	private LocalDate fechaNacimiento;
	//constructores
	/**
	 * @param dni
	 * @param nombre
	 * @param fechaNacimiento
	 */
	public Alumno(String dni, String nombre, LocalDate fechaNacimiento) {
		this.dni = dni;
		this.nombre = nombre;
		this.fechaNacimiento = fechaNacimiento;
	}
	/**
	 * @param dni
	 * @param nombre
	 */
	public Alumno(String dni, String nombre) {
		this.dni = dni;
		this.nombre = nombre;
	}
	/**
	 * Constructor sin parámetros
	 */
	public Alumno() {
		this.dni = "";
		this.nombre = "";
	}
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}
	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the fechaNacimiento
	 */
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	/**
	 * @param fechaNacimiento the fechaNacimiento to set
	 */
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Alumno [dni=" + dni + " nombre=" + nombre + " fechaNacimiento=" + fechaNacimiento + "]";
	}
	
	
	
}

