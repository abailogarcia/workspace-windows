package clases;

import java.util.ArrayList;
/**
 * 
 * @author angel
 *
 */

public class Curso {
	/**
	 * Atributos
	 */
	private String codigo;
	private String nombre;
	private int duracion;
	private ArrayList<Alumno> alumnos;
	/**
	 * @param codigo
	 * @param nombre
	 * @param duracion
	 * @param alumnos
	 */
	public Curso(String codigo, String nombre, int duracion) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.duracion = duracion;
		this.alumnos = new ArrayList<Alumno>();
	}
	/**
	 * @param codigo
	 */
	public Curso(String codigo) {
		this.codigo = codigo;
		this.nombre = "";
		this.duracion = 0;
	}
	/**
	 * Constructor sin par�metros
	 */
	public Curso() {
		this.codigo = "";
		this.nombre = "";
		this.duracion = 0;
	}
	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the duracion
	 */
	public int getDuracion() {
		return duracion;
	}
	/**
	 * @param duracion the duracion to set
	 */
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	/**
	 * @return the alumnos
	 */
	public ArrayList<Alumno> getAlumnos() {
		return alumnos;
	}
	/**
	 * @param alumnos the alumnos to set
	 */
	public void setAlumnos(ArrayList<Alumno> alumnos) {
		this.alumnos = alumnos;
	}
	/**
	 * M�todo para dar de alta un alumno
	 * @param alumno
	 */
	public void altaNuevoAlumno(Alumno alumno) {
		this.alumnos.add(alumno);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Curso [codigo=" + codigo + "\n nombre=" + nombre + "\n duracion=" + duracion + "\n alumnos=" + alumnos
				+ "]";
	}
	
}
