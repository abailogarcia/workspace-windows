package clases;
import java.util.Scanner;
public class ScannerWrapper {
	private Scanner input;
	public ScannerWrapper() {
		this.input=new Scanner(System.in);
	}
	public String metodoQueDaCadena() {
		return input.next();
	}
	public void cerrar() {
		input.close();
	}
}
