package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * 
 * @author angel
 *
 */
public class CursoSinOrdenador extends Curso {
	/**
	 * Constructor sin parámetros
	 */
	public CursoSinOrdenador() {
		super();
	}

	/**
	 * @param codigo
	 * @param nombre
	 * @param duracion
	 */
	public CursoSinOrdenador(String codigo, String nombre, int duracion, ArrayList<Alumno> alumSin) {
		super(codigo, nombre, duracion);
		this.setAlumnos(alumSin);
	}

	/**
	 * @param codigo
	 * @param nombre
	 * @param duracion
	 */
	public CursoSinOrdenador(String codigo, String nombre, int duracion) {
		super(codigo, nombre, duracion);
	}

	/**
	 * @param codigo
	 */
	public CursoSinOrdenador(String codigo) {
		super(codigo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CursoSinOrdenador [" + super.toString() + "]";
	}
	
}
