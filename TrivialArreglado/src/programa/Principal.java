package programa;

import clases.Menu;
/*
 * Error 1 import java.util.*  s�lo hay que importar lo necesario
 * Error 2 Scanner no cerrado e importarmos java.util.Scanner
 * Error 3 Nombre de variables no adecuados (mariano, rajoy...)
 * Error 4 Se inicializan las variables a y e a 0 pero luego s�lo se usan para sumar 0. Las borro del c�lculo de aciertos y errores
 * Error 5 sobra el + pregunta1[4] 2[4] y 3[4]= "5. �Cual no es un tipo de placa base?\na)ATX\nb)ITX\n" + "c)OTX";
 * Error 6 las variables informatica, fechas y geografia, sobran porque s�lo se usan para medir su longitud en los for
 * Lo cambio por pregunta1.length()
 * Error 7 la variable trivial no se usa m�s que para cambiar el nombre a la variable preguntai 
 * Error 8 Mejor el Scanner dentro del main
 * 
 * */
public class Principal {

	public static void main(String[] args) {
		Menu.lanzarMenu();

	}

}
