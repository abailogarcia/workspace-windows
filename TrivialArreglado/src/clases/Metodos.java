package clases;

import java.util.Scanner;

public class Metodos {
	static Scanner input = new Scanner (System.in);
	public static String comprobarRespuesta(String respuesta) {
		if(!(respuesta.equalsIgnoreCase("a")) && !(respuesta.equalsIgnoreCase("b")) && !(respuesta.equalsIgnoreCase("c"))) {
			do {
			System.out.println("Respuesta no contemplada, vuelva a introducir");
			respuesta=input.nextLine();
			}while(!(respuesta.equalsIgnoreCase("a")) && !(respuesta.equalsIgnoreCase("b")) && !(respuesta.equalsIgnoreCase("c"))) ;
			}
		return respuesta;
	}
	
	public static void estadisticas(int puntuacionInformatica, int aciertosInformatica, int fallosInformatica,
	int puntuacionHistoria,int aciertosHistoria, int fallosHistoria, int puntuacionGeografia, 	int aciertosGeografia,
	int fallosGeografia) {
		System.out.println("----------------------");
		System.out.println("Aciertos informatica: " + aciertosInformatica);
		System.out.println("Errores informatica: " + fallosInformatica);
		System.out.println("----------------------");
		System.out.println("Aciertos fechas: " + aciertosHistoria);
		System.out.println("Errores fechas: " + fallosHistoria);
		System.out.println("----------------------");
		System.out.println("Aciertos geografia: " + aciertosGeografia);
		System.out.println("Errores geografia: " + fallosGeografia);
		System.out.println("----------------------");
		int aciertos=aciertosInformatica+aciertosHistoria+aciertosGeografia;
		int errores=fallosInformatica+fallosHistoria+fallosGeografia;
		System.out.println("Aciertos totales: " + aciertos);
		System.out.println("Errores totales: " + errores);
		System.out.println("----------------------");
		System.out.println("Porcentaje aciertos totales: " + (double) aciertos / (aciertos+errores) * 100 + " %");
		System.out.println("Porcentaje aciertos totales: " + (double) errores / (aciertos+errores) * 100 + " %");
		System.out.println("----------------------");
	}
}
