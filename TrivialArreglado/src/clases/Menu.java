package clases;

import java.util.Scanner;

public class Menu {
	static Scanner input = new Scanner (System.in);
	
	public static void lanzarMenu() {
		int puntuacionInformatica = 0;
		int aciertosInformatica = 0;
		int fallosInformatica = 0;
		int puntuacionHistoria = 0;
		int aciertosHistoria = 0;
		int fallosHistoria = 0;
		int puntuacionGeografia = 0;
		int aciertosGeografia = 0;
		int fallosGeografia = 0;
		int puntuacionGlobal = 0;
		
		boolean salir = false;
		int opcion = 0;
		while (!salir) {
			System.out.println("Comenzamos el juego...");
			System.out.println("Selecciona una de las siguientes categor�as");
			System.out.println("1.- Inform�tica");
			System.out.println("2.- Historia");
			System.out.println("3.- Geograf�a");
			System.out.println("4.- Estad�stica");
			System.out.println("5.- Salir");
			opcion = input.nextInt();
			input.nextLine();
			switch (opcion) {
			case 1:
				System.out.println("Has elegido Inform�tica. Tienes que responder a las siguientes preguntas");
				String eleccion;
				for (int i = 0; i < 5; i++) {
					System.out.println(Preguntas.preguntasInformatica[i][0]);
					eleccion = Metodos.comprobarRespuesta(input.next());
				
						if (eleccion.equals(Preguntas.preguntasInformatica[i][1])) {
							System.out.println("Bravo, has acertado. Siguiente pregunta");
							puntuacionInformatica++;
							aciertosInformatica++;
						} else {
							System.out.println("Has fallado, siguiente pregunta");
							fallosInformatica++;
						}
					}
				System.out.println("Tu puntuaci�n en este apartado ha sido " + puntuacionInformatica);
				puntuacionGlobal += puntuacionInformatica;
				System.out.println("Tu puntuaci�n global es " + puntuacionGlobal);
				break;
			case 2:
				System.out.println("Has elegido Historia. Tienes que responder a las siguientes preguntas");
				for (int i = 0; i < 5; i++) {
					System.out.println(Preguntas.preguntasHistoria[i][0]);
					eleccion = Metodos.comprobarRespuesta(input.next());
						if (eleccion.equals(Preguntas.preguntasHistoria[i][1])) {
							System.out.println("Bravo, has acertado. Siguiente pregunta");
							puntuacionHistoria++;
							aciertosHistoria++;
						} else {
							System.out.println("Has fallado, siguiente pregunta");
							fallosHistoria++;
						}
					} 
				System.out.println("Tu puntuaci�n en este apartado ha sido " + puntuacionHistoria);
				puntuacionGlobal += puntuacionHistoria;
				System.out.println("Tu puntuaci�n global es " + puntuacionGlobal);
				break;
			case 3:
				System.out.println("Has elegido Geograf�a. Tienes que responder a las siguientes preguntas");
				for (int i = 0; i < 5; i++) {
					System.out.println(Preguntas.preguntasGeografia[i][0]);
					eleccion = Metodos.comprobarRespuesta(input.next());
						if (eleccion.equals(Preguntas.preguntasGeografia[i][1])) {
							System.out.println("Bravo, has acertado. Siguiente pregunta");
							puntuacionGeografia++;
							aciertosGeografia++;
						} else {
							System.out.println("Has fallado, siguiente pregunta");
							fallosGeografia++;
						}
					}
				System.out.println("Tu puntuaci�n en este apartado ha sido " + puntuacionGeografia);
				puntuacionGlobal += puntuacionGeografia;
				System.out.println("Tu puntuaci�n global es " + puntuacionGlobal);
				break;
			case 4:
				Metodos.estadisticas(puntuacionInformatica, aciertosInformatica, fallosInformatica, puntuacionHistoria,
						aciertosHistoria, fallosHistoria, puntuacionGeografia, aciertosGeografia, fallosGeografia);
				break;
			case 5:
				System.out.println("Se acab�");
				salir = true;
				break;
			default:
				System.out.println("Introduce un n�mero entre 1 y 5");
				break;
			}
		}
		input.close();
	}
}
