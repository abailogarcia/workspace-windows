package clases;

public class Preguntas {
	static String pregunta0101 = "1. �Cual es el lenguaje de programacion mas usado?\na)Java\nb)MySQL\nc)HTML5";
	static String pregunta0102 = "a"; 
	static String pregunta0201 = "2. �Como se hace referencia al padre de una herencia?\na)upper();\nb)super();\nc)supper();";
	static String pregunta0202 = "b"; 
	static String pregunta0301 = "3. �Que utilidad tiene input.nextLine();?\na)Saltar de linea\nb)Limpiar el buffer\nc)Escribir las frases juntas";
	static String pregunta0302 = "b"; 	
	static String pregunta0401 = "4. �Que version de HTML se usa actualmente?\na)5\nb)4\nc)6";
	static String pregunta0402 = "a"; 
	static String pregunta0501 = "5. �Cual no es un tipo de placa base?\na)ATX\nb)ITX\nc)OTX";
	static String pregunta0502 = "c"; 
	
	static String pregunta0601 = "1. �En que anno llego el hombre a la luna?\na)1971\nb)1967\nc)1969";
	static String pregunta0602 = "c"; 
	static String pregunta0701 = "2. �Que periodo corresponde a la Guerra Civil Espannola?\na)1939-1945\nb)1936-1939\nc)1933-1936";
	static String pregunta0702 = "b"; 
	static String pregunta0801 = "3. �Cuando termino la II Guerra Mundial?\na)1955\nb)1944\nc)1945";
	static String pregunta0802 = "c"; 
	static String pregunta0901 = "4. �En que anno fue el atentado del 11S?\na)2000\nb)2001\nc)1999";
	static String pregunta0902 = "b"; 
	static String pregunta1001 = "5. En el anno 1989:\na)Aparecio World Wide Web (www)\nb)Se disolvio la URSS\nc)Tuvo lugar la caida del muro de Berlin";
	static String pregunta1002 = "c"; 
	
	static String pregunta1101 = "1. �Cuantos continentes hay en el mundo?\na)5\nb)6\nc)7";
	static String pregunta1102 = "c"; 
	static String pregunta1201 = "2. �Cual es el rio europeo mas largo?\na)Rio Danubio\nb)Rio Volga\nc)Rio Tamesis";
	static String pregunta1202 = "b"; 
	static String pregunta1301 = "3. �Qu� pa�s tiene mayor n�mero de habitantes?\na)Rusia\nb)China\nc)India";
	static String pregunta1302 = "b"; 
	static String pregunta1401 = "4. Las ruinas de Petra, �a que pais pertenecen?\na)Jordania\nb)Siria\nc)Irak";
	static String pregunta1402 = "a"; 
	static String pregunta1501 = "5. �Cual es la capital de Estados Unidos?\na)Los ngeles\nb)New York\nc)Washigton DC";
	static String pregunta1502 = "c"; 
	
	static String[][] preguntasInformatica = { { pregunta0101, pregunta0102},
			{ pregunta0201, pregunta0202 },
			{ pregunta0301, pregunta0302 },
			{ pregunta0401, pregunta0402 },
			{ pregunta0501, pregunta0502 }, };
	static String[][] preguntasHistoria = { { pregunta0601, pregunta0602 },
			{ pregunta0701, pregunta0702 },
			{ pregunta0801, pregunta0802 },
			{ pregunta0901, pregunta0902 },
			{ pregunta1001, pregunta1002 }, };
	static String[][] preguntasGeografia = { { pregunta1101, pregunta1102},
			{ pregunta1201, pregunta1202 },
			{ pregunta1301, pregunta1302 },
			{ pregunta1401, pregunta1402 },
			{ pregunta1501, pregunta1502 }, };
}
