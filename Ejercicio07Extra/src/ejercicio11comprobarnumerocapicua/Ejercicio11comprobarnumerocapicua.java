package ejercicio11comprobarnumerocapicua;

import java.util.Arrays;
import java.util.Scanner;

public class Ejercicio11comprobarnumerocapicua {

	public static void main(String[] args) {
		// Crea una aplicaci�n que pida un numero por teclado y despu�s comprobaremos si el
	//	numero introducido es capic�a, es decir, que se lee igual sin importar la direcci�n.
	//	Por ejemplo, si introducimos 30303 es capic�a, si introducimos 30430 no es
	//	capic�a. Piensa como puedes dar la vuelta al n�mero.
	//	Una forma de pasar un n�mero a un array es esta
	//	Character.getNumericValue(cadena.charAt(posicion)).

		Scanner input=new Scanner(System.in);
		System.out.println("Dame un n�mero y lo compruebo");
		String numero = input.nextLine();
		int [] numeros=new int[numero.length()];
		numeros = convertirAMatriz(numero);
		capicua(numeros);
		
		input.close();
	}
	private static int[] convertirAMatriz(String numero) {
		int [] numeros=new int[numero.length()];
		for(int i=0; i<numeros.length;i++) {
		numeros[i]=Character.getNumericValue(numero.charAt(i));
		}
	return numeros;
	}
	private static void capicua(int[] matriz) {
		int contador=0;
		for (int i =0; i<matriz.length;i++) {
			if(matriz[i] != matriz[matriz.length-i-1]) {
				contador++;
			}
		}
		if ( contador==0) {
			System.out.println("El n�mero es capic�a");
		} else {
			System.out.println("El n�mero no es capic�a");
		}
		//Elena lo hace as�
		//for ( int i=0, j=1;j<=matriz.length; i++, j++) {
		//	listaPrueba[i] = matriz[matriz.length - j];
		//}
		//if (Arrays.equals(matriz, listaPrueba)) {
		//	return true;
		//} 
		//return false;
	}

}
