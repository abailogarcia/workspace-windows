package ejercicio10notasyalumnos;

import java.util.Scanner;

public class Ejercicio10notasyalumnos {
static Scanner input = new Scanner (System.in);
	public static void main(String[] args) {
		//Crea un array de n�meros y otro de String de 10 posiciones donde insertaremos
	//	notas entre 0 y 10 (debemos controlar que inserte una nota valida), pudiendo ser
	//	decimal la nota en el array de n�meros, en el de Strings se insertaran los nombres de
	//	los alumnos.
		//Despu�s, crearemos un array de String donde insertaremos el resultado de la nota
		//con palabras.
		//Si la nota esta entre 0 y 4,99, ser� un suspenso
		//Si esta entre 5 y 6,99, ser� un bien.
		//Si esta entre 7 y 8,99 ser� un notable.
		//Si esta entre 9 y 10 ser� un sobresaliente.
		//Muestra por pantalla, el alumno su nota y su resultado en palabras. Crea los m�todos
		//que creas conveniente
		pidiendoNotas();
		
		input.close();
	}
	static double[] pedirNotas() {
		double[] notas = new double[10];
		for (int i =0; i<notas.length;i++) {
			System.out.println("Introduce la nota " + i);
			notas[i]=input.nextDouble();
			if (notas[i]<0 || notas[i]>10) {
				System.out.println("Entre 0 y 10 por favor");
				i--;
			}
		}
		return notas;
	}
	static double pedirNota(double[] notas,int i) {
		System.out.println("Introduce la nota " + i);
		notas[i]=input.nextDouble();
		return notas[i];
	}
	static String pedirNombre(String[] nombres, int i) {
		System.out.println("Introduce el nombre " + i);
		nombres[i]=input.nextLine();
		return nombres[i];
	}
	
	
	static void pidiendoNotas() {
		double[] notas = new double[2];
		String[] nombres = new String[2];
		
		for (int i =0; i<notas.length;i++) {
			nombres[i] = pedirNombre(nombres,i);
			input.nextLine();
			notas[i]=pedirNota(notas, i);
			if (notas[i]<0 || notas[i]>10) {
				System.out.println("Entre 0 y 10 por favor");
				i--;
			}
		}
		for (int i =0; i<notas.length;i++) {
			System.out.println("La nota de " + nombres[i] + " es " + notas[i]);
		}
	}

}
