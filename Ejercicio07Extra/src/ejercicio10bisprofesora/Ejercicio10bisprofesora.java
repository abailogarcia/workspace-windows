package ejercicio10bisprofesora;

public class Ejercicio10bisprofesora {

	public static void main(String[] args) {
		//Crea un array de n�meros y otro de String de 10 posiciones donde insertaremos
		//	notas entre 0 y 10 (debemos controlar que inserte una nota valida), pudiendo ser
		//	decimal la nota en el array de n�meros, en el de Strings se insertaran los nombres de
		//	los alumnos.
			//Despu�s, crearemos un array de String donde insertaremos el resultado de la nota
			//con palabras.
			//Si la nota esta entre 0 y 4,99, ser� un suspenso
			//Si esta entre 5 y 6,99, ser� un bien.
			//Si esta entre 7 y 8,99 ser� un notable.
			//Si esta entre 9 y 10 ser� un sobresaliente.
			//Muestra por pantalla, el alumno su nota y su resultado en palabras. Crea los m�todos
			//que creas conveniente
		//tama�o del array
		final int TAMANO=10;
		//creamos los arrays
		String[] nombres=new String[TAMANO];
		double[] notas = new double[TAMANO];
		//rellenamos el array de una vez
		Metodos10.rellenarArrays(notas, nombres);
		//devuelve las notas con palabras
		String[] resultado= Metodos10.anadeResultado(notas);
		//mostramos el resultado
		Metodos10.mostrarArrays(nombres, notas , resultado);
		

	}

}
