package ejercicio01metodospedirymostrar;

import java.util.Scanner;

public class Ejercicio01metodospedirymostrar {

	public static void main(String[] args) {
		// Crea un array de 10 posiciones de n�meros con valores pedidos por teclado.
		//Muestra por consola el �ndice y el valor al que corresponde. Haz dos m�todos, uno
		//para rellenar valores y otro para mostrar.
		int[] matriz=  pedirValores();
		mostrarValores(matriz);
		
	}
	private static int[] pedirValores() {
		Scanner input = new Scanner (System.in);
		int[] valores= new int[10];
		for (int i = 0; i< valores.length; i++) {
			System.out.println("Introduce el valor " + i);
			valores[i] = input.nextInt();
		}
		input.close();
		return valores;
	}

	private static void mostrarValores(int[] matriz) {
		for (int i =0; i<matriz.length; i++) {
			System.out.print(i + " ");
		}
	}
}
