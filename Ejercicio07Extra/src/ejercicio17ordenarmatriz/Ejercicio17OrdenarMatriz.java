package ejercicio17ordenarmatriz;

import java.util.Scanner;

public class Ejercicio17OrdenarMatriz {

	public static void main(String[] args) {
		// Ordenar un array de n�meros con el m�todo de la burbuja.
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el n�mero de filas");
		int filas = input.nextInt();
		System.out.println("Introduce el n�mero de columnas");
		int colum = input.nextInt();
		int[][] matriz=crearMatriz(filas,colum);
		mostrarMatriz(matriz);
		System.out.println("La matriz ordenada es ");
		mostrarMatriz(ordenarMatriz(matriz,filas,colum));
		input.close();
	}
	public static int[][] crearMatriz(int filas, int colum){
		int[][] matriz = new int[filas][colum];
		for(int i=0; i<matriz.length;i++) {
			for (int j=0; j<matriz[i].length;j++) {
				matriz[i][j]= (int)(Math.random()*100);
			}
		}
		return matriz;
	}
	public static void mostrarMatriz(int[][] matriz) {
		for(int i=0; i<matriz.length;i++) {
			for (int j=0; j<matriz[i].length;j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
	}
	public static int[][] ordenarMatriz(int[][] matriz, int filas, int colum){
		//int[][] matrizOrdenada=new int[filas][colum];
		for(int i=0; i<matriz.length;i++) {
			for (int j=0; j<matriz[i].length;j++) {
				for (int k =0; k<matriz.length;k++) {
					for ( int l=0; l<matriz[k].length;l++) {
						if(matriz[i][j]<matriz[k][l]) {
						//	int aux =0;
							int aux=matriz[i][j];
							matriz[i][j]=matriz[k][l];
							matriz[k][l]=aux;
						}
					}
				}
			}
		}
		return matriz;
	}
}
