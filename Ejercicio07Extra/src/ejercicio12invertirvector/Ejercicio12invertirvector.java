package ejercicio12invertirvector;

public class Ejercicio12invertirvector {

	public static void main(String[] args) {
		// Dado un array de n�meros de 5 posiciones con los siguientes valores {1,2,3,4,5},
		//guardar los valores de este array en otro array distinto, pero con los valores
		//invertidos, es decir, que el segundo array deber� tener los valores {5,4,3,2,1}.
		int [] vector1 = {1,2,3,4,5};
		int [] vectorInverso = new int[vector1.length];
		for (int i =0; i<vector1.length;i++) {
			
			System.out.print(vector1[i] + " ");
		}
		System.out.println();
		for (int i =0; i<vector1.length;i++) {
			vectorInverso[i] = vector1[vector1.length-i-1];
			System.out.print(vectorInverso[i] + " ");
		}
	}

}
