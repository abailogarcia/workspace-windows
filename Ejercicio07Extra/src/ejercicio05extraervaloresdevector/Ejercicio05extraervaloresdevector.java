package ejercicio05extraervaloresdevector;

import java.util.Scanner;

public class Ejercicio05extraervaloresdevector {

	public static void main(String[] args) {
		// Crea un array de caracteres que contenga de la �A� a la �Z� (solo las may�sculas).
		//Despu�s, ve pidiendo posiciones del array por teclado y si la posici�n es correcta, se
		//a�adir� a una cadena que se mostrara al final, se dejar� de insertar cuando se
		//introduzca un -1.
		Scanner input = new Scanner(System.in);
		char[] mayusculas= new char[26];
		for (int i ='A', j=0; i<='Z'; i++,j++) {
			mayusculas[j]=(char)i;
		}
		
		String cadena="";
		int eleccion;
		do {
			System.out.println("Elige un �ndice entre 0 y " + (mayusculas.length-1));
			eleccion= input.nextInt();
			
			if(!(eleccion>=-1 && eleccion<=mayusculas.length-1)) {
				System.out.println("Error, inserta otro n�mero");
			}else {
				if(eleccion != -1) {
					cadena += mayusculas[eleccion];
				}
			}
		}while(eleccion !=-1);
		System.out.println(cadena);
		input.close();
	}
}
	
