package ejercicio04creavectorsumaymedia;

public class Ejercicio04creavectorsumaymedia {

	public static void main(String[] args) {
		// Crea un array de n�meros de 100 posiciones, que contendr� los n�meros del 1 al
		//100. Obt�n la suma de todos ellos y la media
		int[] matriz = new int[100];
		int suma = 0;
		for (int i =0, j=1; i<matriz.length; i++,j++) {
			matriz[i]=j;
			suma+=matriz[i];
		}
		mostrarMatriz(matriz);
		System.out.println();
		System.out.println("La suma de la matriz es " + suma);
		System.out.println("La media de la matriz es " + suma/matriz.length);

	}
	static void mostrarMatriz(int[] matriz) {
		for (int i =0; i<matriz.length; i++) {
			System.out.print(matriz[i] + " ");
		}
	}
}
