package ejercicio08crearvectoraleatorioymostrarnumerosqueacaban;

import java.util.Scanner;

public class Ejercicio08crearvectoraleatorioymostrarnumerosqueacaban {
static Scanner input = new Scanner(System.in);
	public static void main(String[] args) {
		// Crea un array de n�meros de un tama�o pasado por teclado, el array contendr�
		//n�meros aleatorios entre 1 y 300 y mostrar aquellos n�meros que acaben en un
		//d�gito que nosotros le indiquemos por teclado (debes controlar que se introduce un
		//numero correcto), estos deben guardarse en un nuevo array.
		//Por ejemplo, en un array de 10 posiciones e indicamos mostrar los n�meros
		//acabados en 5, podr�a salir 155, 25, etc.
		
		System.out.println("Introduce la longitud del vector");
		int longitud = input.nextInt();
		System.out.println("Introduce el n�mero con el que acaba");
		int fin = input.nextInt();
		int [] nuevaMatriz=matrizAleatoria(longitud);
		mostrarMatriz(nuevaMatriz);
		int largo = longitudVector(nuevaMatriz,fin);
		
		
		input.close();
		
	}
	//este m�todo crea una matriz aleatoria
	private static int[] matrizAleatoria(int longitud) {
		int[] matriz = new int[longitud];
		for (int i =0; i<longitud; i++) {
			matriz[i] = (int)(Math.random()*(300-1)+1);
		}
		return matriz;
	}
	private static void mostrarMatriz(int[] matriz) {
		for (int i =0; i<matriz.length; i++) {
			System.out.print(matriz[i] + " ");
		}
	}
	private static int longitudVector(int[] array, int terminal) {
		int largo=0;
		for ( int i =0; i< array.length;i++) {
			if (array[array.length-1] == terminal) {
			largo ++;
			}
		}	
		//if (largo ==0) {
		//System.out.println();
		//System.out.println(terminal);
	//System.out.println(largo);
		//	System.out.println("No se ha encontrado ninguna coincidencia");
		//}
		return largo;
	}
	
	private static int[] buscarEnMatriz(int[] vector, int longi, int termina) {
		int[] inversa = new int[longi];
		for ( int i =0, j=0; i< vector.length;i++) {
			if (vector[vector.length-1] == termina) {
			inversa[j] = vector[i];
			j++;
			}
			
		}
		return inversa;
	}
	private static String numeroAString(int entero) {
		String texto= Integer.toString(entero);
		return texto;
	}
	private static String[] convierteMatrizNumeroAString(int[] matriz, int largo) {
		String[] matrizTexto = new String[largo];
		for ( int i =0; i< matriz.length;i++) {
			matrizTexto[i] = numeroAString(matriz[i]);
			}
		return matrizTexto;
	}
	//private static String[] recortaMatriz(String[] matriz, int largo) {
		//String[] matrizRecortada = new String[largo];
		//for ( int i =0; i< matriz.length;i++) {
			//matrizTexto[i] = numeroAString(matriz[i]);
			//}
	//}

}
