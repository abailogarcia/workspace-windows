package ejercicio06creararraydesdestring;

import java.util.Scanner;

public class Ejercicio06creararraydesdestring {

	public static void main(String[] args) {
		// Pide al usuario por teclado una frase y pasa sus caracteres a un array de caracteres.
		//Puedes hacer con o sin m�todos de String.
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce una frase");
		String frase = input.nextLine();
		char[] vectorFrase= new char[frase.length()];

		for (int i =0 ; i<frase.length(); i++) {
			
			vectorFrase[i]= frase.charAt(i);
			System.out.print(vectorFrase[i] + " ");
		}
		input.close();
		
	}

}
