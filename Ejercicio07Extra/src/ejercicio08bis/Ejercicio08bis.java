package ejercicio08bis;

import java.util.Scanner;

public class Ejercicio08bis {

	public static void main(String[] args) {
		// Crea un array de n�meros de un tama�o pasado por teclado, el array contendr�
				//n�meros aleatorios entre 1 y 300 y mostrar aquellos n�meros que acaben en un
				//d�gito que nosotros le indiquemos por teclado (debes controlar que se introduce un
				//numero correcto), estos deben guardarse en un nuevo array.
				//Por ejemplo, en un array de 10 posiciones e indicamos mostrar los n�meros
				//acabados en 5, podr�a salir 155, 25, etc.
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce el tama�o del vector");
		int tan= input.nextInt();
		int[] num = new int[tan];
		
		int ultimoDigito;
		do {
			System.out.println("Introduce un n�mero entre 0 y 9");
			ultimoDigito = input.nextInt();
		} while (!(ultimoDigito>=0 && ultimoDigito <=9));
		
		rellenarNumAleatorioArray(num,1,300);
		for (int i =0; i<num.length;i++) {
			System.out.print(num[i] + " ");
		}
		System.out.println();
		//creamos un array qeu contenga los n�mero terminados en el num especificado
		int[] terminadosEn = numTerminadosEn(num,ultimoDigito);
		//mostramos el resultado
		mostrarArrayTerminadosEn(terminadosEn);
		input.close();

	}
	public static void rellenarNumAleatorioArray(int[] vector, int a , int b) {
		for (int i =0; i<vector.length;i ++) {
			vector[i]=((int)Math.floor(Math.random()*(a-b)+b));
		}
	}
	public static void mostrarArrayTerminadosEn(int[] vector) {
		for (int i =0; i<vector.length;i++) {
			//no incluimos las posiciones que tienen un 0
			if (vector[i]!=0) {
				System.out.println("El n�mero " + vector[i] + " acaba en el n�mero deseado") ;
				
			}
		}
	}
	public static int[] numTerminadosEn(int[] vectorNum, int ultimoNumero) {
		//array que almacenar� los n�meros terminados en el n�mero pedido
		int[] terminadosEn=new int[vectorNum.length];
		int numeroFinal=0;
		for (int i =0; i<terminadosEn.length;i++) {
			//restamos el n�mero por el mismo n�mero sin unidades
			// por ejemplo 325-320 =5
			numeroFinal=vectorNum[i] %10;
			//si el n�mero buscado es el buscado lo a�adimos
			if ( numeroFinal == ultimoNumero) {
				terminadosEn[i]=vectorNum[i];
			}
		}
		return terminadosEn;
	}
}
