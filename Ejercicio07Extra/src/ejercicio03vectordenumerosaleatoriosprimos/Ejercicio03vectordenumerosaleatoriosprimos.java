package ejercicio03vectordenumerosaleatoriosprimos;

import java.util.Scanner;

public class Ejercicio03vectordenumerosaleatoriosprimos {

	public static void main(String[] args) {
		// Crea un array de n�meros de un tama�o pasado por teclado, el array contendr�
		//n�meros aleatorios primos entre los n�meros deseados, por �ltimo, nos indica cual
		//es el mayor de todos.
		//Haz un m�todo para comprobar que el n�mero aleatorio es primo, puedes hacer
		//todos los m�todos que necesites
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce la longitud del vector");
		int longitud = input.nextInt();
		System.out.println("Introduce el primer n�mero aleatorior");
		int primer = input.nextInt();
		System.out.println("Introduce el �ltimo n�mero aleatorio");
		int ulti = input.nextInt();
		int[] nuevaMatriz=generaMatriz(longitud, primer, ulti);
		mostrarMatriz(nuevaMatriz);
		maximoMatriz(nuevaMatriz);
		input.close();
	}
	public static boolean compruebaPrimo(int numero) {
	//	boolean esPrimo=true;
		for(int i =2; i<numero;i++) {
			if (numero%i==0) {
				return false;
			}	
		}
		return true;
	}
	public static int generaAleatorio(int principio, int ulti) {
		return (int)(Math.random()*(ulti-principio)+principio);
	}
	public static int[] generaMatriz(int tama�o, int principio, int ulti) {
		int[] matriz = new int[tama�o];
		int candidato;
		for (int i =0; i<tama�o; i++) {
			candidato=generaAleatorio(principio, ulti);
			if (compruebaPrimo(candidato)) {
				matriz[i] = candidato;
			} else {
				i--;
			}
		}
		return matriz;
	}
	static void mostrarMatriz(int[] matriz) {
		for (int i =0; i<matriz.length; i++) {
			System.out.print(matriz[i] + " ");
		}
	}
	public static void maximoMatriz(int[] matriz) {
		int max =matriz[0];
		for (int i =0; i<matriz.length;i++) {
			if(matriz[i]>max) {
				max=matriz[i];
			}
		}
		System.out.println();
		System.out.println("El valor m�ximo del vector es " + max);
	}
	
}
