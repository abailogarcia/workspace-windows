package ejercicioprevios;

import java.util.Scanner;

public class Metodos01 {
	static Scanner input = new Scanner (System.in);
	public static String[] rellenarVector() {
	
		System.out.println("Rellenamos el vector");
		String[] frases = new String[2];
		for (int i =0; i<frases.length;i++) {
			System.out.println("Introduce la frase " + i);
			frases[i] = input.nextLine();
		}
 	
 		return frases;
	}
	
	public static void visualizarPalabras(String[] frases) {
		System.out.println("Introduce una componente");
		int componente = input.nextInt();
		System.out.println("La frase de la componente " + componente + " es " + frases[componente]);
		int cantidadEspacios=0;
		for (int i =0; i<frases[componente].length() ; i++) {
			if (frases[componente].charAt(i)==' ') {
				cantidadEspacios++;
			}
		}
		String[] palabras = frases[componente].split(" ");
		for (int i=0; i<cantidadEspacios+1;i++) {
			
			System.out.println(palabras[i]);
		}
	}
}
