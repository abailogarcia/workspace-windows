package ejercicio02vectoraleatorio;

import java.util.Scanner;

public class Ejercicio02vectoraleatorio {

	public static void main(String[] args) {
		// Crea un array de n�meros donde le indicamos por teclado el tama�o del array,
		//rellenaremos el array con n�meros aleatorios entre 0 y 9, al final muestra por
		//pantalla el valor de cada posici�n y la suma de todos los valores. Haz un m�todo
		//para rellenar el array (que tenga como par�metros los n�meros entre los que tenga
		//que generar), para mostrar el contenido y la suma del array y un m�todo privado
		//para generar n�mero aleatorio (lo puedes usar para otros ejercicios)
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce el tama�o del vector");
		int tama�o = input.nextInt();
		System.out.println("Introduce el primer n�mero aleatorior");
		int primer = input.nextInt();
		System.out.println("Introduce el �ltimo n�mero aleatorio");
		int ulti = input.nextInt();
		
		int[] matriz = creaVector(tama�o,primer,ulti);
		mostrarMatriz(matriz);
		sumaMatriz(matriz);
		input.close();

	}
	static int[] creaVector(int tama�o, int empieza, int acaba) {
		int[] vector = new int[tama�o];
		for (int i =0; i< tama�o; i++) {
			vector[i] = (int)(Math.random()*(acaba-empieza)+ empieza);
		}
		return vector;
	}
	static void mostrarMatriz(int[] matriz) {
		for (int i =0; i<matriz.length; i++) {
			System.out.print(matriz[i] + " ");
		}
	}
	static void sumaMatriz(int[] matriz) {
		int suma=0;
		for (int i =0; i<matriz.length; i++) {
			suma+=matriz[i];
		}
		System.out.println();
		System.out.println("El vector suma " + suma);
	}

}
