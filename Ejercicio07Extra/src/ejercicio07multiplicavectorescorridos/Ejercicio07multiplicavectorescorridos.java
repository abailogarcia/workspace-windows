package ejercicio07multiplicavectorescorridos;

import java.util.Scanner;

public class Ejercicio07multiplicavectorescorridos {

	public static void main(String[] args) {
		// Crea dos arrays de n�meros con una posici�n pasado por teclado.
		//Uno de ellos estar� rellenado con n�meros aleatorios y el otro apuntara al array
		//anterior, despu�s crea un nuevo array con el primer array (usa de nuevo new con el
		//primer array) con el mismo tama�o que se ha pasado por teclado, rell�nalo de nuevo
		//con n�meros aleatorios.
		//Despu�s, crea un m�todo que tenga como par�metros, los dos arrays y devuelva uno
		//nuevo con la multiplicaci�n de la posici�n 0 del array1 con el del array2 y as�
		//sucesivamente. Por �ltimo, muestra el contenido de cada array
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la longitud del vector");
		int longitud = input.nextInt();
		
		int[] array1 = new int[longitud];
		int[] array2;
		array2=array1;
		for ( int i =0; i<longitud; i++) {
			array1[i]=(int)(Math.random()*100);
			System.out.print(array1[i] + " ");
		}
		System.out.println();
		for ( int i =0; i<longitud; i++) {
			
			System.out.print(array2[i] + " ");
		}
		
		System.out.println();
		array1=new int[longitud];
		for ( int i =0; i<longitud; i++) {
			array1[i]=(int)(Math.random()*100);
			System.out.print(array1[i] + " ");
		}
		System.out.println();
		multiplicaVectores(array1, array2);
		input.close();
	}
	static void multiplicaVectores(int[] array1, int[] array2) {
		int[] multiplica=new int[array1.length];
		for ( int i =0; i<array1.length;i++) {
			multiplica[i]=array1[i]*array2[i];
			System.out.print(multiplica[i] + " ");
		}
		
	}

}
