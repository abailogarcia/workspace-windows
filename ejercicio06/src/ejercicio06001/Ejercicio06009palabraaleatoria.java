package ejercicio06001;

import java.util.Scanner;

public class Ejercicio06009palabraaleatoria {

	public static void main(String[] args) {
		// Aplicaci�n que hace uso del m�todo palabraAleatoria(int longitud). Este m�todo recibe la
		//longitud de caracteres que queremos que tenga la cadena y nos devuelve un String de esa
		//longitud con letras aleatorias en el rango A-Z may�sculas. Podemos usar el m�todo del
		//ejercicio anterior.
		Scanner input =new Scanner(System.in);
		System.out.println("Introduce una longitud de palabra");
		int longit=input.nextInt();
		System.out.println("El nuevo pass es " + Metodos.palabraAleatoria(longit));
		
		input.close();
		
	}

}
