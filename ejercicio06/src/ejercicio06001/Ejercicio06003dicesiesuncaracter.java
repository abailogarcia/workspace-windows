package ejercicio06001;

import java.util.Scanner;

public class Ejercicio06003dicesiesuncaracter {

	public static void main(String[] args) {
		// Crea una aplicaci�n (Ejercicio3) que haga uso del m�todo �esDigito�. Este m�todo se crea
		//en una clase (Metodos) y recibe un tipo char, y devuelve true o false, si el car�cter es una
		//cifra, o no.
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce un car�cter");
		char texto= input.next().charAt(0);
		
		System.out.println(Metodos.esDigito(texto));
		
		input.close();

	}

}
