package ejercicio06001;

import java.util.Scanner;

public class Ejercicio06002maximoyminimo {

	public static void main(String[] args) {
		// Crea dos m�todos en una clase (Metodos): El m�todo �maximo� recibe 2 n�meros
		//enteros, y me devuelve el n�mero mayor. El m�todo �minimo� recibe 2 n�meros y
		//devuelve el menor. Crea una aplicaci�n (Ejercicio2) que muestre el uso de esos 2 m�todos.
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce un n�mero");
		int num1= input.nextInt();
		System.out.println("Introduce otro n�mero");
		int num2= input.nextInt();
		System.out.println("El m�ximo es "+ Metodos.maximo(num1, num2));
		System.out.println("El m�nimo es " + Metodos.minimo(num1, num2));
		
		
		input.close();
	}
	

}
