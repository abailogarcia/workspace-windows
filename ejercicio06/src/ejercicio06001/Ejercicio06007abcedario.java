package ejercicio06001;

public class Ejercicio06007abcedario {

	public static void main(String[] args) {
		// Aplicaci�n que llama al m�todo abecedario(). Dicho m�todo no devuelve ni recibe nada y
		//muestra por pantalla las letras may�sculas de la A-Z de la siguiente forma: AAA, AAB,
		//AAC,... ABA, ABB, ABC, ABD,... ZZZ. Cada combinaci�n en una l�nea distinta. Desde AAA
		//hasta ZZZ.
		Metodos.abcedario();

	}

}
