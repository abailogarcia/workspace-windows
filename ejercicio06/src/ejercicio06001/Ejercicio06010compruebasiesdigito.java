package ejercicio06001;

import java.util.Scanner;

public class Ejercicio06010compruebasiesdigito {

	public static void main(String[] args) {
		// Crear una aplicaci�n que pide una cadena de texto, y mediante el m�todo esEntero(String
		//cadena), que devuelve un boolean indicando si la cadena corresponde a un n�mero entero
	//o no. El m�todo esEntero, eval�a todos los caracteres de la cadena (0 - cadena.length()) y
	//me dice false si encuentra alg�n car�cter que no sea una cifra. Tambi�n debe atender al
	//signo negativo
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce una cadena para verificar");
		String texto= input.nextLine();
		if (Metodos.esEntero(texto)) {
			System.out.println("Es un n�mero entero");
		} else {
			System.out.println("No es un n�mero entero");
		}
		
		input.close();
		
	}

}
