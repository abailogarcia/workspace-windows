package ejercicio06001;

import java.util.Scanner;

public class Ejercicio06008aleatorioentre2numeros {

	public static void main(String[] args) {
		// Crea una aplicaci�n que nos genere un n�mero aleatorio entero. Para ello haremos uso del
		//m�todo aleatorio(int inicio, int final), que recibe dos valores enteros, entre los cuales se
		//crear� el n�mero entero.
		Scanner input = new Scanner (System.in);
		System.out.println("Dame un entero");
		int entero1 = input.nextInt();
		System.out.println("Dame otro entero");
		int entero2= input.nextInt();
		System.out.println("El aletario entre " + entero1 + " y " + entero2 + " es " + Metodos.aleatorio(entero1,entero2));
				
		input.close();

	}

}
