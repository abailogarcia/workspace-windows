package ejercicio06001;

import java.util.Scanner;

public class Ejercicio06004primos {

	public static void main(String[] args) {
		// Crea una clase (Metodos) que contenga el m�todo esPrimo(). Dicho m�todo recibe un
	//	n�mero entero e indica si es primo o no (boolean). Crea una aplicaci�n (Ejercicio4) que
	//	pida al usuario un n�mero entero positivo, y mostrar todos los n�meros primos desde 1
	//	hasta el n�mero le�do, usando el m�todo creado.
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un entero");
		int numero=input.nextInt();
		for (int i =1; i < numero; i++) {
			if ( Metodos.esPrimo2(i)==true ) {
				System.out.println("El " + i + " es primo");
			}
			
		}
		
		input.close();

	}

}
