package ejercicio06001;

import java.util.Scanner;

public class ejercicio06011cambiarletras {

	public static void main(String[] args) {
		// Aplicaci�n que pide una palabra en min�sculas y se la pasa al m�todo cifrar(String cadena,
	//	int desfase). El m�todo cifrar() devuelve un String, que es el resultado de cambiar cada
//	caracter del String cadena recibido, por el car�cter que est� tantas posiciones en la tabla
//	ascii como indica el valor �desfase�. Ejemplo cadena = �abcd� ; desfase = 2; Entonces
//	cambio la �a� por la �c�, la �b� por la �d�, la �c� por la �e�, y la �d� por la �f�. Cada letra es
//	cambiada por la letra 2 posiciones m�s adelante. Si con el desfase me paso de la letra �z�
//	continuo de nuevo con la �a�.
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el texto que quieres cifrar");
		String texto= input.nextLine().toLowerCase();
	//	input.nextLine();
		System.out.println("Introduce el desfase");
		int desfase=input.nextInt();
		System.out.println("El texto cifrado: " + Metodos.cifrar(texto, desfase));
		
		
		input.close();

	}

}
