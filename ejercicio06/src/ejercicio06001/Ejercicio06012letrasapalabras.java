package ejercicio06001;

import java.util.Scanner;

public class Ejercicio06012letrasapalabras {

	public static void main(String[] args) {
		// Crear un programa que me muestra una cadena de texto String. Dicha cadena de texto
		//ser� devuelta por un m�todo leeCaracteres() que no recibe nada. El m�todo ser� el
		//encargado de leer constantemente caracteres (cifras o letras) hasta que introduzcamos la
		//cifra 0. La cadena que devuelve dicho m�todo debe ser el resultado de unir todos los
		//caracteres introducidos por teclado separados por un espacio.
		
		//public se puede usar en cualquier
		// vaci� o package se puede usar en el mismo paquete, puede estar en otra clase
		//private solo en la misma clase
		
		String cadena = Metodos.leeCaracteres();
		System.out.println(cadena);
	}
}
