package ejercicio06001;

import java.util.Scanner;

public class Ejercicio06005aleatorio {

	public static void main(String[] args) {
		// Crea una aplicaci�n (Ejercicio5) que nos genere un n�mero aleatorio entero. Para ello en
		//una clase nueva (Metodos) crearemos el m�todo aleatorio(int final), que recibe un entero,
		//y nos genera un n�mero aleatorio entre 0 y final. (necesito usar el m�todo Math.random()
		//para crear el m�todo aleatorio())
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un entero");
		int numero=input.nextInt();
		System.out.println("El aleatorio es " + Metodos.aleatorio(numero));
		
		input.close();

	}

}
