package ejercicio06001;

import java.util.Scanner;

public class Metodos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	static void edad(int a�os) {
		if (a�os>=18) {
			System.out.println("Eres mayor de edad");
		} else {
			System.out.println("Eres menor de edad");
		}
	}
	static int maximo(int n1, int n2) {
		int max=(n1>n2)?n1:n2;
		return max;
	}
	static int minimo(int n3, int n4) {
		int min=(n3<n4)?n3:n4;
		return min;
	}
	static boolean esDigito(char text) {
		boolean caracter;
		if ( (text >96 && text<123) || (text >64 && text < 91)) {
			return true;
		} else {
			return false;
		}
	//	return caracter;
	}
	static boolean esPrimo(int n5) {
		int contador=0;
		boolean primo = true;
		for (int i=2;i<n5; i++) {
			if (n5%i==0) {
				contador++;
			}
		}
		if (contador>0) {
			primo=false;
		}
		return primo;
	}
	static boolean esPrimo2(int n9) {
		int contador=0;
		boolean primo = true;
		for (int i=2;i<n9; i++) {
			if (n9%i==0) {
				return false;
			} 
		} return true;
		
	}
	//que recibe un entero,
	//y nos genera un n�mero aleatorio entre 0 y final.
	static int aleatorio(int n6) {
		int numero=(int)Math.round((Math.random()*n6));
		return numero;
	}
	static int aleatorio(int n6, int n7) {
		int max=maximo(n6,n7);
		int min=minimo(n6,n7);
		int numero=(int)Math.round((Math.random()*(max-min) + min));
		return numero;
	}
	static void mostrarTablaAscii() {
		for (int i=0;i<=127;i++) {
			System.out.println(i+" "+ (char)i);
		}
	}

	static void abcedario() {
		for (int i =65;i<=90;i++) {
			char primer=(char)i;
			//System.out.print((char)i);
			for (int j =65;j<=90;j++) {
				char segun=(char)j;
				//System.out.print((char)j);
				for (int k =65;k<=90;k++) {
					System.out.println(primer +""+ segun + "" + (char)k);
				}
			}
		}
	}
	
	static String palabraAleatoria(int longitud) {
		String palabra="";
		for (int i =0; i<longitud; i++) {
			char caracter=(char)aleatorio(33,126);
			palabra+=caracter;
		}
		return palabra;
	}
	
	static boolean esEntero(String cadena) {
		int numerico=0;
		for (int i= 0; i<cadena.length(); i++) {
			char caracter = cadena.charAt(i);
			if (i==0) {
				if (caracter == 45 || (caracter >=48 && caracter <= 57)) {
					
						} else {
							numerico++;
					}
				
			} else {
					if (caracter >=48 && caracter <= 57) {
						
						}else {
							numerico++;
						}
					}
			
		}
		if (numerico ==0) {
			return true;
		} else {
			return false;
		}
	}
	static String cifrar(String cadena, int desfase) {
		String palabra="";
		//si tengo un n�mero m�s grande que 26, permite trabajar menos
		//si es 5 avanza 5
		//si es 30 avanza 4 (a partir de 26)
		//desfase=desfase%26;
		for (int i =0; i<cadena.length(); i++) {
			char caracter = cadena.charAt(i);
			int suma = caracter +desfase;
			if (suma >122) {
				caracter = (char)(96 + (suma-122));
				palabra+=caracter;
			}	 else {
				caracter +=desfase;
				palabra+=caracter;
				}
			}
		return palabra;
	}
	
	static String leeCaracteres() {
		String cadena="";
		char caracter;
		Scanner input = new Scanner(System.in);
		do {
			System.out.println("Introduce un car�cter");
			caracter = input.nextLine().charAt(0);
			if (caracter!='0') {
			cadena+=caracter + " ";
			}
		} while (caracter !='0');
		
		input.close();
		return cadena;
	}


	static String leeCadena() {
		String cadenaResultado="";
		String cadenaLeida="";
	
		Scanner input = new Scanner(System.in);
		do {
			System.out.println("Introduce una cadena");
			cadenaLeida = input.nextLine();
			if (cadenaLeida.equals("fin")) {
			cadenaResultado+=cadenaLeida + " ";
			}
		} while (!cadenaLeida.equals("fin"));
		
		input.close();
		return cadenaResultado;
	}
	
	static int deCadenaAEntero(String cadena) {
		int resultado=0;
		if (esEntero(cadena)) {
			int exponente=0;
			//convierto el string a int
			for (int i=cadena.length()-1;i>0;i--) {
				//48 es 0 en ascii
				resultado=resultado+((cadena.charAt(i)-48)*(int)Math.pow(10,exponente));
				exponente++;
			}
			if (cadena.charAt(0)=='-') {
				resultado=-resultado;
			} else {
				resultado = resultado + ((cadena.charAt(0) - 48) * (int) Math.pow(10, exponente));
			}
		}
		return resultado;
	}
	
	static int longitudPalabraMasLarga() {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un texto");
		String miTexto= input.nextLine();
		int longitudPalabra =0;
		int textoLeido=0;
		int espacioInicio=0;
		int cuentaEspacios	=0;
		for (int i = 0; i< miTexto.length(); i++) {
			textoLeido++;
				if (miTexto.charAt(i)==' ') {
					cuentaEspacios++;	
					int resta= textoLeido-espacioInicio - cuentaEspacios;
						if (resta>longitudPalabra) {
							longitudPalabra=resta;
						}
				espacioInicio=i;
				}		
				if (i==(miTexto.length()-1)) {
					int resta2= textoLeido-espacioInicio - 1;
					if (resta2>longitudPalabra) {
						longitudPalabra=resta2;
					}
			}
			
		}
		
		input.close();
		return longitudPalabra;
	}
	static String palabraMasLarga(String palabras) {
		if(!palabras.contains(" ")){
			return palabras;
		}
		//Si hay espacios, cojo la primera palabra
		String palabraLarga = palabras.substring(0, palabras.indexOf(' '));
		String palabraEncontrada;
		
		int espacioInicio = palabras.indexOf(' '); 
		
		for(int i = espacioInicio + 1; i < palabras.length(); i++){
			//Si me encuentro un espacio o el final de la cadena
			if(palabras.charAt(i) == ' ' || i == (palabras.length() - 1)){
				//Extraigo la palabra
				palabraEncontrada = palabras.substring(espacioInicio +1, i);
				//Si su longitud es mayor a la que ya tenia, la guardo
				if(palabraEncontrada.length() > palabraLarga.length()){
					palabraLarga = palabraEncontrada; 
				}
				espacioInicio = i;
			}	
			
		}
		return palabraLarga;
	}
	
	static String invertirCadena(String texto) {
		String textoInvertido="";
		for (int i =( texto.length()-1); i>=0;i--) {
			textoInvertido+=texto.charAt(i);
		}
		return textoInvertido;
	}
	
	static int absoluto(int a) {
		if (a>=0) {
			return a;
		} else {
			return -a;
		}
	}
	static double absoluto(double a) {
		if (a<0) {
			return -a;
		}
		return a;
	}
	//static int maximo(int n1, int n2) {
	//	int max=(n1>n2)?n1:n2;
	//	return max;
	//}
	static double maximo(double a, double b) {
		double max=(a>b)?a:b;
		return max;
	}
	static double minimo(double n3, double n4) {
		double min=(n3<n4)?n3:n4;
		return min;
	}
	static long redondear(double a) {
		//if ((a - (int)a)>=0.5) {
		//	return ((int)a+1);
		//}
		//return (int)a;
		return Math.round(a);
	}
	static int redondearAlza(double a) {
		return (int)a+1;
	}
	static int redondearBaja(double a) {
		return (int)a;
	}
	static int potencia(int base, int exponente) {
		int resultado =1;
		for (int i =1; i<=exponente;i++) {
			resultado=resultado*base;
		}
		return resultado;
	}
	//Genera n� aleatorio entre 0 y final
	static int aleatorio2(int fin) {
		return (int)(Math.random() *(fin+1));
	}
	int aleatorio2(int inicio, int fin) {
		return (int)(Math.random() *(fin+1)) - inicio;
	}
}
