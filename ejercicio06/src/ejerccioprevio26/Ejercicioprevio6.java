package ejerccioprevio26;

import java.util.Scanner;

public class Ejercicioprevio6 {
static Scanner input =new Scanner(System.in);

	public static void main(String[] args) {
		// llamo al m�todo mostrarNombre
		mostrarNombre();
		//llamo al metodo calcularPrecio
		double precio=calcularPrecio();
		System.out.println("El precio total es "+precio);
		//llamo al m�todo sumaDatos
		double miSuma=sumaDatos(2,3);
		System.out.println("La suma es " + miSuma);
		//llamo al m�todo sumaDatos(3 par�metrros)
		double miSuma1=sumaDatos(2,3,4);
		System.out.println("La suma es " + miSuma1);

		input.close();
	}
	// metodo void
	static void mostrarNombre() {
		System.out.println("dame tu nombre");
		String nombre=input.nextLine();
		System.out.println("Tu nombre es "+ nombre);
	}
	//metodo double
	static double calcularPrecio() {
		System.out.println("Dame una cantidad");
		double cantidad=input.nextDouble();
		double total=cantidad*1.21;
		return total;
	}
	//m�todo double con par�metros
	static double sumaDatos(double numero1, double numero2) {
		return numero1 + numero2;
	}
	
	//metodo sobrecargado
	static double sumaDatos(double numero1, double numero2, double numero3) {
		return numero1 + numero2 + numero3;
	}

}
