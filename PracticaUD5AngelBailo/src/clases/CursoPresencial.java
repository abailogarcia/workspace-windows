package clases;

public class CursoPresencial extends Curso{
	private int numeroAula;

	public CursoPresencial(String codigo, String nombre, int duracion, int numeroAula) {
		super(codigo, nombre, duracion);
		this.numeroAula = numeroAula;
	}

	public int getNumeroAula() {
		return numeroAula;
	}

	public void setNumeroAula(int numeroAula) {
		this.numeroAula = numeroAula;
	}

	@Override
	public String toString() {
		return "CursoPresencial [numeroAula=" + numeroAula + ", toString()=" + super.toString() + "]";
	}
	
}
