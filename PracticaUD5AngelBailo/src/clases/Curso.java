package clases;

import java.util.ArrayList;

public class Curso {
	private String codigo;
	private String nombre;
	private int duracion;
	private ArrayList<Alumno> listaAlumnos;
	public Curso(String codigo, String nombre, int duracion) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.duracion = duracion;
		listaAlumnos = new ArrayList<Alumno>();
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getDuracion() {
		return duracion;
	}
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	public ArrayList<Alumno> getListaAlumnos() {
		return listaAlumnos;
	}
	public void setListaAlumnos(ArrayList<Alumno> listaAlumnos) {
		listaAlumnos = listaAlumnos;
	}
	@Override
	public String toString() {
		return "Curso [codigo=" + codigo + ", nombre=" + nombre + ", duracion=" + duracion + ", ListaAlumnos="
				+ listaAlumnos + "]";
	}
	
	
}
