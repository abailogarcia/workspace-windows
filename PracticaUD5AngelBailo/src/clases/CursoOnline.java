package clases;

public class CursoOnline extends Curso{
	private String plataforma;

	public CursoOnline(String codigo, String nombre, int duracion, String plataforma) {
		super(codigo, nombre, duracion);
		this.plataforma = plataforma;
	}

	public String getPlataforma() {
		return plataforma;
	}

	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}

	@Override
	public String toString() {
		return "CursoOnline [plataforma=" + plataforma + ", toString()=" + super.toString() + "]";
	}
	
}
