package clases;

import java.util.ArrayList;
import java.util.Iterator;

public class GestorCursos {
	private ArrayList<Alumno> listaAlumnos;
	private ArrayList<Curso> listaCursos;

	public GestorCursos() {
		listaAlumnos = new ArrayList<Alumno>();
		listaCursos = new ArrayList<Curso>();
	}

	public ArrayList<Alumno> getListaAlumnos() {
		return listaAlumnos;
	}

	public void setListaAlumnos(ArrayList<Alumno> listaAlumnos) {
		this.listaAlumnos = listaAlumnos;
	}

	public ArrayList<Curso> getListaCursos() {
		return listaCursos;
	}

	public void setListaCursos(ArrayList<Curso> listaCursos) {
		this.listaCursos = listaCursos;
	}

	// alta alumno
	public void altaAlumno(String dni, String nombre, String apellido, int edad) {
		Alumno alumno = new Alumno(dni, nombre, apellido, edad);
		listaAlumnos.add(alumno);
	}
	//existe alumno
	public boolean existeAlumno(String dni) {
		for (Alumno alum : listaAlumnos) {
			if (alum != null && alum.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}
	// buscar alumno
	public Alumno buscarAlumno(String dni) {
		for (Alumno alum : listaAlumnos) {
			if (alum != null && alum.getDni().equals(dni)) {
				return alum;
			}
		}
		return null;
	}

	// modificar nombre de alumno
	public void cambiarNombreAlumno(String dni, String nuevoNombre) {
		if (buscarAlumno(dni) != null) {
			buscarAlumno(dni).setNombre(nuevoNombre);
		}
	}

	// alta curso
	public void altaCurso(String codigo, String nombre, int duracion) {
		Curso curso = new Curso(codigo, nombre, duracion);
		listaCursos.add(curso);
	}

	// alta curso online
	public void altaCursoOnline(String codigo, String nombre, int duracion, String plataforma) {
		CursoOnline cursoOnline = new CursoOnline(codigo, nombre, duracion, plataforma);
		listaCursos.add(cursoOnline);
	}

	// alta curso presencial
	public void altaCursoPresencial(String codigo, String nombre, int duracion, int numeroAula) {
		CursoPresencial cursoPresencial = new CursoPresencial(codigo, nombre, duracion, numeroAula);
		listaCursos.add(cursoPresencial);
	}
	//existe curso
		public boolean existeCurso(String codigo) {
			for (Curso cursico : listaCursos) {
				if (cursico != null && cursico.getCodigo().equals(codigo)) {
					return true;
				}
			}
			return false;
		}
	// buscar curso
	public Curso buscarCurso(String codigo) {
		for (Curso cursico : listaCursos) {
			if (cursico != null && cursico.getCodigo().equals(codigo)) {
				return cursico;
			}
		}
		return null;
	}

	// buscar curso online
	public CursoOnline buscarCursoOnline(String codigo) {
		for (Curso cursico : listaCursos) {
			if (cursico != null && cursico.getCodigo().equals(codigo) && cursico instanceof CursoOnline) {
				return (CursoOnline) cursico;
			}
		}
		return null;
	}

	// buscar curso presencial
	public CursoPresencial buscarCursoPresencial(String codigo) {
		for (Curso cursico : listaCursos) {
			if (cursico != null && cursico instanceof CursoPresencial && cursico.getCodigo().equals(codigo)) {
				return (CursoPresencial) cursico;
			}
		}
		return null;
	}

	// modificar nombre curso
	public void modificarNombreCurso(String codigo, String nuevoNombre) {
		if(buscarCurso(codigo)!=null) {
		buscarCurso(codigo).setNombre(nuevoNombre);
		} else {
			System.out.println("El curso no existe");
		}
	}

	// eliminar alumno
	public void eliminarAlumno(String dni) {
		for (Curso cursico : listaCursos) {
			Iterator<Alumno> iteradorAlumno = cursico.getListaAlumnos().iterator();
			while (iteradorAlumno.hasNext()) {
				Alumno alum = iteradorAlumno.next();
				if (alum.getDni().equals(dni)) {
					iteradorAlumno.remove();
				}
			}
		}

		Iterator<Alumno> iteradorAlum = listaAlumnos.iterator();
		while (iteradorAlum.hasNext()) {
			Alumno alumno = iteradorAlum.next();
			if (alumno.getDni().equals(dni)) {
				iteradorAlum.remove();
			}
		}

	}

	// eliminar curso
	public void eliminarCurso(String codigo) {
		Iterator<Curso> iteradorCurso = listaCursos.iterator();
		while (iteradorCurso.hasNext()) {
			Curso cursico = iteradorCurso.next();
			if (cursico.getCodigo().equals(codigo)) {
				iteradorCurso.remove();
			}
		}
	}

	// eliminar curso online
	public void eliminarCursoOnline(String codigo) {
		Iterator<Curso> iteradorCurso = listaCursos.iterator();
		while (iteradorCurso.hasNext()) {
			CursoOnline cursico = (CursoOnline) iteradorCurso.next();
			if (cursico.getCodigo().equals(codigo)) {
				iteradorCurso.remove();
			}
		}
	}

	// eliminar curso presencial
	public void eliminarCursoPresencial(String codigo) {
		Iterator<Curso> iteradorCurso = listaCursos.iterator();
		while (iteradorCurso.hasNext()) {
			CursoPresencial cursico = (CursoPresencial) iteradorCurso.next();
			if (cursico.getCodigo().equals(codigo)) {
				iteradorCurso.remove();
			}
		}
	}

	// comprobar alumno en curso
	public boolean comprobarAlumnoCurso(String codigo, String dni) {
		Curso cursico = buscarCurso(codigo);
		for (Alumno alum : cursico.getListaAlumnos()) {
			if (alum.getDni().equals(dni)) {
				return true;

			}
		}
		return false;
	}

	// inscribir alumno en curso
	public void inscribirAlumnoCurso(String codigo, String dni) {
		Alumno alum = buscarAlumno(dni);
		Curso cursico = buscarCurso(codigo);
		cursico.getListaAlumnos().add(alum);
	}

	public void inscribirAlumnoCursoOnline(String codigo, String dni) {
		Alumno alum = buscarAlumno(dni);
		CursoOnline cursico = (CursoOnline) buscarCurso(codigo);
		cursico.getListaAlumnos().add(alum);
	}

	public void inscribirAlumnoCursoPresencial(String codigo, String dni) {
		Alumno alum = buscarAlumno(dni);
		CursoPresencial cursico = (CursoPresencial) buscarCurso(codigo);
		cursico.getListaAlumnos().add(alum);
	}

	// listados
	public void listarAlumnos() {
		for (Alumno alum : listaAlumnos) {
			if (alum != null) {
				System.out.println(alum);
			}
		}
	}

	public void listarUnAlumno(String dni) {
		for (Alumno alum : listaAlumnos) {
			if (alum != null && alum.getDni().equals(dni)) {
				System.out.println(alum);
			} else {
				System.out.println("El alumno " + dni + " no existe");
			}
		}
	}

	public void listarAlumnosPorCurso(String codigo) {
		Curso cursico = buscarCurso(codigo);
		System.out.println("Lista de alumnos del curso " + cursico.getCodigo() + " " + cursico.getNombre());
		for (Alumno alum : cursico.getListaAlumnos()) {
			if (alum != null) {
				System.out.println(alum);
			}
		}
	}

	public void listarCursos() {
		for (Curso cursico : listaCursos) {
			if (cursico != null) {
				System.out.println(cursico);
			}
		}
	}

	public void listarUnCurso(String codigo) {
		if (buscarCurso(codigo) != null) {
			System.out.println(buscarCurso(codigo));
		} else {
			System.out.println("El curso "+codigo+" no existe");
		}
	}

	public void listarCursosDeUnAlumno(String dni) {
		if (buscarAlumno(dni) != null) {
			System.out.println("Lista de cursos del alumno " + dni);
			for (Curso cursico : listaCursos) {
				for (Alumno alum : cursico.getListaAlumnos()) {
					if (alum.getDni().equals(dni)) {
						System.out.println(cursico.getNombre());
					}
				}
			}
		} else {
			System.out.println("El alumno " + dni + " no existe");
		}
	}
	@Override
	public String toString() {
		return "GestorCursos [listaAlumnos=" + listaAlumnos + ", listaCursos=" + listaCursos + "]";
	}
}
