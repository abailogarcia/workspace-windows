package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.GestorCursos;

public class GestorTest {
	static GestorCursos gestor = new GestorCursos();
	
	@BeforeAll
	public static void inscribirAlumnosEnCursos() {
		System.out.println("Before All");
		System.out.println("Insertamos alumnos");
		gestor.altaAlumno("11a", "Alberto", "Sanchez", 20);
		gestor.altaAlumno("22b", "Santiago", "Lopez", 21);
		gestor.altaAlumno("33c", "Ricardo", "Gracia", 19);
		gestor.listarAlumnos();
		gestor.altaCurso("111", "curso1", 10);
		gestor.altaCurso("222", "curso2", 20);
		gestor.altaCurso("333", "curso3", 30);
		gestor.listarCursos();
	}
	
	@Test
	public void inscribirAlumnoCurso() {
		System.out.println("Asignar alumno a curso");
		gestor.inscribirAlumnoCurso("111", "11a");
		assertEquals(gestor.existeAlumno("11a"), gestor.buscarCurso("111").getListaAlumnos().contains(gestor.buscarAlumno("11a")));
		System.out.println("Comprobamos");
		System.out.println(gestor.existeAlumno("11a"));
		System.out.println(gestor.buscarCurso("111").getListaAlumnos().contains(gestor.buscarAlumno("11a")));
	}
	@Test
	public void inscribirAlumnoCurso2() {
		System.out.println("Asignar alumno a curso2");
		gestor.inscribirAlumnoCurso("222", "22b");
		assertNotEquals(gestor.existeAlumno("22b"), gestor.buscarCurso("111").getListaAlumnos().contains(gestor.buscarAlumno("22b")));
		System.out.println("Comprobamos");
		System.out.println(gestor.existeAlumno("22b"));
		System.out.println(gestor.buscarCurso("111").getListaAlumnos().contains(gestor.buscarAlumno("22b")));
	}
	@AfterAll
	public static void mostrarAlumnosCursos() {
		System.out.println("After All");
		gestor.listarAlumnos();
		gestor.listarCursos();
		System.out.println("Se acab�");
	}
}
