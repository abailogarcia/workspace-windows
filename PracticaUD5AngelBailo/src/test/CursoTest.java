package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;

//import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Alumno;
import clases.Curso;
import clases.GestorCursos;

public class CursoTest {
	static GestorCursos gestor = new GestorCursos();
	
	@BeforeAll
	public static void insertarCurso() {
		System.out.println("Before All");
		gestor.altaCurso("111", "curso1", 10);
		gestor.altaCurso("222", "curso2", 20);
		gestor.altaCurso("333", "curso3", 30);
		gestor.listarCursos();
		System.out.println();
	}
	@Test
	public void testExisteCurso() {
		System.out.println("Test existe curso");
		boolean resultado = gestor.existeCurso("111");
		assertTrue (resultado);
		System.out.println("Comprueba existencia");
		System.out.println(resultado);
		System.out.println();
	}
	@Test
	public void testExisteCurso2() {
		System.out.println("Test existe curso que falla");
		boolean resultado = gestor.existeCurso("112");
		assertFalse (resultado);
		System.out.println("Comprueba no existencia");
		System.out.println(resultado);
		System.out.println();
	}
	@Test
	public void buscarCursos() {
		System.out.println("Buscar cursos");
		Curso curso4 = new Curso("444", "curso4", 10);
		gestor.getListaCursos().add(curso4);
		assertEquals(curso4, gestor.buscarCurso("444"));
		System.out.println("Comprobamos");
		System.out.println(curso4);
		System.out.println(gestor.buscarCurso("444"));
		System.out.println();
	}
	@Test
	public void buscarCursos2() {
		System.out.println("Buscar cursos que falla");
		Curso curso5 = new Curso("555", "curso5", 11);
		gestor.getListaCursos().add(curso5);
		assertNotEquals(curso5, gestor.buscarCurso("4445"));
		System.out.println("Comprobamos");
		System.out.println(curso5);
		System.out.println(gestor.buscarCurso("4445"));
		System.out.println();
	}
	@Test 
	public void testCambiarNombreCurso() {
		System.out.println("Test de cambio de nombre a curso");
		Curso curso6 = new Curso("666", "curso6", 19);
		gestor.getListaCursos().add(curso6);
		gestor.modificarNombreCurso("666", "nuevoCurso");
		gestor.listarUnCurso("666");
		assertEquals(gestor.buscarCurso("666").getNombre(), curso6.getNombre());
		System.out.println("Comprobamos");
		System.out.println(gestor.buscarCurso("666").getNombre());
		System.out.println(curso6.getNombre());
		System.out.println();
	}
	@Test 
	public void testCambiarNombreCurso2() {
		System.out.println("Test de cambio de nombre a curso que falla");
		Curso curso7 = new Curso("777", "curso7", 19);
		gestor.getListaCursos().add(curso7);
		gestor.modificarNombreCurso("7775", "nuevoCurso");
		assertNotEquals(curso7.getNombre(), "nuevoCurso");
		System.out.println("Comprobamos la diferencia");
		System.out.println(curso7.getNombre());
		System.out.println("nuevoCurso");
		System.out.println();
	}
	@Test
	public void testEliminarCurso() {
		System.out.println("Test de borrado de curso");
		Curso curso8 = new Curso("888", "curso8", 20);
		gestor.getListaCursos().add(curso8);
		gestor.eliminarCurso("888");
		assertFalse(gestor.existeCurso("888"));
		System.out.println("Comprobamos");
		System.out.println("El curso existe? " +gestor.existeCurso("888"));
		System.out.println();
	}
	@Test
	public void testEliminarCurso2() {
		System.out.println("Test de borrado de curso que falla");
		Curso curso9 = new Curso("999", "curso9", 20);
		gestor.getListaCursos().add(curso9);
		gestor.eliminarCurso("9997");
		assertTrue(gestor.existeCurso("999"));
		System.out.println("Comprobamos si lo ha borrado");
		System.out.println("El curso existe? " +gestor.existeCurso("999"));
		System.out.println();
	}
	@AfterAll
	public static void mostrarCursos() {
		System.out.println("Mostramos los cursos");
		gestor.listarCursos();
		System.out.println("Se acab�");
		
	}
}
