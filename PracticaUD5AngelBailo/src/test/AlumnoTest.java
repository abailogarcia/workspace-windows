package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Alumno;
import clases.GestorCursos;

class AlumnoTest {
	
	static GestorCursos gestor = new GestorCursos();
	
	@BeforeAll
	public static void insertarAlumnos() {
		System.out.println("Before All");
		System.out.println("Insertamos alumnos");
		gestor.altaAlumno("11a", "Alberto", "Sanchez", 20);
		gestor.altaAlumno("22b", "Santiago", "Lopez", 21);
		gestor.altaAlumno("33c", "Ricardo", "Gracia", 19);
		gestor.listarAlumnos();
		System.out.println();
	}
	@Test
	public void testExisteAlumno() {
		System.out.println("Test existe alumno");
		boolean resultado = gestor.existeAlumno("11a");
		assertTrue (resultado);
		System.out.println("Comprueba existencia");
		System.out.println(resultado);
		System.out.println();
	}
	@Test
	public void testExisteAlumno2() {
		System.out.println("Test existe alumno que falla");
		boolean resultado = gestor.existeAlumno("11b");
		assertFalse (resultado);
		System.out.println("Comprueba no existencia");
		System.out.println(resultado);
		System.out.println();
	}
	@Test
	public void testBuscarAlumno() {
		System.out.println("Test b�squeda alumnos");
		Alumno alumno1 = new Alumno("44d", "Pepe", "Lamata", 22);
		gestor.getListaAlumnos().add(alumno1);
		assertEquals(alumno1, gestor.buscarAlumno("44d"));
		System.out.println("Comprobamos");
		System.out.println(alumno1);
		System.out.println(gestor.buscarAlumno("44d"));
		System.out.println();
	}
	@Test
	public void testBuscarAlumno2() {
		System.out.println("Test b�squeda alumnos que falla");
		Alumno alumno2 = new Alumno("55e", "Sara", "Lagos", 18);
		gestor.getListaAlumnos().add(alumno2);
		assertNotEquals(alumno2, gestor.buscarAlumno("55ef"));
		System.out.println("Comprobamos diferente");
		System.out.println(alumno2);
		System.out.println(gestor.buscarAlumno("55ef"));
		System.out.println();
	}
	@Test 
	public void testCambiarNombreAlumno() {
		System.out.println("Test de cambio de nombre a alumno");
		Alumno alumno3 = new Alumno("66f", "Clara", "Ibanez", 19);
		gestor.getListaAlumnos().add(alumno3);
		gestor.cambiarNombreAlumno("66f", "Marta");
		assertEquals(alumno3.getNombre(), "Marta");
		System.out.println("Comprobamos");
		System.out.println(alumno3.getNombre());
		System.out.println("Marta");
		System.out.println();
	}
	@Test 
	public void testCambiarNombreAlumno2() {
		System.out.println("Test de cambio de nombre a alumno que falla");
		Alumno alumno4 = new Alumno("77g", "Laura", "Jaime", 19);
		gestor.getListaAlumnos().add(alumno4);
		gestor.cambiarNombreAlumno("66f", "Marta");
		assertNotEquals(alumno4.getNombre(), "Marta");
		System.out.println("Comprobamos diferente");
		System.out.println(alumno4.getNombre());
		System.out.println("Marta");
		System.out.println();
	}
	@Test
	public void testEliminarAlumno() {
		System.out.println("Test de borrado de alumno");
		Alumno alumno5 = new Alumno("88h", "Pili", "Tejar", 22);
		gestor.getListaAlumnos().add(alumno5);
		gestor.eliminarAlumno("88h");
		assertFalse(gestor.existeAlumno("88h"));
		System.out.println("Comprobamos");
		System.out.println("El alumno existe? " +gestor.existeAlumno("88h"));
		System.out.println();
	}
	
	@AfterAll
	public static void mostrarAlumnos() {
		System.out.println("After All");
		gestor.listarAlumnos();
		System.out.println("Se acab�");
	}
}
