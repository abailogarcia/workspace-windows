package programa;

import clases.GestorCursos;

public class Programa {

	public static void main(String[] args) {
		GestorCursos gestor = new GestorCursos();
		System.out.println("Creamos alumnos");
		gestor.altaAlumno("1a", "angel", "bailo", 21);
		gestor.altaAlumno("2b", "paco", "perez", 17);
		gestor.altaAlumno("3c", "juan", "garcia", 14);
		gestor.listarAlumnos();
		System.out.println();
		System.out.println("Creamos cursos");
		gestor.altaCursoPresencial("p1", "Excel", 20, 1);
		gestor.altaCursoPresencial("p2", "word", 15, 2);
		gestor.altaCursoOnline("o1", "bombero", 200, "edmodo");
		gestor.altaCursoOnline("o2", "auxiliar", 150, "moodle");
		gestor.listarCursos();
		System.out.println();
		System.out.println("Asignamos alumnos a cursos");
		gestor.inscribirAlumnoCurso("p1", "1a");
		gestor.inscribirAlumnoCurso("o1", "1a");
		gestor.inscribirAlumnoCurso("p2", "2b");
		gestor.inscribirAlumnoCurso("o2", "3c");
		gestor.inscribirAlumnoCurso("p1", "2b");
		gestor.listarCursos();
		System.out.println();
		//System.out.println("Lista de alumnos del curso p1");
		gestor.listarAlumnosPorCurso("p1");
		System.out.println();
		//System.out.println("Lista de cursos del alumno 1a");
		gestor.listarCursosDeUnAlumno("1a");
		System.out.println();
		System.out.println("Cambiamos el nombre del alumno 1a");
		gestor.cambiarNombreAlumno("1a", "Fernando");
		gestor.listarUnAlumno("1a");
		System.out.println("Elimino el alumno 1a");
		gestor.eliminarAlumno("1a");
		gestor.listarAlumnos();
		gestor.listarCursosDeUnAlumno("1a");
		gestor.listarCursos();
		System.out.println();
		System.out.println("Elimino el curso o2");
		gestor.eliminarCurso("o2");
		gestor.listarUnCurso("o2");;
	}

}
