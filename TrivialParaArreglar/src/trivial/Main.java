package trivial;

import java.util.Scanner;

//import java.util.*;
/*
 * Error 1 import java.util.*  s�lo hay que importar lo necesario
 * Error 2 Scanner no cerrado e importarmos java.util.Scanner
 * Error 3 Nombre de variables no adecuados (mariano, rajoy...)
 * Error 4 Se inicializan las variables a y e a 0 pero luego s�lo se usan para sumar 0. Las borro del c�lculo de aciertos y errores
 * Error 5 sobra el + pregunta1[4] 2[4] y 3[4]= "5. �Cual no es un tipo de placa base?\na)ATX\nb)ITX\n" + "c)OTX";
 * Error 6 las variables informatica, fechas y geografia, sobran porque s�lo se usan para medir su longitud en los for
 * Lo cambio por pregunta1.length()
 * Error 7 la variable trivial no se usa m�s que para cambiar el nombre a la variable preguntai 
 * Error 8 Mejor el Scanner dentro del main
 * 
 * */
 
public class Main {
	//static Scanner teclado = new Scanner(System.in);

	public static void main(String[] args) {
		 Scanner teclado = new Scanner(System.in);
	//	String[] trivial=new String[15];
	//	String[] informatica = new String[5];
	//	String[] fechas = new String[5];
	//	String[] geografia = new String[5];

		String[] pregunta1 = new String[5];
		String[] pregunta2 = new String[5];
		String[] pregunta3 = new String[5];

		String[] respuesta1 = new String[5];
		String[] respuesta2 = new String[5];
		String[] respuesta3 = new String[5];

		int aciertosInformatica = 0;
		int fallosInformatica = 0;
		int aciertosFechas = 0;
		int fallosFechas = 0;
		int aciertosGeografia = 0;
		int fallosGeografia = 0;
		/*
		int a=0;
		int e=0;
		*/
		int aciertos=0;
		int errores=0;
		
		pregunta1[0] = "1. �Cual es el lenguaje de programacion mas usado?\na)Java\nb)MySQL\nc)HTML5";
		pregunta1[1] = "2. �Como se hace referencia al padre de una herencia?\na)upper();\nb)super();\nc)supper();";
		pregunta1[2] = "3. �Que utilidad tiene input.nextLine();?\na)Saltar de linea\nb)Limpiar el buffer\nc)Escribir las frases juntas";
		pregunta1[3] = "4. �Que version de HTML se usa actualmente?\na)5\nb)4\nc)6";
		pregunta1[4] = "5. �Cual no es un tipo de placa base?\na)ATX\nb)ITX\nc)OTX";
	//	pregunta1[4] = "5. �Cual no es un tipo de placa base?\na)ATX\nb)ITX\n" + "c)OTX";

		pregunta2[0] = "1. �En que anno llego el hombre a la luna?\na)1971\nb)1967\nc)1969";
		pregunta2[1] = "2. �Que periodo corresponde a la Guerra Civil Espannola?\na)1939-1945\nb)1936-1939\nc)1933-1936";
		pregunta2[2] = "3. �Cuando termino la II Guerra Mundial?\na)1955\nb)1944\nc)1945";
		pregunta2[3] = "4. �En que anno fue el atentado del 11S?\na)2000\nb)2001\nc)1999";
		//pregunta2[4] = "5. En el anno 1989:\na)Aparecio World Wide Web (www)\nb)Se disolvio la URSS\n"
		//		+ "c)Tuvo lugar la caida del muro de Berlin";
		pregunta2[4] = "5. En el anno 1989:\na)Aparecio World Wide Web (www)\nb)Se disolvio la URSS\nc)Tuvo lugar la caida del muro de Berlin";

		pregunta3[0] = "1. �Cuantos continentes hay en el mundo?\na)5\nb)6\nc)7";
		pregunta3[1] = "2. �Cual es el rio europeo mas largo?\na)Rio Danubio\nb)Rio Volga\nc)Rio Tamesis";
		pregunta3[2] = "3. �Qu� pa�s tiene mayor n�mero de habitantes?\na)Rusia\nb)China\nc)India";
		pregunta3[3] = "4. Las ruinas de Petra, �a que pais pertenecen?\na)Jordania\nb)Siria\nc)Irak";
		//pregunta3[4] = "5. �Cual es la capital de Estados Unidos?\na)Los ngeles\nb)New York\n" + "c)Washigton DC";
		pregunta3[4] = "5. �Cual es la capital de Estados Unidos?\na)Los ngeles\nb)New York\nc)Washigton DC";

		System.out.println("� PREGUNTAS DE INFORMATICA �");
		for (int i = 0; i < pregunta1.length; i++) {
			System.out.println(pregunta1[i]);
		//	trivial[i]=pregunta1[i];
			respuesta1[i] = teclado.nextLine();
			
			if(!(respuesta1[i]).equalsIgnoreCase("a") && !(respuesta1[i]).equalsIgnoreCase("b") && !(respuesta1[i]).equalsIgnoreCase("c")) {
			do {
			System.out.println("Respuesta no contemplada, vuelva a introducir");
			respuesta1[i]=teclado.nextLine();
			}while(!(respuesta1[i]).equalsIgnoreCase("a") && !(respuesta1[i]).equalsIgnoreCase("b") && !(respuesta1[i]).equalsIgnoreCase("c"));
			}
		}

		System.out.println("\n");
		System.out.println("� PREGUNTAS DE ACONTECIMIENTOS HISTORICOS �");
		for (int i = 0; i < pregunta2.length; i++) {
			System.out.println(pregunta2[i]);
		//	trivial[i]=pregunta2[i];
			respuesta2[i] = teclado.nextLine();
			if(!(respuesta1[i]).equalsIgnoreCase("a") && !(respuesta1[i]).equalsIgnoreCase("b") && !(respuesta1[i]).equalsIgnoreCase("c")) {
			do {
				System.out.println("Respuesta no contemplada, vuelva a introducir");
				respuesta2[i]=teclado.nextLine();
				}while(!(respuesta2[i]).equalsIgnoreCase("a") && !(respuesta2[i]).equalsIgnoreCase("b") && !(respuesta2[i]).equalsIgnoreCase("c"));
			}
		}

		System.out.println("\n");
		System.out.println("� PREGUNTAS DE GEOGRAFIA �");
		for (int i = 0; i < pregunta3.length; i++) {
			System.out.println(pregunta3[i]);
		//	trivial[i]=pregunta3[i];
			respuesta3[i] = teclado.nextLine();
			if(!(respuesta1[i]).equalsIgnoreCase("a") && !(respuesta1[i]).equalsIgnoreCase("b") && !(respuesta1[i]).equalsIgnoreCase("c")) {
			do {
				System.out.println("Respuesta no contemplada, vuelva a introducir");
				respuesta3[i]=teclado.nextLine();
				}while(!(respuesta3[i]).equalsIgnoreCase("a") && !(respuesta3[i]).equalsIgnoreCase("b") && !(respuesta3[i]).equalsIgnoreCase("c"));
			}
		}
		

		if (respuesta1[0].equalsIgnoreCase("a")) {
			aciertosInformatica++;
		} else {
			fallosInformatica++;
		}

		if (respuesta1[1].equalsIgnoreCase("b")) {
			aciertosInformatica++;
		} else {
			fallosInformatica++;
		}

		if (respuesta1[2].equalsIgnoreCase("b")) {
			aciertosInformatica++;
		} else {
			fallosInformatica++;
		}

		if (respuesta1[3].equalsIgnoreCase("a")) {
			aciertosInformatica++;
		} else {
			fallosInformatica++;
		}

		if (respuesta1[4].equalsIgnoreCase("c")) {
			aciertosInformatica++;
		} else {
			fallosInformatica++;
		}

		if (respuesta2[0].equalsIgnoreCase("c")) {
			aciertosFechas++;
		} else {
			fallosFechas++;
		}

		if (respuesta2[1].equalsIgnoreCase("b")) {
			aciertosFechas++;
		} else {
			fallosFechas++;
		}

		if (respuesta2[2].equalsIgnoreCase("c")) {
			aciertosFechas++;
		} else {
			fallosFechas++;
		}

		if (respuesta2[3].equalsIgnoreCase("b")) {
			aciertosFechas++;
		} else {
			fallosFechas++;
		}

		if (respuesta2[4].equalsIgnoreCase("c")) {
			aciertosFechas++;
		} else {
			fallosFechas++;
		}

		if (respuesta3[0].equalsIgnoreCase("c")) {
			aciertosGeografia++;
		} else {
			fallosGeografia++;
		}

		if (respuesta3[1].equalsIgnoreCase("b")) {
			aciertosGeografia++;
		} else {
			fallosGeografia++;
		}

		if (respuesta3[2].equalsIgnoreCase("b")) {
			aciertosGeografia++;
		} else {
			fallosGeografia++;
		}

		if (respuesta3[3].equalsIgnoreCase("a")) {
			aciertosGeografia++;
		} else {
			fallosGeografia++;
		}

		if (respuesta3[4].equalsIgnoreCase("c")) {
			aciertosGeografia++;
		} else {
			fallosGeografia++;
		}
		
		
		System.out.println("----------------------");
		System.out.println("Aciertos informatica: " + aciertosInformatica);
		System.out.println("Errores informatica: " + fallosInformatica);
		System.out.println("----------------------");
		System.out.println("Aciertos fechas: " + aciertosFechas);
		System.out.println("Errores fechas: " + fallosFechas);
		System.out.println("----------------------");
		System.out.println("Aciertos geografia: " + aciertosGeografia);
		System.out.println("Errores geografia: " + fallosGeografia);
		System.out.println("----------------------");
		aciertos=aciertosInformatica+aciertosFechas+aciertosGeografia;
		errores=fallosInformatica+fallosFechas+fallosGeografia;
		
		System.out.println("Aciertos totales: " + aciertos);
		System.out.println("Errores totales: " + errores);
		System.out.println("----------------------");
		System.out.println("Porcentaje aciertos totales: " + (double) aciertos / 15 * 100 + " %");
		System.out.println("Porcentaje aciertos totales: " + (double) errores / 15 * 100 + " %");
		System.out.println("----------------------");
teclado.close();
	}

}
