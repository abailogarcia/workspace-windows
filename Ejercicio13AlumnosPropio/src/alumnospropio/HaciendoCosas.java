package alumnospropio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class HaciendoCosas {
	private Connection conexion = null;
	PreparedStatement sentencia = null;

	public void conectar() {
		try {
			String servidor = "jdbc:mysql://localhost:3306/";
			String bbdd = "alumnos";
			String user = "root";
			String pass = "";
			conexion = DriverManager.getConnection(servidor + bbdd, user, pass);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void seleccionar() {
		String sentenciaSQL = "SELECT * FROM alumnos";
		try {
			sentencia = conexion.prepareStatement(sentenciaSQL);
			ResultSet resultado = sentencia.executeQuery();
			while (resultado.next()) {
				System.out.println(resultado.getString(1) + " " + resultado.getString(2) + " "
			+ resultado.getString(3) + " " + resultado.getInt(4));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void insertar(String nombre, String apellido, int edad) {
		try {
			String sentenciaSQL = "INSERT INTO alumnos (nombre, apellido, edad) VALUES (?,?,?)";
			sentencia = conexion.prepareStatement(sentenciaSQL);
			sentencia.setString(1, nombre);
			sentencia.setString(2, apellido);
			sentencia.setInt(3, edad);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public void actualizar(int id, String nombre, String apellido, int edad) {
		try {
			String sentenciaSQL = "UPDATE alumnos SET nombre =?, apellido=?, edad=? WHERE id=?";
			sentencia = conexion.prepareStatement(sentenciaSQL);
			sentencia.setString(1, nombre);
			sentencia.setString(2, apellido);
			sentencia.setInt(3, edad);
			sentencia.setInt(4, id);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public void eliminar(int id) {
		try {
		String sentenciaSQL = "DELETE FROM alumnos WHERE id =?";
		PreparedStatement sentencia;
			sentencia = conexion.prepareStatement(sentenciaSQL);
			sentencia.setInt(1, id);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void desconectar() {
		try {
			conexion.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
