package alumnospropio;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		HaciendoCosas misDatos = new HaciendoCosas();
		Scanner input = new Scanner(System.in);
		System.out.println("Conectamos");
		misDatos.conectar();
		
		System.out.println("Mostramos datos");
		misDatos.seleccionar();
		
		System.out.println("Inserto dato");
		System.out.println("Dame el nombre");
		String nombre = input.nextLine();
		System.out.println("Dame el apellido");
		String apellido = input.nextLine();
		System.out.println("Dame la edad");
		int edad = input.nextInt();
		misDatos.insertar(nombre, apellido, edad);
		misDatos.seleccionar();
		input.nextLine();
		
		System.out.println("Actualizamos datos");
		System.out.println("Dame el id");
		int id = input.nextInt();
		input.nextLine();
		System.out.println("Dame el nombre");
		nombre = input.nextLine();
		System.out.println("Dame el apellido");
		apellido = input.nextLine();
		System.out.println("Dame la edad");
		edad = input.nextInt();
		misDatos.actualizar(id, nombre, apellido, edad);
		misDatos.seleccionar();
		input.nextLine();
		
		System.out.println("Borramos datos");
		System.out.println("Dame el id a borrar");
		id=input.nextInt();
		misDatos.eliminar(id);
		misDatos.seleccionar();
		
		System.out.println("Desconecto");
		misDatos.desconectar();
		
		input.close();
	}

}
