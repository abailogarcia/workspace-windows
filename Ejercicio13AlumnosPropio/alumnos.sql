CREATE DATABASE alumnos;
use alumnos;

CREATE TABLE alumnos(
id INTEGER auto_increment primary key,
nombre varchar(50),
apellido varchar(50),
fecha_nacimiento date
);
