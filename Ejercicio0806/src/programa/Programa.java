package programa;

import clases.Fruteria;

public class Programa {

	public static void main(String[] args) {
		// Creamos instancia
		System.out.println("Creamos instancia de fruteria 4 frutas y 3 verduras");
		int maxFrutas=4;
		int maxVerduras=3;
		Fruteria miFruteria=new Fruteria(maxFrutas,maxVerduras);
		System.out.println("Instancia creada");
		
		System.out.println("Damos de alta 4 frutas y 3 verduras");
		miFruteria.altaFruta("NNNN", "Naranja", 1.55, "Fruteria1");
		miFruteria.altaFruta("CCCC", "Manzana", 1.20, "Fruteria1");
		miFruteria.altaFruta("FFFF", "Pera", 0.78, "Fruteria1");
		miFruteria.altaFruta("RRRR", "Melocot�n", 1.60, "Fruteria2");
		miFruteria.altaVerdura("BBBB", "Brocoli", 0.2, "Fruteria1");
		miFruteria.altaVerdura("AAAA", "Alcachofas", 0.3, "Fruteria1");
		miFruteria.altaVerdura("BRB R", "Borraja", 0.4, "Fruteria1");
		
		System.out.println("Listamos frutas");
		miFruteria.listarFruta();
		System.out.println("Listamos verduras");
		miFruteria.listarVerdura();
		
		System.out.println("Buscamos fruta por c�digo NNNN");
		System.out.println(miFruteria.buscarFruta("NNNN"));
		System.out.println("Buscamos verdura por c�digo AAAA");
		System.out.println(miFruteria.buscarVerdura("AAAA"));
		
		System.out.println("Eliminamos la fruta CCCC");
		miFruteria.eliminarFruta("CCCC");
		miFruteria.listarFruta();
		System.out.println("Eliminamos la verdura BRBR");
		miFruteria.eliminarVerdura("BRBR");
		miFruteria.listarVerdura();
		
		System.out.println("Almacenar nueva fruta");
		miFruteria.altaFruta("WWW", "Kiwi", 1.33, "Fruteria3");
		miFruteria.listarFruta();
		System.out.println("Almacenar nueva verdura");
		miFruteria.altaVerdura("PPPP", "Puerros", 1.34, "Fruteria3");
		miFruteria.listarVerdura();
		
		System.out.println("Modificamos fruta www a pera");
		miFruteria.cambiarTipoFruta("WWW", "Pera");
		System.out.println("Modificamos verdura ppp a pimiento");
		miFruteria.cambiarTipoVerdura("PPPP", "Pimineto");
		
		System.out.println("Listamos frutas por fruteria");
		miFruteria.listarFrutasPorFruteria("Fruteria1");
		System.out.println("Listamos verduras por fruteria");
		miFruteria.listarVerdurasPorFruteria("Fruteria1");
		
	}

}
