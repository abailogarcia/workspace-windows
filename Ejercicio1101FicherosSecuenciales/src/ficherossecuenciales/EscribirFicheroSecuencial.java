package ficherossecuenciales;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class EscribirFicheroSecuencial {
	public static String archivo = "datos.txt";
	//creamos una variable para guardar el nombre del archivo
	public static void main(String[] args) {
		try {
			//1.- abrir el fichero para escribir
			PrintWriter f = new PrintWriter(new FileWriter(archivo,true));
			Scanner input = new Scanner (System.in);
			String linea="";
			//true escribir� al fina
			//false escribir� al principio, escribe encima
			//2.- escribo en el final
			System.out.println("Dame el texto a guardar en el fichero");
			linea = input.nextLine();
			f.print("\n"+ linea);
			//3.- cierro el fichero
			f.close();
			input.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
