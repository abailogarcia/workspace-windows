package ficherossecuenciales;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

//cualquier flujo de informaci�n en java necesita un stream(flujo)
		//un flujo es una conexi�n entre el dispositivo de entrada o salida
		//tipos de flujos
			//caracteres(texto)
			//bytes(binario)
		//tipos de archivos
			//texto (no enriquecido)
			//binarios (enriquecidos) video texto im�genes
		//**Clase bytes
			//Reader, Writer
		//**Clase de caracteres
			//InputStreamReader, OutputStreamReader
		//**Modos de acceso
			//secuencial
			//aleatorio
			//lectura BufferedReader
			//escritura PrintWriter
public class LecturaFicheroSecuencial {

	public static void main(String[] args) {
		try {
		//1.- Crear un puntero al archivo
			BufferedReader f= new BufferedReader(new FileReader("datos.txt"));
			//ruta -> "c:\\documentos\\datos.txt"
			//secuencia de escape \n
			
		//2.- Recorrer el archivo linea a linea
		String nombre="";
		nombre=f.readLine();
		//el proceso de leer un fichero secuencialmente finaliza cuando se lee el fin del fichero
		//al final del fichero encuentra (null)
		while (nombre!=null) {
			System.out.println(nombre);
			nombre=f.readLine();
		}
		//3.- Cerrar el archivo
			f.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no existe");
		} catch (IOException e) {
		System.out.println("Error, el archivo no es accesible");
		}
		
		

	}

}
