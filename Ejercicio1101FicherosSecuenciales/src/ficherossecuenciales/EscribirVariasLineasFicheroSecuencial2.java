package ficherossecuenciales;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class EscribirVariasLineasFicheroSecuencial2 {
	public static String archivo = "datos.txt";
	public static void main(String[] args)  {
		try {
		// 1.- Abrir el archivo para escribir
		PrintWriter f = new PrintWriter(new FileWriter(archivo,true));
		Scanner input = new Scanner (System.in);
		
		//2.- Escribir en el archivo linea a linea hasta escribir fin
		//leer linea a linea de teclado
		//mientras la linea no sea fin escribir la linea en el archivo
		System.out.println("Escribiendo lineas en un archivo (fin para acabar)");
		String linea="";
		linea = input.nextLine();
		while (!linea.equalsIgnoreCase("fin")) {
			f.println(linea);
			linea = input.nextLine();
			
		}
		
		//3.- cerrar archivo
		f.close();
		input.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("No se puede escribir en el archivo");
		}
	}

}
