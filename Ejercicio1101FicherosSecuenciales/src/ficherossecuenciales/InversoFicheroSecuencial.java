package ficherossecuenciales;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class InversoFicheroSecuencial {
	public static final String ARCHIVO="datos2.txt";
	public static final String ARCHIVO2="inverso.txt";
	public static void main(String[] args) throws IOException {
		//1.-abrir el archivo para leer y creamos un arraylist
		BufferedReader origen = new BufferedReader(new FileReader(ARCHIVO));
		ArrayList<String> v = new ArrayList<String>();
		
		//2.-leo el archivo y escribo en el vector
		String linea="";
		linea= origen.readLine();
		while(linea!=null) {
			v.add(linea);
			linea=origen.readLine();
		}
		//3.- cierro el archivo
		origen.close();
		//4.- abro el segundo archivo para escribir
		PrintWriter destino = new PrintWriter(new FileWriter(ARCHIVO2,false));
		
		//5.- leo el vector del rev�s y voy escribiendo
		for(int i=v.size()-1; i>=0;i--) {
			destino.println(v.get(i));
		}
		//6.- cierro el archivo
		destino.close();
		//7.- visualizo el primero 
		System.out.println("Visualizo archivo origen");
		visualizarArchivo(ARCHIVO);
		//8.- visualizo el segundo
		System.out.println("Visualizo archivo destino");
		visualizarArchivo(ARCHIVO2);
	}
	public static void visualizarArchivo(String nombreArchivo) {
		try {
			// 1.- abro para lectura
			BufferedReader origen = new BufferedReader(new FileReader(nombreArchivo));
			// 2.- Recorro linea a linea hasta null
			String linea = "";
			linea = origen.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = origen.readLine();
			}
			// 3.- cierro fichero
			origen.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no existe");
			// cierro aplicacion
			System.exit(0);
		} catch (IOException e) {
			System.out.println("fichero no accesible");
			System.exit(0);
		}
	}
}
