package inversoPoo;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Principal {

	public static void main(String[] args) throws IOException {
		// variables
		String nombreArchivo = "datos2.txt";
		
		// crear objeto de clase Archivo
		//uso el constructor que tiene el nombre del archvio
		Archivo unArchivo=new Archivo(nombreArchivo);
		//crear archivo invertido
		//uso el objeto que tiene el nombre del archivo
		unArchivo.crearArchivoInvertido();
		//visualizar archivo origen (m�todo static)
		System.out.println("Mostrar origen");
		Archivo.visualizarArchivo(nombreArchivo);
		//visualizar archivo destino (m�todo no static)
		System.out.println();
		System.out.println("Mostrar destino");
		Archivo.visualizarArchivo("nombreInvertido.txt");

	}

}
