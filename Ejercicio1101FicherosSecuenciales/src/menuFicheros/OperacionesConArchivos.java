package menuFicheros;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Vector;

public class OperacionesConArchivos {
	//atributos
	private String archivo;
	
	//constructor
	public OperacionesConArchivos(String archivo) {
		this.archivo= archivo;
	}
	//m�todos para trabajar con archivos
	
	//crear el archivo desde teclado
	public void crearArchivo() throws IOException {
		System.out.println("CREAR ARCHIVO");
		//1.- abro para lectura y entrada de teclado
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		//2.- abro para escritura y buffer de lectura
		PrintWriter fuente = new PrintWriter(new BufferedWriter(new FileWriter(this.archivo)));
		System.out.println("Creaci�n del archivo");
		System.out.println("Introducir lineas (* para acabar");
		
		//3.- recorro linea a linea hasta *
		String linea = in.readLine();
		while (!linea.equalsIgnoreCase("*")) {
			fuente.println(linea);
			linea=in.readLine();
		}
		System.out.println("Archivo creado");
		
		//4.-cerrar archivo
		fuente.close();
	}
	
	//visualizar informaci�n  archivo 
	public void visualizarArchivo() throws IOException {
		String linea;
		System.out.println("VISUALIZAR ARCHIVO " + this.archivo);
		//1.- abro para lectura
		BufferedReader fuente =new BufferedReader(new FileReader(this.archivo));
		
		//2.- recorro linea a linea hsta null
		linea= fuente.readLine();
		while (linea!=null) {
			System.out.println(linea);
			linea=fuente.readLine();
		}
		//3.- cierro el archivo
		fuente.close();
	}
	
	//Convertir may�sculas min�sculas
	public void convertirMayusMinus(int opcionElegida) {
		String linea;
		System.out.println("CONVERTIR MAY�SCULAS/MIN�SCULAS");
		if (opcionElegida==1) {
			System.out.println("May�sculas");
		}else {
			System.out.println("Min�sculas");
		}
		//declaro arraylist
		ArrayList<String> vectorMayMin = new ArrayList<String>();
		try {
			//1.- conecto en modo lectura
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			//2.- guardo las lineas en el vector
			linea = fuente.readLine();
			while (linea!=null) {
				vectorMayMin.add(linea);
				linea=fuente.readLine();
			}
			//3.- cerrar el archivo
			fuente.close();
			//4.- leo el vector y lo paso a mayusculas y minusculas
			PrintWriter fuenteMayMin = new PrintWriter(new FileWriter(this.archivo));
			for (int i =0; i<vectorMayMin.size(); i++) {
				if(opcionElegida==1) {
					fuenteMayMin.println(vectorMayMin.get(i).toUpperCase());
				} else {
					fuenteMayMin.println(vectorMayMin.get(i).toLowerCase());
				}
			}
			//5.- cierro el archivo
			fuenteMayMin.close();
			
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
	}
	
	//contar lineas
	public int numeroLineas() {
		System.out.println("C0NTAR LINEAS");
		String linea;
		int contadorLineas=0;
		try {
			//1.- abro para lectura
			BufferedReader fuente =new BufferedReader(new FileReader(this.archivo));
			
			//2.- recorro linea a linea hsta null y contar
			linea= fuente.readLine();
			while (linea!=null) {
				contadorLineas++;
				linea=fuente.readLine();
			}
			//3.- cierro el archivo
			fuente.close();
		}catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
		return contadorLineas;
	}
	
	//buscar palabra
	public void buscarPalabra(String valorABuscar) {
		System.out.println("BUSCAR PALABRA EN VECTOR");
		String linea;
		//declaramos un vector
		Vector<String> vectorBuscar = new Vector<String>();
		try {
			//1.- abrir para leer
			BufferedReader fuente =new BufferedReader(new FileReader(this.archivo));
			//2.- recorro linea a linea y metemos las palabras en el vector
			linea= fuente.readLine();
			while(linea!=null) {
				String letra;
				String palabra="";
				//extraemos una a una las letras
				//y vamos a cumulando las palabras
				//al finalizar cada vuelta de for tendremos una palabra
				for(int i= 0; i<linea.length();i++) {
					letra = linea.substring(i,i+1);
					if(letra.equalsIgnoreCase(" ") ==false) {
						palabra = palabra+letra;
					}
				}
				vectorBuscar.add(palabra);
				palabra="";
				linea=fuente.readLine();
			}
			System.out.println("Palabra buscada");
			System.out.println(valorABuscar);
			System.out.println("Vector a buscar");
			System.out.println(vectorBuscar);
			//3.- buscar la palabra en el vector
			int cuentaPalabras=0;
			for(int j=0; j<vectorBuscar.size();j++) {
				String valorEnVector=vectorBuscar.elementAt(j).toString();
				if(valorEnVector.equalsIgnoreCase(valorABuscar)==true) {
					cuentaPalabras++;
				}
			}
			if(cuentaPalabras==0) {
				System.out.println("La palabra no se encuentra en el archivo");
				
			}else {
				System.out.println("La palabra se encuentra en el archivo " + cuentaPalabras + " veces");
			}
			
			//4.- cerrar el archivo
			fuente.close();
			
		}catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
	}
	
	
}
