package programa;

import java.sql.SQLException;

import clases.EmpresaTransporte;

public class Programa {

	public static void main(String[] args) {
		EmpresaTransporte miTransporte = new EmpresaTransporte();
		System.out.println("Alta conductor");
		miTransporte.altaConductor("1234", "Jesus", "B1");
		miTransporte.altaConductor("5678", "Maria", "B2");
		System.out.println("Alta carretillero");
		miTransporte.altaCarretillero("2222", "Lorenzo", 25);
		System.out.println("Alta transporte");
		miTransporte.altaTransporte("1111", "Transporte", "2001-11-04","1234");
		System.out.println("Registrar carretillero transporte");
		miTransporte.registarCarretilleroTransporte("1111", "2222");

		System.out.println("Listar conductores");
		miTransporte.listarConductores();
		System.out.println("Listar carretilleros");
		miTransporte.listarCarretilleros();
		
		System.out.println("Guardar datos");
		miTransporte.guardarDatos();
		System.out.println("Cargar datos");
		/*try {
			miTransporte.cargarDatos();
		} catch (ClassNotFoundException e1) {

			e1.printStackTrace();
		}*/


		System.out.println("Conectar BBDD");
		miTransporte.conectarBBDD();
		System.out.println("Guardar conductores BBDD");
		miTransporte.guardarConductoresBBDD("B1");
		
		System.out.println("Cargar conductores BBDD");
		miTransporte.cargarConductoresBBDD();
		
		System.out.println("Listar conductores");
		miTransporte.listarConductores();
		System.out.println("Listar carretilleros");
		miTransporte.listarCarretilleros();

	}

}
