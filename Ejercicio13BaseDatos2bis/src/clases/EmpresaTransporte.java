package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class EmpresaTransporte {
	private ArrayList<Personal> listaPersonal;
	private ArrayList<Transporte> listaTransporte;
	private Connection conexion;
	
	public EmpresaTransporte() {
		this.listaPersonal = new ArrayList<Personal>();
		this.listaTransporte = new ArrayList<Transporte>();
	}

	public ArrayList<Personal> getListaPersonal() {
		return listaPersonal;
	}

	public void setListaPersonal(ArrayList<Personal> listaPersonal) {
		this.listaPersonal = listaPersonal;
	}

	public ArrayList<Transporte> getListaTransporte() {
		return listaTransporte;
	}

	public void setListaTransporte(ArrayList<Transporte> listaTransporte) {
		this.listaTransporte = listaTransporte;
	}
	
	public void altaConductor(String dni, String nombre, String carnet) {
		listaPersonal.add(new Conductor(dni, nombre, carnet));
		Collections.sort(listaPersonal);
	}
	
	public void altaCarretillero(String dni, String nombre, double costeHora) {
		listaPersonal.add(new Carretillero(dni, nombre, costeHora));
		Collections.sort(listaPersonal);
	}
	public Conductor buscaConductor(String dni) {
		for(Personal persona:listaPersonal) {
			if(persona!=null && persona instanceof Conductor && persona.getDni().equals(dni)) {
				return (Conductor) persona;
			}
		}
		return null;
	}
	public Carretillero buscaCarretillero(String dni) {
		for(Personal persona:listaPersonal) {
			if(persona!=null && persona instanceof Carretillero && persona.getDni().equals(dni)) {
				return (Carretillero) persona;
			}
		}
		return null;
	}
	public Transporte buscaTransporte(String codigo) {
		for(Transporte transporte:listaTransporte) {
			if(transporte!=null && transporte.getCodigo().equals(codigo)) {
				return transporte;
			}
		}
		return null;
	}
	public boolean compruebaConductor(String dni) {
		for(Personal persona:listaPersonal) {
			if(persona!=null && persona instanceof Conductor && persona.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}
	public boolean compruebaCarretillero(String dni) {
		for(Personal persona:listaPersonal) {
			if(persona!=null && persona instanceof Carretillero && persona.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}
	public boolean compruebaTransporte(String codigo) {
		for(Transporte transporte:listaTransporte) {
			if(transporte!=null && transporte.equals(codigo)) {
				return true;
			}
		}
		return false;
	}
	public void altaTransporte(String codigo, String empresa, String fechaTransporte, String dni) {
		if(!compruebaTransporte(codigo)) {
			if(compruebaConductor(dni)) {
				listaTransporte.add(new Transporte(codigo, empresa, fechaTransporte, buscaConductor(dni)));
				Collections.sort(listaTransporte);
			}else {
				System.out.println("No existe el conductor");
			}
		}else {
			System.out.println("El transporte ya existe");
		}
	}
	public void registarCarretilleroTransporte(String codigo, String dni) {
		if(compruebaTransporte(codigo)) {
			if(compruebaCarretillero(dni)) {
				buscaTransporte(codigo).getListaCarretilleros().add(buscaCarretillero(dni));
			}else {
				System.out.println("No existe el carretillero");
			}
		}else {
			System.out.println("No existe el transporte");
		}
	}
	//guardar los datos en fichero
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datos.dat")));
			escritor.writeObject(listaPersonal);
			escritor.writeObject(listaTransporte);
			escritor.close();
		} catch (IOException e) {
			 System.out.println("Error de entrada salida");
		}
	}
	//cargar los datos de fichero
	@SuppressWarnings("unchecked")
	public void cargarDatos() throws ClassNotFoundException {
		try {
			ObjectInputStream escritor = new ObjectInputStream(new FileInputStream("src/datos.dat"));
			listaPersonal = (ArrayList<Personal>) escritor.readObject();
			listaTransporte = (ArrayList<Transporte>) escritor.readObject();
			escritor.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
		System.out.println("Error de entrada salida");
		}
	}
	
	//conectar base datos
	public void conectarBBDD() {
		String servidor = "jdbc:mysql://localhost:3306/transporte3ev";
		try {
			conexion =  DriverManager.getConnection(servidor, "root", "" );
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//guardar los conductores en la bd
	public void guardarConductoresBBDD(String carnet) {
		String query = "INSERT INTO conductores(dni, nombre, carnet) VALUES (?,?,?)";	
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			for(Personal persona: listaPersonal) {
				if(persona instanceof Conductor && ((Conductor) persona).getCarnet().equals(carnet)) {
					sentencia.setString(1, persona.getDni());
					sentencia.setString(2, persona.getNombre());
					sentencia.setString(3, ((Conductor)persona).getCarnet());
					sentencia.executeUpdate();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cargarConductoresBBDD() {
		String query = "SELECT * FROM conductores";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			ResultSet resultado = sentencia.executeQuery();
			while(resultado.next()) {
				listaPersonal.add(new Conductor(resultado.getString(1), resultado.getString(2), resultado.getString(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	//listar conductores
	public void listarConductores() {
		for(Personal persona:listaPersonal) {
			if(persona instanceof Conductor) {
				System.out.println(persona);
			}
		}
	}
	public void listarCarretilleros() {
		for(Personal persona:listaPersonal) {
			if(persona instanceof Carretillero) {
				System.out.println(persona);
			}
		}
	}
}
