package clases;

public class Conductor extends Personal{

	private static final long serialVersionUID = 1L;

	private String carnet;
	
	public Conductor(String dni, String nombre, String carnet) {
		super(dni, nombre);
		this.carnet = carnet;
	}

	public String getCarnet() {
		return carnet;
	}

	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}

	@Override
	public int compareTo(Personal o) {
			return this.getDni().compareTo(o.getDni());
	}

	@Override
	public String toString() {
		return "Conductor [carnet=" + carnet + ", toString()=" + super.toString() + "]";
	}

}
