package ejercicio05matematicas;
/**
 * Ejercicio de Matem�ticas
 * @author angel bailo
 *
 */
public final class Matematicas {

	public static final double PI = 3.1415;

	private Matematicas() {

	}
/**
 * M�todo que calcula el valor absoluto
 * @param a el valor sobre el que se calcula el valor absoluto
 * @return un entero con el valor absoluto
 */
	public static int absoluto(int a) {
		if (a < 0) {
			a = -a;
		}
		return a;
	}
	/**
	 * M�todo que calcula el valor absoluto
	 * @param a el valor sobre el que se calcula el valor absoluto
	 * @return un double con el valor absoluto
	 */
	public static double absoluto(double a) {
		if (a < 0) {
			a = -a;
		}
		return a;
	}
/**
 * M�todo que calcula el m�ximo de 2 enteros
 * @param a valor del primer entero a comaparar
 * @param b valor del segundo entero a comaparar
 * @return un entero con el mayor valor
 */
	public static int maximo(int a, int b) {
		if (a > b) {
			return a;
		}
		return b;
	}
	/**
	 * M�todo que calcula el m�ximo de 2 doubles
	 * @param a valor del primer double a comaparar
	 * @param b valor del segundo double a comaparar
	 * @return un double con el mayor valor
	 */
	public static double maximo(double a, double b) {
		if (a >= b) {
			return a;
		}
		return b;
	}
	/**
	 * M�todo que calcula el m�nimo de 2 enteros
	 * @param a valor del primer entero a comaparar
	 * @param b valor del segundo entero a comaparar
	 * @return un entero con el m�nimo valor
	 */
	public static int minimo(int a, int b) {
		if (a > b) {
			return b;
		}
		return a;
	}
	/**
	 * M�todo que calcula el m�nimo de 2 doubles
	 * @param a valor del primer double a comaparar
	 * @param b valor del segundo double a comaparar
	 * @return un double con el m�nimo valor
	 */
	public static double minimo(double a, double b) {
		if (a > b) {
			return b;
		}
		return a;
	}
/**
 * M�todo para redondear un double
 * @param a es el valor a redondear
 * @return un double con el valor redondeado
 */
	public static int redondear(double a) {

		if (a - (int) a >= 0.5) {
			return (int) a + 1;
		}
		return (int) a;
	}

	public static int redondearAlza(double a) {

		if (a != (int) a) {
			return (int) a + 1;
		}
		return (int) a;

	}

	public static int redondearBaja(double a) {

		return (int) a;
	}

	public static int potencia(int base, int exponente) {
		int resultado = 1;

		for (int i = 0; i < exponente; i++) {
			resultado = resultado * base;
		}
		return resultado;
	}

	public static int aleatorio(int fin) {

		return (int) (random() * (fin + 1));
	}

	public static int aleatorio(int inicio, int fin) {

		return (int) (random() * (fin - inicio)) + inicio;
	}

	public static Double random() {
		// Obtengo nanosegundos de 1 a 999
		int semilla = (int) System.nanoTime() % 1000;
		// lo divido entre 1000
		double aleatorio = semilla / 1000.0;
		return aleatorio;
	}
}
