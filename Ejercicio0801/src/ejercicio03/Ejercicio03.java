package ejercicio03;
import ejercicio01.Vehiculo;
public class Ejercicio03 {

	public static void main(String[] args) {
		// crearr veh�culo con constructor sin par�metros
		System.out.println("Creo veh�culo 1");
		Vehiculo vehiculo1=new Vehiculo();
		System.out.println(vehiculo1.getVehiculoCreado());
		
		//crear veh�culo con constructor con 2 par�metros
		//mostrar el veh�culo
		System.out.println("Creo veh�culo 2");
		Vehiculo vehiculo2=new Vehiculo("moto","yamaha");
		System.out.println(vehiculo1.getVehiculoCreado());
		System.out.println(vehiculo2.getVehiculoCreado());
		//crear veh�culo con constructor con 4 par�metros
		//mostrar veh�culos creado
		System.out.println("Creo veh�culo 3");
		Vehiculo vehiculo3=new Vehiculo("coche","audi", 5.5F, 4);
		System.out.println(vehiculo1.getVehiculoCreado());
		System.out.println(vehiculo2.getVehiculoCreado());
		System.out.println(vehiculo3.getVehiculoCreado());
		
	}

}
