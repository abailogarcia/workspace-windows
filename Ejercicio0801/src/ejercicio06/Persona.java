package ejercicio06;

import java.time.LocalDate;

public class Persona {
	//atributos
	private String nombre;
	private String apellidos;
	private LocalDate fechaNacimiento;
	private boolean esHombre;
	private double altura;
	private double peso;
	//constructores
	public Persona(String nombre, String apellidos) {
		this.nombre=nombre;
		this.apellidos=apellidos;
	}
	public Persona(String nombre, String apellidos, String fechaNacimiento, boolean esHombre ) {
		//llamo al constructor de la clase persona con 2 parámetros
		this(nombre,apellidos);
		//recibo un parámetro String pero lo tengo que guardar en un localdate
		//tengo que parsearlo
		//de este modo recibo un string y lo guardo en un localdate
		this.fechaNacimiento=LocalDate.parse(fechaNacimiento);
		this.esHombre=esHombre;
	}
	//this.atributo accede a un atributo de esta clase
	//this.metodo() accede a un método de esta clase
	//this() accede a un constructor de esta clase
	/**
	 * 
	 * @param nombre
	 * @param apellidos
	 * @param fechaNacimiento
	 * @param esHombre
	 * @param altura
	 * @param peso
	 */
	public Persona(String nombre, String apellidos, String fechaNacimiento, boolean esHombre, double altura, double peso) {
		//llamo al constructor de la clase persona con 4 parámetros
		this(nombre,apellidos, fechaNacimiento, esHombre);
		this.altura=altura;
		this.peso=peso;
	}
	//setter y getter
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public boolean isEsHombre() {
		return esHombre;
	}
	public void setEsHombre(boolean esHombre) {
		this.esHombre = esHombre;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}


	
	
}
