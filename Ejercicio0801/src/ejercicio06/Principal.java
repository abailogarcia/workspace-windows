package ejercicio06;

public class Principal {

	public static void main(String[] args) {
		//sin lectura por teclado
		// crear persona con constructor con 2 par�metros
		//mostrar datos
		Persona persona1 = new Persona("Angel", "Bailo Garc�a");
		System.out.println("El nombre de la persona 1 es " + persona1.getNombre());
		System.out.println("Los apellidos de la persona 1 es " + persona1.getApellidos());
		
		
		Persona persona2 = new Persona("Mariano", "Lopez Navarro", ("2005-12-01"),true);
		System.out.println("El nombre de la persona 2 es " + persona2.getNombre());
		System.out.println("Los apellidos de la persona 2 es " + persona2.getApellidos());
		System.out.println("La fecha de nacimiento de la persona 2 es " +persona2.getFechaNacimiento());
		System.out.println("El sexo de la persona 2 es hombre? " + persona2.isEsHombre());
		
		Persona persona3 = new Persona("Elena", "Jaime Sanz", ("1965-06-25"),false, 1.65, 62);
		System.out.println("El nombre de la persona 3 es " + persona3.getNombre());
		System.out.println("Los apellidos de la persona 3 es " + persona3.getApellidos());
		System.out.println("La fecha de nacimiento de la persona 3 es " +persona3.getFechaNacimiento());
		System.out.println("El sexo de la persona 3 es hombre? " + persona3.isEsHombre());
		System.out.println("La altura de la persona 3 es  " + persona3.getAltura());
		System.out.println("El peso de la persona 3 es " + persona3.getPeso());
		
	}

}
