package romanos;

import java.util.Scanner;
import java.util.*;

//error 1 importar java.util.*
//error 2 definir 2 variables en la misma linea int unidades, decenas;
//error 3 variable con may�sculas 	int Centenas;
// error 4 en los switch falta el default
// error 5 en vez de usar distintos if mejor usar else if
// error 6 hay que definir las variables m�s cerca de donde se van a usar
//error 7 el nombre del escanner no usa lowerUpperCase
// error 8 el scanner no est� cerrado

public class Main {

	public static final String INICIO="Inicio del programa";
	
	public static void main(String args[]) {
		Scanner vamosaleer = new Scanner(System.in);
		int unidades, decenas;
		int Centenas;
		int millares;
		String uni = "";
		String dec = "";
		String cen = ""; 
		String mil = "";
		System.out.println("Programa que convierte un n�mero ar�bigo a romano (1 - 3999)");
		System.out.println("Dame un numero");
		int num = vamosaleer.nextInt();

		unidades = num % 10;
		decenas = (num / 10) % 10;
		Centenas = (num / 100) % 10;
		millares = (num / 1000) % 10;

		if (num > 0 && num < 4000) {
			if (unidades >= 0 && unidades < 10) 
			{
				switch (unidades) {
				case 0:
					uni = "";
					break;
				case 1:
					uni = "I";
					break;
				case 2:
					uni = "II";
					break;
				case 3:
					uni = "III";
					break;
				case 4:
					uni = "IV";
					break;
				case 5:
					uni = "V";
					break;
				case 6:
					uni = "VI";
					break;
				case 7:
					uni = "VII";
					break;
				case 8:
					uni = "VIII";
					break;
				case 9:
					uni = "IX";
					break;
				}
			}
			if (decenas >= 0 && decenas < 10) {
				switch (decenas) {
				case 0:
					dec = "";
					break;
				case 1:
					dec = "X";
					break;
				case 2:
					dec = "XX";
					break;
				case 3:
					dec = "XXX";
					break;
				case 4:
					dec = "XL";
					break;
				case 5:
					dec = "L";
					break;
				case 6:
					dec = "LX";
					break;
				case 7:
					dec = "LXX";
					break;
				case 8:
					dec = "LXXX";
					break;
				case 9:
					dec = "XC";
					break;
				}
			}
			if (Centenas >= 0 && Centenas < 10) {
				switch (Centenas) {
				case 0:
					cen = "";
					break;
				case 1:
					cen = "C";
					break;
				case 2:
					cen = "CC";
					break;
				case 3:
					cen = "CCC";
					break;
				case 4:
					cen = "CD";
					break;
				case 5:
					cen = "D";
					break;
				case 6:
					cen = "DC";
					break;
				case 7:
					cen = "DCC";
					break;
				case 8:
					cen = "DCCC";
					break;
				case 9:
					cen = "CM";
					break;
				}
			}
			if (millares >= 0 && millares < 4) {
				switch (millares) {
				case 0:
					mil = "";
					break;
				case 1:
					mil = "M";
					break;
				case 2:
					mil = "MM";
					break;
				case 3:
					mil = "MMM";
					break;
				}
				System.out.println("El n�mero romano es: " + mil + cen + dec + uni);
			}
		} else {
			System.out.println("N�mero inv�lido");
		}
	}
}
