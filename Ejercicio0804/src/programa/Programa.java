package programa;

import clases.Zoo;

public class Programa {

	public static void main(String[] args) {
		
		System.out.println("1,- Crear instancia de zoo con 4 animales");
		int numAnimales=4;
		Zoo miZoo = new Zoo(numAnimales);
		System.out.println("Instancia creada");
		
		System.out.println("2.- Dar de alta animales");
		miZoo.altaAnimal("Tim�n", 10, "Suricato", "zoo1");
		miZoo.altaAnimal("Pumba", 30, "fac�quero", "zoo1");
		miZoo.altaAnimal("Simba", 60, "le�n", "zoo1");
		miZoo.altaAnimal("Kowalski", 20, "Ping�ino", "zoo2");
		
		System.out.println("3.- Listar animales");
		miZoo.listarAnimales();
		
		System.out.println("4.- Buscar un animal");
		System.out.println(miZoo.buscarAnimal("Tim�n"));
		
		System.out.println("5.- Eliminar un animal");
		miZoo.eliminarAnimal("Pumba");
		miZoo.listarAnimales();
		
		System.out.println("6.- A�adimos un nuevo animal");
		miZoo.altaAnimal("Dori", 1, "Pez cirujano", "zoo2");
		miZoo.listarAnimales();
		
		System.out.println("7.- Cambiamos nombre animal");
		miZoo.cambiarNombreAnimal("Dori", "Hola soy Dori");
		miZoo.listarAnimales();
		
		System.out.println("8.- Listar animales de un zoo");
		miZoo.listarAnimales("zoo1");
		

	}

}
