package ejercicio03;

public class Principal {

	public static void main(String[] args) {
		//creamos profesores
		Profesor profesor1=new Profesor("Juan", "Fern�ndez", 33, "DAM");
		Profesor profesor2=new Profesor("Paco", "Garc�a", 33, "ASIR");
		Profesor profesor3=new Profesor("Elena", "S�nchez", 33, "DAW");
		Profesor profesor4=new Profesor("Salvador", "Calatayud", 33, "DAM");
		
		//muestro los valores
		System.out.println("Profesor1 ");
		System.out.println(profesor1.toString());
		System.out.println("Profesor2 ");
		System.out.println(profesor2.toString());
		System.out.println("Profesor3 ");
		System.out.println(profesor3.toString());
		System.out.println("Profesor4 ");
		
		//para llamar al m�todo toString de un String, no es necesario indicarlo
		//es lo mismo System.out.println(profesor4.toString()); que
		//System.out.println(profesor4);
		System.out.println(profesor4);
		
		System.out.println("Profesor1 ");
		profesor1.mostrarDatosProfesor(profesor1);
		System.out.println("Profesor2 ");
		profesor2.mostrarDatosProfesor(profesor2);
		System.out.println("Profesor3 ");
		profesor3.mostrarDatosProfesor(profesor3);
		System.out.println("Profesor4 ");
		profesor4.mostrarDatosProfesor(profesor4);
	}

}
