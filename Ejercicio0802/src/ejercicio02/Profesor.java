package ejercicio02;

public class Profesor {
	//atributos
	private String nombre;
	private String apellidos;
	private int edad;
	private String ciclo;
	
	
	//constructores
	public Profesor() {
			
		}
	public Profesor(String nombre, String apellidos, int edad, String ciclo) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.ciclo = ciclo;
	}
	//setter y getter
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	
	//cuando en un m�todo me indica delante @Override java me dice que el m�todo ya existe pero yo lo puedo sobreescribit
	//este m�todo me permite mostrar los atributos de una clase
	//autom�tico -> bot�n derecho -> generate toString
	//toString
	@Override
	public String toString() {
		return "Profesor [nombre=" + nombre + ", apellidos=" + apellidos + ", edad=" + edad + ", ciclo=" + ciclo + "]";
	}
	
	
	
	
	
}
