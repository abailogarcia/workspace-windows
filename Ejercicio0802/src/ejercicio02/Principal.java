package ejercicio02;

public class Principal {

	public static void main(String[] args) {
		//creamos profesores
		Profesor profesor1=new Profesor("Juan", "Fern�ndez", 33, "DAM");
		Profesor profesor2=new Profesor("Paco", "Garc�a", 33, "ASIR");
		Profesor profesor3=new Profesor("Elena", "S�nchez", 33, "DAW");
		Profesor profesor4=new Profesor("Salvador", "Calatayud", 33, "DAM");
		
		//muestro los valores
		System.out.println("Profesor1 ");
		System.out.println(profesor1.toString());
		System.out.println("Profesor2 ");
		System.out.println(profesor2.toString());
		System.out.println("Profesor3 ");
		System.out.println(profesor3.toString());
		System.out.println("Profesor4 ");
		System.out.println(profesor4.toString());

	}

}
