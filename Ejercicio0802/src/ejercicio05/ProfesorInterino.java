package ejercicio05;

public class ProfesorInterino extends Profesor {
	//atributos
	private double anyosInterino;
	//constructor

	public ProfesorInterino(String nombre, String apellidos, int edad, String ciclo, double anyosInterino) {
		super(nombre, apellidos, edad, ciclo);
		this.anyosInterino = anyosInterino;
	}
	public ProfesorInterino() {
		
	}
	//setter y getter
	public double getAnyosInterino() {
		return anyosInterino;
	}
	public void setAnyosInterino(double anyosInterino) {
		this.anyosInterino = anyosInterino;
	}
	//toString
	@Override
	public String toString() {
		return "ProfesorInterino  " +super.toString() +" anyosInterino=" + anyosInterino;
	}
	
	double obtenerSalarioBase () {
		double salarioBase= 1100 + this.anyosInterino*63.25;
		return salarioBase;
	}
	
}
