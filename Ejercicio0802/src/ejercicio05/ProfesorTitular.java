package ejercicio05;

public class ProfesorTitular extends Profesor {
	//atributos
	private double anyosCargo;
	//constructor
	public ProfesorTitular(double anyosCargo) {
		super();
		this.anyosCargo = anyosCargo;
	}
	public ProfesorTitular(String nombre, String apellidos, int edad, String ciclo, double anyosCargo) {
		super(nombre, apellidos, edad, ciclo);
		this.anyosCargo = anyosCargo;
	}

	//setter y getter
	public double getAnyosCargo() {
		return anyosCargo;
	}

	public void setAnyosCargo(double anyosCargo) {
		this.anyosCargo = anyosCargo;
	}
	
	//toString
	@Override
	public String toString() {
		return "ProfesorTitular  "+super.toString() + " " +"anyosCargo=" + anyosCargo + "";
	}
	//m�todos
	double obtenerSalarioBase () {
		double salarioBase= 1100 + this.anyosCargo*63.25;
		return salarioBase;
	}
	
	
	
	
	
}
