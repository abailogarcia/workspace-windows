package ejercicio05;

import ejercicio03.Profesor;

public class Principal {

	public static void main(String[] args) {
		
		//creamos profesores
				Profesor profesor1=new Profesor("Juan", "Fern�ndez", 33, "DAM");
				Profesor profesor2=new Profesor("Paco", "Garc�a", 33, "ASIR");
				Profesor profesor3=new Profesor("Elena", "S�nchez", 33, "DAW");
				Profesor profesor4=new Profesor("Salvador", "Calatayud", 33, "DAM");
				
				//muestro los valores
				System.out.println("Profesor1 ");
				System.out.println(profesor1.toString());
				System.out.println("Profesor2 ");
				System.out.println(profesor2.toString());
				System.out.println("Profesor3 ");
				System.out.println(profesor3.toString());
				System.out.println("Profesor4 ");
				
			//creamos profesor titular
				ProfesorTitular profesorTitular1 =new ProfesorTitular("Mar�a" , "Jim�nez", 45, "DAW" , 5);
				System.out.println("ProfesorTitular1");
				profesorTitular1.mostrarDatosProfesor(profesorTitular1);
				System.out.println();
				System.out.println( " y su salario es " + profesorTitular1.obtenerSalarioBase());
				System.out.println();
				
			//creamos profesor interino
				ProfesorInterino profesorInterino1 =new ProfesorInterino("Alejandro" , "Mart�n", 36, "DAM" , 3);
				System.out.println("ProfesorInterino1");
				profesorInterino1.mostrarDatosProfesor(profesorInterino1);
				System.out.println();
				System.out.println( " y su salario es " + profesorInterino1.obtenerSalarioBase());
				System.out.println();
	}

}
