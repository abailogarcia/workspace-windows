package ejercicio01;

public class Principal {

	public static void main(String[] args) {
		//creamos profesores
		Profesor profesor1=new Profesor("Juan", "Fern�ndez", 33, "DAM");
		Profesor profesor2=new Profesor("Paco", "Garc�a", 33, "ASIR");
		Profesor profesor3=new Profesor("Elena", "S�nchez", 33, "DAW");
		Profesor profesor4=new Profesor("Salvador", "Calatayud", 33, "DAM");
		
		//muestro los valores
		System.out.println("Profesor1");
		System.out.println("Nombre " + profesor1.getNombre());
		System.out.println("Apellidos" + profesor1.getApellidos());
		System.out.println("Edad " + profesor1.getEdad());
		System.out.println("Ciclo " + profesor1.getCiclo());
		System.out.println("Profesor2");
		System.out.println("Nombre " + profesor2.getNombre());
		System.out.println("Apellidos" + profesor2.getApellidos());
		System.out.println("Edad " + profesor2.getEdad());
		System.out.println("Ciclo " + profesor2.getCiclo());
		System.out.println("Profesor3");
		System.out.println("Nombre " + profesor3.getNombre());
		System.out.println("Apellidos" + profesor3.getApellidos());
		System.out.println("Edad " + profesor3.getEdad());
		System.out.println("Ciclo " + profesor3.getCiclo());
		System.out.println("Profesor4");
		System.out.println("Nombre " + profesor4.getNombre());
		System.out.println("Apellidos" + profesor4.getApellidos());
		System.out.println("Edad " + profesor4.getEdad());
		System.out.println("Ciclo " + profesor4.getCiclo());
		

	}

}
