package ejercicio090101contostring;

public class Vehiculo {
	private static int vehiculoCreado=0;
	//atributos
	private String tipo;
	private String marca;
	private float consumo;
	private float kmTotales;
	private int NumRuedas;
	private boolean funciona;
	//constructor sin parámetros
	public Vehiculo() {
		vehiculoCreado++;
	}
	
	//constructor con parámetros1
	public Vehiculo(String tipo, String marca) {
		this.tipo = tipo;
		this.marca = marca;
		this.kmTotales=0;
		this.funciona=true;
		vehiculoCreado++;
	}
//constructor con parámetros2
	public Vehiculo(String tipo, String marca, float consumo, int numRuedas) {
		this.tipo = tipo;
		this.marca = marca;
		this.consumo = consumo;
		this.NumRuedas = numRuedas;
		this.kmTotales=0;
		this.funciona=true;
		vehiculoCreado++;
	}
	//getter y setter

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public float getConsumo() {
		return consumo;
	}

	public void setConsumo(float consumo) {
		this.consumo = consumo;
	}

	public float getKmTotales() {
		return kmTotales;
	}

	public void setKmTotales(float kmTotales) {
		this.kmTotales = kmTotales;
	}

	public int getNumRuedas() {
		return NumRuedas;
	}

	public void setNumRuedas(int numRuedas) {
		NumRuedas = numRuedas;
	}

	public boolean isFunciona() {
		return funciona;
	}

	public void setFunciona(boolean funciona) {
		this.funciona = funciona;
	}
	
	public static int getVehiculoCreado() {
		return vehiculoCreado;
	}

	public static void setVehiculoCreado(int vehiculoCreado) {
		Vehiculo.vehiculoCreado = vehiculoCreado;
	}

	//combustibleConsumido()
	public float combustibleConsumido() {
		float consumido=this.kmTotales/100*this.consumo;
		return consumido;
	}
	public float combustibleConsumido(float kilometros) {
		float consumido=kilometros/100*this.consumo;
		return consumido;
	}
	
	// trucarCuentaKm()
public	void trucarCuentaKm(float kmTotales) {
		this.setKmTotales(kmTotales);
		//this.kmTotales=kmTotales;
	}
 public	void trucarCuentaKm() {
		this.setKmTotales(0);
		//this.kmTotales=0;
	}
//toString
@Override
public String toString() {
	return "Vehiculo [tipo=" + tipo + ", marca=" + marca + ", consumo=" + consumo + ", kmTotales=" + kmTotales
			+ ", NumRuedas=" + NumRuedas + ", funciona=" + funciona + "]";
}
	
}
