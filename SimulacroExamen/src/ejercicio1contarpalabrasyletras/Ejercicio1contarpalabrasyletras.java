package ejercicio1contarpalabrasyletras;

import java.util.Scanner;

public class Ejercicio1contarpalabrasyletras {
static Scanner input = new Scanner(System.in);
	public static void main(String[] args) {
		String[] matriz=rellenarVector(2);
		mostrarMatriz(matriz);
		cantidadPalabras(matriz);
		
		input.close();

	}
	public static String[] rellenarVector(int longitud) {
		String[] frases = new String[longitud];
		for (int i =0; i<longitud; i++) {
			System.out.println("Introduce una frase");
			frases[i]= input.nextLine();
		}
		return frases;
	}
	public static void mostrarMatriz(String[] matriz) {
		for (int i =0; i<matriz.length; i++) {
			System.out.println(matriz[i] + " ");
		}
	}
	//saca un vector de palabras de una frase
	public static String[] extraerPalabras(String[] frases, int componente) {
		//System.out.println("Introduce una componente");
		//int componente = input.nextInt();
		System.out.println("La frase de la componente " + componente + " es " + frases[componente]);
		int cantidadEspacios=0;
		
		for (int i =0; i<frases[componente].length() ; i++) {
			if (frases[componente].charAt(i)==' ') {
				cantidadEspacios++;
			}
		}
		String[] frase= new String[cantidadEspacios];
		for (int i=0; i<cantidadEspacios+1;i++) {
			frase = frases[componente].split(" ");
			//System.out.println(palabras[i]);
		}
		return frase;
	}
	//cuenta las palabras que hay en un vector
	public static void cantidadPalabras(String[] frases) {
		for (int i =0; i<frases.length;i++) {
			String[] texto=extraerPalabras(frases,i);
			System.out.println("La frase " +i + " es " + frases[i].toUpperCase());
			System.out.println("La frase  tiene " + texto.length + " palabras");
			for (int j=0; j<texto.length;j++) {
				System.out.println("La palabra " + texto[j] + " tiene " + texto[j].length()+ " letras");
				
			}
			}
		}
	//crea un vector con palabras SOBRA
	public static String[] sacarPalabras(String[] frases, int posicion) {
			String[] textos=extraerPalabras(frases,posicion);
			return textos;
		}
	//cuenta las letras de cada componente
		public static int extraerLetras(String[] frase, int posicion) {
			int cantidadLetrasPalabra=0;
			for (int i=0; i<frase.length;i++) {
				cantidadLetrasPalabra=frase[i].length();
			}
			return cantidadLetrasPalabra;
		}
	}

