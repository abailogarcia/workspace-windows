package ejercicio2visualizarmatriz;

public class Ejercicio2visualizarmatriz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	 int[][] matriz = {
			 			{1,2,3,4,5,6},{9,8,7,6,5,4}
	 			};
	 mostrarVector(matriz);
	 buscarMaximo(matriz);
	 buscarMinimo(matriz);
	 }
	public static void mostrarVector(int[][] matriz) {
		for(int i =0; i<matriz.length; i++) {
			for (int j=0; j< matriz[i].length; j++) {
			System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
		//System.out.println(" ");
	}
	public static void buscarMaximo(int[][] matriz) {
		int max=matriz[0][0];
		int maxi=0;
		int maxj=0;
		for(int i =0; i<matriz.length; i++) {
			for (int j=0; j< matriz[i].length; j++) {
				if (matriz[i][j]%2==0) {
						if(matriz[i][j] > max) {
						max = matriz[i][j];
						maxi=i;
						maxj=j;
						}
				}
			}
		}
		System.out.println("El m�ximo n�mero par es " + max );
		System.out.println("Su posici�n es la (" + maxi+ ","+ maxj+")");
	}
	public static void buscarMinimo(int[][] matriz) {
		int min=matriz[0][0];
		int mini=0;
		int minj=0;
		for(int i =0; i<matriz.length; i++) {
			for (int j=0; j< matriz[i].length; j++) {
				if (matriz[i][j]%2!=0) {
						if(matriz[i][j] < min) {
						min = matriz[i][j];
						mini=i;
						minj=j;
						}
				}
			}
		}
		System.out.println("El m�nimo n�mero impar es " + min );
		System.out.println("Su posici�n es la (" + mini+ ","+ minj+")");
	}
}