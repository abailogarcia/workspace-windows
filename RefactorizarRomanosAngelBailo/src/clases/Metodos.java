package clases;

public class Metodos {
	public static String calcularUnidadesRomano(int num) {
		String uni = "";
		int unidades = num % 10;
		if (unidades >= 0 && unidades < 10) {
			switch (unidades) {
			case 0:
				uni = "";
				break;
			case 1:
				uni = "I";
				break;
			case 2:
				uni = "II";
				break;
			case 3:
				uni = "III";
				break;
			case 4:
				uni = "IV";
				break;
			case 5:
				uni = "V";
				break;
			case 6:
				uni = "VI";
				break;
			case 7:
				uni = "VII";
				break;
			case 8:
				uni = "VIII";
				break;
			case 9:
				uni = "IX";
				break;
			default:
				System.out.println("Error de n�mero");
			}
		} else {
			return null;
		}
		return uni;
	}

	public static String calcularDecenasRomano(int num) {
		String dec = "";
		int decenas = (num / 10) % 10;
		if (decenas >= 0 && decenas < 10) {
			switch (decenas) {
			case 0:
				dec = "";
				break;
			case 1:
				dec = "X";
				break;
			case 2:
				dec = "XX";
				break;
			case 3:
				dec = "XXX";
				break;
			case 4:
				dec = "XL";
				break;
			case 5:
				dec = "L";
				break;
			case 6:
				dec = "LX";
				break;
			case 7:
				dec = "LXX";
				break;
			case 8:
				dec = "LXXX";
				break;
			case 9:
				dec = "XC";
				break;
			default:
				System.out.println("Error de n�mero");
			}
		} else {
			return null;
		}
		return dec;
	}

	public static String calcularCentenasRomano(int num) {
		String cen = "";
		int centenas = (num / 100) % 10;
		if (centenas >= 0 && centenas < 10) {
			switch (centenas) {
			case 0:
				cen = "";
				break;
			case 1:
				cen = "C";
				break;
			case 2:
				cen = "CC";
				break;
			case 3:
				cen = "CCC";
				break;
			case 4:
				cen = "CD";
				break;
			case 5:
				cen = "D";
				break;
			case 6:
				cen = "DC";
				break;
			case 7:
				cen = "DCC";
				break;
			case 8:
				cen = "DCCC";
				break;
			case 9:
				cen = "CM";
				break;
			default:
				System.out.println("Error de n�mero");
			}
		} else {
			return null;
		}
		return cen;
	}

	public static String calcularMillaresRomano(int num) {
		String mil = "";
		int millares = (num / 1000) % 10;
		if (millares >= 0 && millares < 4) {
			switch (millares) {
			case 0:
				mil = "";
				break;
			case 1:
				mil = "M";
				break;
			case 2:
				mil = "MM";
				break;
			case 3:
				mil = "MMM";
				break;
			default:
				System.out.println("Error de n�mero");
			}
		} else {
			return null;
		}
		return mil;
	}
}
