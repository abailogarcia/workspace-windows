package programa;
import clases.Metodos;
import java.util.Scanner;
//error 1 importar java.util.*
//error 2 definir 2 variables en la misma linea int unidades, decenas;
//error 3 variable con may�sculas 	int Centenas;
//error 4 en los switch falta el default
//error 5 en vez de usar distintos if mejor usar else if
//error 6 hay que definir las variables m�s cerca de donde se van a usar
//error 7 el nombre del escanner no usa lowerUpperCase
//error 8 el scanner no est� cerrado

public class Programa {

	public static void main(String[] args) {
		Scanner vamosALeer = new Scanner(System.in);
		System.out.println("Programa que convierte un n�mero ar�bigo a romano (1 - 3999)");
		System.out.println("Dame un numero");
		int num = vamosALeer.nextInt();
		
		System.out.println("El n�mero romano es: " + Metodos.calcularMillaresRomano(num)+ Metodos.calcularCentenasRomano(num) 
		+ Metodos.calcularDecenasRomano(num) + Metodos.calcularUnidadesRomano(num));
		vamosALeer.close();
	}

}
