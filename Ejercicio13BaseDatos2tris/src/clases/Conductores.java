package clases;

public class Conductores extends Personal {

	private static final long serialVersionUID = 1L;
	private String carnet;
	
	public Conductores(String dni, String nombre, String carnet) {
		super(dni, nombre);
		this.carnet = carnet;
	}

	public String getCarnet() {
		return carnet;
	}
	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}
	
	@Override
	public int compareTo(Personal o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String toString() {
		return "Conductores [carnet=" + carnet + ", toString()=" + super.toString() + "]";
	}
	
}
