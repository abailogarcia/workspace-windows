package clases;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class Transportes implements Comparable<Transportes>, Serializable {

	private static final long serialVersionUID = 1L;
	private String codigo;
	private String empresa;
	private LocalDate fechaTransporte;
	private Conductores conductor;
	private ArrayList<Carretilleros> listaCarretilleros;

	public Transportes(String codigo, String empresa, String fechaTransporte, Conductores conductor) {
		this.codigo = codigo;
		this.empresa = empresa;
		this.fechaTransporte = LocalDate.parse(fechaTransporte);
		this.conductor = conductor;
		this.listaCarretilleros = new ArrayList<Carretilleros>();
	}

	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public LocalDate getFechaTransporte() {
		return fechaTransporte;
	}

	public void setFechaTransporte(LocalDate fechaTransporte) {
		this.fechaTransporte = fechaTransporte;
	}

	public Conductores getConductor() {
		return conductor;
	}

	public void setConductor(Conductores conductor) {
		this.conductor = conductor;
	}

	public ArrayList<Carretilleros> getListaCarretilleros() {
		return listaCarretilleros;
	}

	public void setListaCarretilleros(ArrayList<Carretilleros> listaCarretilleros) {
		this.listaCarretilleros = listaCarretilleros;
	}

	@Override
	public int compareTo(Transportes o) {
		return this.getFechaTransporte().compareTo(o.getFechaTransporte());
	}
	@Override
	public String toString() {
		return "Transportes [codigo=" + codigo + ", empresa=" + empresa + ", fechaTransporte=" + fechaTransporte
				+ ", conductor=" + conductor + ", listaCarretilleros=" + listaCarretilleros + "]";
	}

}
