package clases;

public class Carretilleros extends Personal {

	private static final long serialVersionUID = 1L;
	
	private double costeHora;

	public Carretilleros(String dni, String nombre, double costeHora) {
		super(dni, nombre);
		this.costeHora = costeHora;
	}

	public double getCosteHora() {
		return costeHora;
	}

	public void setCosteHora(double costeHora) {
		this.costeHora = costeHora;
	}

	@Override
	public int compareTo(Personal arg0) {
		return this.getDni().compareTo(arg0.getDni());
	}
	@Override
	public String toString() {
		return "Carretilleros [costeHora=" + costeHora + ", toString()=" + super.toString() + "]";
	}

	
	
}
