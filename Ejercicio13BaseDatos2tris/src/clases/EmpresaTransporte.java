package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class EmpresaTransporte {
	private ArrayList<Transportes> listaTransportes;
	private ArrayList<Personal> listaPersonal;
	private Connection conexion;

	public EmpresaTransporte() {
		this.listaTransportes = new ArrayList<Transportes>();
		this.listaPersonal = new ArrayList<Personal>();
	}

	public ArrayList<Transportes> getListaTransportes() {
		return listaTransportes;
	}

	public void setListaTransportes(ArrayList<Transportes> listaTransportes) {
		this.listaTransportes = listaTransportes;
	}

	public ArrayList<Personal> getListaPersonal() {
		return listaPersonal;
	}

	public void setListaPersonal(ArrayList<Personal> listaPersonal) {
		this.listaPersonal = listaPersonal;
	}

	public void altaConductor(String dni, String nombre, String carnet) {
		listaPersonal.add(new Conductores(dni, nombre, carnet));
		Collections.sort(listaPersonal);
	}

	public void altaCarretillero(String dni, String nombre, double costeHora) {
		listaPersonal.add(new Carretilleros(dni, nombre, costeHora));
	}

	public Conductores buscaConductor(String dni) {
		for (Personal persona : listaPersonal) {
			if (persona instanceof Conductores && ((Conductores) persona).getDni().equals(dni)) {
				return (Conductores) persona;
			}
		}
		return null;
	}

	public Carretilleros buscaCarretillero(String dni) {
		for (Personal persona : listaPersonal) {
			if (persona instanceof Carretilleros && ((Carretilleros) persona).getDni().equals(dni)) {
				return (Carretilleros) persona;
			}
		}
		return null;
	}

	public Transportes buscaTransporte(String codigo) {
		for (Transportes transporte : listaTransportes) {
			if (transporte.getCodigo().equals(codigo)) {
				return transporte;
			}
		}
		return null;
	}

	public void altaTransporte(String codigo, String empresa, String fechaTransporte, String dni) {
		if (buscaConductor(dni) != null) {
			listaTransportes.add(new Transportes(codigo, empresa, fechaTransporte, buscaConductor(dni)));
			Collections.sort(listaTransportes);
		} else {
			System.out.println("No existe el conductor");
		}
	}
	
	public void registarCarretilleroTransporte(String codigo, String dni) {
		if(buscaTransporte(codigo)!=null) {
			if(buscaCarretillero(dni)!=null) {
				buscaTransporte(codigo).getListaCarretilleros().add(buscaCarretillero(dni));
			}else {
				System.out.println("El carretillero no existe");
			}
		}else {
			System.out.println("El transporte no existe");
		}
	}
	public void borraTransporte(String codigo) {
		Iterator<Transportes> iterador = listaTransportes.iterator();
		while(iterador.hasNext()) {
			Transportes transporte = (Transportes)iterador.next();
			if(transporte.getCodigo().equals(codigo)) {
				iterador.remove();
			}
		}
	}
	//guardar datos
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datos.dat")));
			escritor.writeObject(listaPersonal);
			escritor.writeObject(listaTransportes);
			escritor.close();
		}catch(IOException e) {
			System.out.println("Error de entrada salida");
		}
	}
	//cargar datos
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream escritor = new ObjectInputStream(new FileInputStream("src/datos.dat"));
			listaPersonal= (ArrayList<Personal>) escritor.readObject();
			listaTransportes= (ArrayList<Transportes>) escritor.readObject();
			escritor.close();
		}catch(IOException e) {
			System.out.println("Error de entrada salida");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	//conexi�n
	public void conectarBBDD() {
		String servidor = "jdbc:mysql://localhost:3306/transporte3ev";
		try {
			conexion = DriverManager.getConnection(servidor, "root", "");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//guardar conductores
	public void guardarConductoresBBDD(String carnet) {
		String query="INSERT INTO conductores(dni,nombre,carnet) VALUES(?,?,?)";
		try {
			PreparedStatement sentencia= conexion.prepareStatement(query);
			for(Personal persona:listaPersonal) {
				if(persona instanceof Conductores && ((Conductores)persona).getCarnet().equals(carnet)) {
					sentencia.setString(1, persona.getDni());
					sentencia.setString(2, persona.getNombre());
					sentencia.setString(3, ((Conductores)persona).getCarnet());
					sentencia.executeUpdate();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	//cargar conductores
	public void cargarConductoresBBDD() {
		String query = "SELECT * FROM conductores";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			ResultSet resultado= sentencia.executeQuery();
			while(resultado.next()) {
				listaPersonal.add(new Conductores(resultado.getString(2), resultado.getString(3), resultado.getString(4)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void listarConductores() {
		for(Personal persona:listaPersonal) {
			if(persona instanceof Conductores) {
				System.out.println(persona);
			}
		}
	}
	public void listarCarretilleros() {
		for(Personal persona:listaPersonal) {
			if(persona instanceof Carretilleros) {
				System.out.println(persona);
			}
		}
	}
}
