package programa;

import clases.Supermercado;

public class Programa {

	public static void main(String[] args) {
		Supermercado superm= new Supermercado();
		
		superm.altaCliente("Juan");
		superm.altaCliente("Pepe");
		superm.altaProducto("pan", 1.2, true);
		superm.altaProducto("leche", 1, false);
		superm.altaProducto("cafe", 3.5, true);
		superm.altaVentaCliente(1);
		superm.altaVentaCliente(2);
		superm.listarClientes();
		System.out.println();
		superm.listarVentas();
		System.out.println();
		superm.listarProductos();
		System.out.println(superm.buscarVenta(1));
		System.out.println(superm.buscarProducto("pan"));
		superm.insertarProductoVenta(1, "pan");
		superm.insertarProductoVenta(1, "leche");
		
		System.out.println();
		System.out.println("Guardando datos");
		superm.guardarDatos();
		System.out.println("Cargando datos");
		superm.cargarDatos();
	}

}
