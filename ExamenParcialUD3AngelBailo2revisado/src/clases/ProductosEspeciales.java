package clases;

import java.io.Serializable;

public class ProductosEspeciales extends Productos implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final int FRAGIL= 10;
	//si es fr�gil se incrementa el valor constante de 10;
	private boolean esFragil;
	
	public ProductosEspeciales(String nombreProducto, double precio, boolean esFragil) {
		super(nombreProducto, precio);
		this.esFragil = esFragil;
	}

	public boolean isEsFragil() {
		return esFragil;
	}

	public void setEsFragil(boolean esFragil) {
		this.esFragil = esFragil;
	}

	@Override
	public int compareTo(Productos o) {
		return getNombreProducto().compareTo(o.getNombreProducto());
	}

	@Override
	public double precioVenta() {
		double nuevoPrecio = getPrecio();
		if(esFragil) {
			nuevoPrecio += FRAGIL;
		} 
			return nuevoPrecio;
		
	}

	@Override
	public String toString() {
		return "ProductosEspeciales [esFragil=" + esFragil + ", toString()=" + super.toString() + "]";
	}
}
