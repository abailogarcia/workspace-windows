package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class Supermercado implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private ArrayList<Productos> listaProductos;
	private ArrayList<Clientes> listaClientes;
	private ArrayList<Ventas> listaVentas;
	public Supermercado() {
		listaProductos = new ArrayList<Productos>();
		listaClientes = new ArrayList<Clientes>();
		listaVentas = new ArrayList<Ventas>();
	}
	//public Supermercado() {
		
	//}
	//alta cliente
	public void altaCliente(String nombre) {
		listaClientes.add(new Clientes(listaClientes.size() +1, nombre, LocalDate.now()));
	}
	//alta productos sobrecargada
	public void altaProducto(String nombreProducto, double precio, int tipoOferta ) {
		listaProductos.add(new ProductosOferta(nombreProducto, precio, tipoOferta));
		Collections.sort(listaProductos);
	}
	public void altaProducto(String nombreProducto, double precio, boolean esFragil) {
		listaProductos.add(new ProductosEspeciales(nombreProducto, precio, esFragil));
		Collections.sort(listaProductos);
	}
	//crear una venta para un cliente
	public void altaVentaCliente(int codigoCliente) {
		if(buscarCliente(codigoCliente)!=null) {
			listaVentas.add(new Ventas(listaVentas.size()+1, LocalDate.now(), buscarCliente(codigoCliente)));
		}else {
			System.out.println("El cliente no existe");
		}
	}
	public Clientes buscarCliente(int codigoCliente) {
		for(Clientes cliente: listaClientes) {
			if(cliente.getCodigoCliente() == codigoCliente) {
				return cliente;
			}
		}
		return null;
	}
	
	public void insertarProductoVenta(int codigoVenta, String nombreProducto) {
		if(buscarVenta(codigoVenta)!=null) {
			if(buscarProducto(nombreProducto)!=null) {
				buscarVenta(codigoVenta).listaProductos.add(buscarProducto(nombreProducto));
				//buscarVenta(codigoVenta).getListaProductos().add(buscarProducto(nombreProducto));
			}else {
				System.out.println("No existe el producto");
			}
		}else {
			System.out.println("No existe la venta");
		}
	}
	public Ventas buscarVenta(int codigoVenta) {
		for(Ventas venta: listaVentas) {
			if(venta.getCodigoVenta() == codigoVenta) {
				return venta;
			}
		}
		return null;
	}
	public Productos buscarProducto(String nombreProducto) {
		for(Productos producto: listaProductos) {
			if(producto.getNombreProducto().equalsIgnoreCase(nombreProducto)) {
				return producto;
			}
		}
		return null;
	}
	/*
	public double precioTotal(int codigoVenta) {
			double sumaPrecio=0;
			for(Productos producto: buscarVenta(codigoVenta).listaProductos) {
				sumaPrecio+= (double)producto.precioVenta();
			}
		return sumaPrecio;
	}*/
	public void listaVentasCliente(int codigoCliente) {
		for(Ventas venta : listaVentas) {
			if(venta.getCliente().equals(buscarCliente(codigoCliente))) {
				System.out.println(venta);
			}
		}
		/*
		if(buscarCliente(codigoCliente)!=null) {
			for(Ventas venta : listaVentas) {
				System.out.print(venta);
				//System.out.println(precioTotal(venta.getCodigoVenta()));
			}
		} else {
			System.out.println("El cliente no existe");
		}*/
	}
	public void listarVentas() {
		for(Ventas venta : listaVentas) {
			System.out.print(venta);
			System.out.println();
			//System.out.println(precioTotal(venta.getCodigoVenta()));
		}
	}
	public void listarClientes() {
		for(Clientes cliente: listaClientes) {
			System.out.println(cliente);
		}
	}
	public void listarProductos() {
		for(Productos producto: listaProductos) {
			System.out.println(producto);
		}
	}
	
	//guardar datos
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new
			FileOutputStream(new File("src/datos.dat")));
			escritor.writeObject(listaClientes);
			escritor.writeObject(listaProductos);
			escritor.writeObject(listaVentas);
			escritor.close();
		}catch(IOException e) {
			System.out.println("Error de entrada/salida");
		}
	}
	//cargar datos
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		ObjectInputStream escritor;
		try {
			escritor = new ObjectInputStream(new 
			FileInputStream(new File("src/datos.dat")));
			listaClientes = (ArrayList<Clientes>) escritor.readObject();
			listaVentas = (ArrayList<Ventas>) escritor.readObject();
			listaProductos = (ArrayList<Productos>) escritor.readObject();
			escritor.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
