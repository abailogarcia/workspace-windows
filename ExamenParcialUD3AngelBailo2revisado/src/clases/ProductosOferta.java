package clases;

import java.io.Serializable;

public class ProductosOferta extends Productos implements Serializable{


	private static final long serialVersionUID = 1L;
	//el tipoOferta ser� 1=semanal 0=diaria
	private int tipoOferta;

	
	public ProductosOferta(String nombreProducto, double precio, int tipoOferta) {
		super(nombreProducto, precio);
		this.tipoOferta = tipoOferta;
	}

	

	public int getTipoOferta() {
		return tipoOferta;
	}



	public void setTipoOferta(int tipoOferta) {
		this.tipoOferta = tipoOferta;
	}



	@Override
	public int compareTo(Productos arg0) {
		return getNombreProducto().compareTo(arg0.getNombreProducto());
	}

	@Override
	public double precioVenta() {
		double nuevoPrecio = getPrecio();
		if(tipoOferta==1) {
			return nuevoPrecio = nuevoPrecio*0.9;
		}else {
			return nuevoPrecio = nuevoPrecio * 0.85;
		}
		}
		
	

	@Override
	public String toString() {
		return "ProductosOferta [tipoOferta=" + tipoOferta + ", toString()=" + super.toString() + "]";
	}
	

}
