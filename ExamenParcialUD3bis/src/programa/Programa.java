package programa;

import clases.Supermercado;

public class Programa {

	public static void main(String[] args) {
		Supermercado super1= new Supermercado();
		System.out.println("Alta clientes, articulos y creamos ventas");
		super1.altaCliente("Jesus");
		super1.altaCliente("Andres");
		super1.altaProducto("1111", 2.5, "semanal");
		super1.altaProducto("2222", 1.6, "dia");
		super1.altaProducto("3333", 15.5, true);
		super1.altaProducto("4444", 21.6, false);
		super1.crearVentaCliente(1);
		super1.crearVentaCliente(2);
		super1.crearVentaCliente(1);
		super1.introducirProductoVenta(1, "1111");
		super1.introducirProductoVenta(1, "2222");
		
		super1.introducirProductoVenta(2, "3333");
		super1.introducirProductoVenta(2, "4444");
	
		super1.introducirProductoVenta(3, "1111");
		super1.introducirProductoVenta(3, "3333");
		System.out.println();
		System.out.println("Guardando datos....");
		super1.guardarDatos();
		System.out.println("Cargando datos....");
		//super1.cargarDatos();

		System.out.println();
		System.out.println("Mostramos ventas clientes 1");
		super1.mostrarVentasCliente(1);
		System.out.println("Mostramos ventas clientes 2");
		super1.mostrarVentasCliente(2);
		System.out.println("Listamos ventas");
		super1.listarVentas();

	}

}
