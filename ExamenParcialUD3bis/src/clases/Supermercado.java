package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class Supermercado implements Serializable {

	private static final long serialVersionUID = 1L;

	private ArrayList<Productos> listaProductos;
	private ArrayList<Clientes> listaClientes;
	private ArrayList<Ventas> listaVentas;

	public Supermercado() {
		listaProductos = new ArrayList<Productos>();
		listaClientes = new ArrayList<Clientes>();
		listaVentas = new ArrayList<Ventas>();
	}

	public void altaCliente(String nombre) {
		listaClientes.add(new Clientes(listaClientes.size() + 1, nombre, LocalDate.now()));
	}

	public void altaProducto(String nombreProducto, double precio, String tipoOferta) {
		listaProductos.add(new ProductoOferta(nombreProducto, precio, tipoOferta));
		Collections.sort(listaProductos);
	}

	public void altaProducto(String nombreProducto, double precio, boolean fragil) {
		listaProductos.add(new ProductosEspeciales(nombreProducto, precio, fragil));
		Collections.sort(listaProductos);
	}

	public void crearVentaCliente(int codigoCliente) {
		if (buscarCliente(codigoCliente) != null) {
			listaVentas.add(new Ventas(listaVentas.size() + 1, LocalDate.now(), buscarCliente(codigoCliente)));
		} else {
			System.out.println("El cliente no existe");
		}
	}

	public Clientes buscarCliente(int codigoCliente) {
		for (Clientes cliente : listaClientes) {
			if (cliente != null && cliente.getCodigoCliente() == codigoCliente) {
				return cliente;
			}
		}
		return null;
	}

	public void introducirProductoVenta(int codigoVenta, String nombreProducto) {
		if (buscarVenta(codigoVenta) != null) {
			if (buscarProducto(nombreProducto) != null) {
				buscarVenta(codigoVenta).getListaProductos().add(buscarProducto(nombreProducto));
			} else {
				System.out.println("El producto no existe");
			}
		} else {
			System.out.println("La venta no existe");
		}
	}

	public Ventas buscarVenta(int codigoVenta) {
		for (Ventas venta : listaVentas) {
			if (venta != null && venta.getCodigoVenta() == codigoVenta) {
				return venta;
			}
		}
		return null;
	}

	public Productos buscarProducto(String nombreProducto) {
		for (Productos producto : listaProductos) {
			if (producto != null && producto.getNombreProducto().equals(nombreProducto)) {
				return producto;
			}
		}
		return null;
	}

	public void mostrarVentasCliente(int codigoCliente) {
		if (buscarCliente(codigoCliente) != null) {
			for (Ventas venta : listaVentas) {
				if (venta.getCliente().equals(buscarCliente(codigoCliente))) {
					System.out.print(venta + " ");
					System.out.println(venta.calcularPrecioTotal());
				}
			}
		} else {
			System.out.println("El cliente no existe");
		}
	}

	public void listarVentas() {
		for (Ventas venta : listaVentas) {
			System.out.print(venta);
			for (Productos producto : venta.getListaProductos()) {
				System.out.print(producto);
				System.out.println(producto.calcularPrecio());
			}
		}
	}

	// guardar datos
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datos.dat")));
			escritor.writeObject(listaClientes);
			escritor.writeObject(listaProductos);
			escritor.writeObject(listaVentas);
			escritor.close();

		} catch (IOException e) {
			System.out.println("Error de entrada salida");
		}
	}
	//cargar datos
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream lector = new ObjectInputStream(new FileInputStream("src/datos.dat"));
			listaVentas=(ArrayList<Ventas>) lector.readObject();
			listaProductos=(ArrayList<Productos>) lector.readObject();
			listaClientes=(ArrayList<Clientes>) lector.readObject();
		}	
		catch(IOException e) {
			System.out.println("Error de entrada salida");
		}
		catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	}
}
