package clases;

public class ProductoOferta extends Productos{

	private static final long serialVersionUID = 1L;
	
	private String tipoOferta;
	public static final double SEMANAL=0.9;
	public static final double DIARIO=0.85;
	public ProductoOferta(String nombreProducto, double precio, String tipoOferta) {
		super(nombreProducto, precio);
		this.tipoOferta = tipoOferta;
	}

	public String getTipoOferta() {
		return tipoOferta;
	}

	public void setTipoOferta(String tipoOferta) {
		this.tipoOferta = tipoOferta;
	}

	@Override
	public int compareTo(Productos o) {
		return getNombreProducto().compareTo(o.getNombreProducto());
	}

	@Override
	public double calcularPrecio() {
		double precioTotal=0;
		if(this.getTipoOferta().equals("semanal")) {
			precioTotal=this.getPrecio()*SEMANAL;

		}else if(this.getTipoOferta().equals("dia")) {
			precioTotal=this.getPrecio()*DIARIO;
		}
		return precioTotal;
	}

	@Override
	public String toString() {
		return "ProductoOferta [tipoOferta=" + tipoOferta + ", toString()=" + super.toString() + "]";
	}

		
}
