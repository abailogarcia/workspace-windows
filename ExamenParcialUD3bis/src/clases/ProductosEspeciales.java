package clases;

public class ProductosEspeciales extends Productos {

	private static final long serialVersionUID = 1L;

	private boolean fragil;
	public static final double FRAGIL = 10;

	public ProductosEspeciales(String nombreProducto, double precio, boolean fragil) {
		super(nombreProducto, precio);
		this.fragil = fragil;
	}

	public boolean isFragil() {
		return fragil;
	}

	public void setFragil(boolean fragil) {
		this.fragil = fragil;
	}

	@Override
	public int compareTo(Productos o) {
		return this.getNombreProducto().compareTo(o.getNombreProducto());
	}

	@Override
	public double calcularPrecio() {
		double precioTotal=0;
		if(fragil) {
			precioTotal= this.getPrecio()+FRAGIL;
		}
		return precioTotal;
	}

	@Override
	public String toString() {
		return "ProductosEspeciales [fragil=" + fragil + ", toString()=" + super.toString() + "]";
	}

}
