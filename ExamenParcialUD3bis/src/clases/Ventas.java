package clases;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class Ventas implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int codigoVenta;
	private LocalDate fechaVenta;
	private Clientes cliente;
	protected ArrayList<Productos> listaProductos;
	
	public Ventas(int codigoVenta, LocalDate fechaVenta, Clientes cliente) {
		super();
		this.codigoVenta = codigoVenta;
		this.fechaVenta = fechaVenta;
		this.cliente = cliente;
		listaProductos = new ArrayList<Productos>();
	}
	
	public int getCodigoVenta() {
		return codigoVenta;
	}

	public void setCodigoVenta(int codigoVenta) {
		this.codigoVenta = codigoVenta;
	}

	public LocalDate getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(LocalDate fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	public ArrayList<Productos> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(ArrayList<Productos> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public double calcularPrecioTotal() {
		double precioTotal=0;
		for(Productos producto: listaProductos) {
			precioTotal+=producto.calcularPrecio();
		}
		return precioTotal;
	}
	
	@Override
	public String toString() {
		return "Ventas [codigoVenta=" + codigoVenta + ", fechaVenta=" + fechaVenta + ", cliente=" + cliente
				+ ", listaProductos=" + listaProductos + "]";
	}

	
	
}
