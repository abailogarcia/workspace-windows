package clases;

public class Verduras {
	private String codigoVerdura;
	private String tipoVerdura;
	private double precio;
	private String fruteria;
	private Proveedor proveedor;
	//constructor
	public Verduras(String codigoVerdura) {
		this.codigoVerdura=codigoVerdura;
	}
	/**
	 * @return the codigoVerdura
	 */
	public String getCodigoVerdura() {
		return codigoVerdura;
	}
	/**
	 * @param codigoVerdura the codigoVerdura to set
	 */
	public void setCodigoVerdura(String codigoVerdura) {
		this.codigoVerdura = codigoVerdura;
	}
	/**
	 * @return the tipoVerdura
	 */
	public String getTipoVerdura() {
		return tipoVerdura;
	}
	/**
	 * @param tipoVerdura the tipoVerdura to set
	 */
	public void setTipoVerdura(String tipoVerdura) {
		this.tipoVerdura = tipoVerdura;
	}
	/**
	 * @return the precio
	 */
	public double getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	/**
	 * @return the fruteria
	 */
	public String getFruteria() {
		return fruteria;
	}
	/**
	 * @param fruteria the fruteria to set
	 */
	public void setFruteria(String fruteria) {
		this.fruteria = fruteria;
	}
	/**
	 * @return the proveedor
	 */
	public Proveedor getProveedor() {
		return proveedor;
	}
	/**
	 * @param proveedor the proveedor to set
	 */
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Verduras [codigoVerdura=" + codigoVerdura + ", tipoVerdura=" + tipoVerdura + ", precio=" + precio
				+ ", fruteria=" + fruteria + ", proveedor=" + proveedor + "]";
	}
	
}
