package clases;

public class Proveedor {
	//atributos
	private String nombreProveedor;
	private String telefono;
	
	/**
	 * @param nombreProveedor
	 */
	//constructor
	public Proveedor(String nombreProveedor) {
		this.nombreProveedor=nombreProveedor;
	}
	//setter y getter
	/**
	 * @return the nombreProveedor
	 */
	public String getNombreProveedor() {
		return nombreProveedor;
	}
	/**
	 * @param nombreProveedor the nombreProveedor to set
	 */
	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Proveedor [nombreProveedor=" + nombreProveedor + ", telefono=" + telefono + "]";
	}
	
}
