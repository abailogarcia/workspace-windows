package programa;

import java.util.Scanner;

import clases.Fruteria;
import clases.Proveedor;

public class programa {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Creamos instancia de fruteria con 4 frutas y 3 verduras");
		int maxFrutas=4;
		int maxVerduras=3;
		Fruteria miFruteria=new Fruteria(maxFrutas, maxVerduras);
		System.out.println("Instancia creada");
		System.out.println("Damos de alta 2 proveedores");
		Proveedor proveedor1=new Proveedor("Proveedor Fruta");
		System.out.println("Asigno tel�fono");
		proveedor1.setTelefono("9764646");
		Proveedor proveedor2=new Proveedor("Proveedor Verdura");
		System.out.println("Asigno tel�fono");
		proveedor1.setTelefono("976454544");
		
		System.out.println("Damos de alta 4 frutas y 3 verduras");
		miFruteria.altaFruta("NNNN", "Naranja", 1.55, "Fruteria1", proveedor1);
		miFruteria.altaFruta("CCCC", "Manzana", 1.20, "Fruteria1", proveedor1);
		miFruteria.altaFruta("FFFF", "Pera", 0.78, "Fruteria1", proveedor1);
		//miFruteria.altaFruta("RRRR", "Melocot�n", 1.60, "Fruteria2", proveedor1);
		miFruteria.altaVerdura("BBBB", "Brocoli", 0.2, "Fruteria1", proveedor2);
		miFruteria.altaVerdura("AAAA", "Alcachofas", 0.3, "Fruteria1", proveedor2);
		//miFruteria.altaVerdura("BRBR", "Borraja", 0.4, "Fruteria1", proveedor2);
		//miFruteria.altaFruta("123", "tropical", 2, "Fruteria 1", proveedor1);
		
		System.out.println("Listamos frutas");
		miFruteria.listarFrutas();
		System.out.println("Listamos verduras");
		miFruteria.listarVerduras();
		boolean salir =false;
		int opcion=0;
		while(!salir) {
			System.out.println("1.-Crear fruta");
			System.out.println("2.-Crear verdura");
			System.out.println("3.-Listar frutas");
			System.out.println("4.-Listar verduras");
			System.out.println("5.-Buscar frutas");
			System.out.println("6.-Buscar verduras");
			System.out.println("7.-Eliminar fruta");
			System.out.println("8.-Eliminar verdura");
			System.out.println("9.-Modificar el tipo de fruta");
			System.out.println("10.-Modificar el tipo de verdura");
			System.out.println("11.-Listar frutas por fruter�a");
			System.out.println("12.-Listar verduras por fruter�a");
			System.out.println("13.-Listar frutas por proveedor");
			System.out.println("14.-Listar verduras por proveedor");
			System.out.println("15.- Salir");
			System.out.println("Escoge una opci�n");
			opcion=input.nextInt();
			input.nextLine();
			switch(opcion) {
			case 1:	
				System.out.println("Introduce los datos de una nueva fruta");
				System.out.println("Introduce el c�digo");
				String nuevoCodigo=input.nextLine();
				System.out.println("Introduce el tipo");
				String nuevoTipo=input.nextLine();
				System.out.println("Introduce el precio");
				double nuevoPrecio=input.nextDouble();
				input.nextLine();
				System.out.println("Introduce la fruteria");
				String nuevaFruteria=input.nextLine();
				//System.out.println("Introduce el proveedor");
				//Proveedor nuevoProveedor=input.nextLine();
				miFruteria.altaFruta(nuevoCodigo, nuevoTipo, nuevoPrecio, nuevaFruteria, proveedor1);		
				System.out.println("Se ha creado la fruta");
				miFruteria.listarFrutasPorCodigoFruta(nuevoCodigo);
				break;
			case 2:
				System.out.println("Introduce los datos de una nueva verdura");
				System.out.println("Introduce el c�digo");
				String nuevoCodigo2=input.nextLine();
				System.out.println("Introduce el tipo");
				String nuevoTipo2=input.nextLine();
				System.out.println("Introduce el precio");
				double nuevoPrecio2=input.nextDouble();
				input.nextLine();
				System.out.println("Introduce la fruteria");
				String nuevaFruteria2=input.nextLine();
				//System.out.println("Introduce el proveedor");
				//Proveedor nuevoProveedor=input.nextLine();
				miFruteria.altaVerdura(nuevoCodigo2, nuevoTipo2, nuevoPrecio2, nuevaFruteria2, proveedor1);		
				System.out.println("Se ha creado la verdura");
				miFruteria.listarVerdurasPorCodigoVerdura(nuevoCodigo2);
				break;
			case 3:
				System.out.println("Lista de Frutas");
				miFruteria.listarFrutas();
				break;
			case 4:
				System.out.println("Lista de Verduras");
				miFruteria.listarVerduras();
				break;
			case 5:
				System.out.println("Introduce el c�digo de fruta a buscar");
				String codigoBuscar=input.nextLine();
				System.out.println(miFruteria.buscarFruta(codigoBuscar));
				//miFruteria.listarFrutasPorCodigoFruta(miFruteria.buscarFruta(codigoBuscar));
				break;
			case 6:
				System.out.println("Introduce el c�digo de verdura a buscar");
				String codigoBuscar2=input.nextLine();
				System.out.println(miFruteria.buscarVerdura(codigoBuscar2));
				break;
			case 7:
				System.out.println("Introduce el c�digo de fruta a eliminar");
				String codigoEliminaFruta=input.nextLine();
				miFruteria.eliminarFruta(codigoEliminaFruta);
				//System.out.println("Se elimin� la fruta " + codigoEliminaFruta);
				break;
			case 8:
				System.out.println("Introduce el c�digo de verdura a eliminar");
				String codigoEliminaVerdura=input.nextLine();
				miFruteria.eliminarVerdura(codigoEliminaVerdura);
				break;
			case 9:
				System.out.println("Introduce el c�digo  de fruta");
				String codigoFrutaCambiar=input.nextLine();
				System.out.println("Introduce el nuevo tipo de fruta");
				String tipoFrutaNuevo=input.nextLine();
				miFruteria.cambiarTipoFruta(codigoFrutaCambiar, tipoFrutaNuevo);
				miFruteria.listarFrutasPorCodigoFruta(codigoFrutaCambiar);
				break;
			case 10:
				System.out.println("Introduce el c�digo  de verdura");
				String codigoVerduraCambiar=input.nextLine();
				System.out.println("Introduce el nuevo tipo de verdura");
				String tipoVerduraNuevo=input.nextLine();
				miFruteria.cambiarTipoVerdura(codigoVerduraCambiar, tipoVerduraNuevo);
				miFruteria.listarVerdurasPorCodigoVerdura(codigoVerduraCambiar);
				break;
			case 11:
				System.out.println("Introduce el c�digo de fruteria para listar sus frutas");
				String codigoFruteria =input.nextLine();
				miFruteria.listarFrutasPorFruteria(codigoFruteria);
				break;
			case 12:
				System.out.println("Introduce el c�digo de fruteria para listar sus verduras");
				String codigoFruteria2 =input.nextLine();
				miFruteria.listarVerdurasPorFruteria(codigoFruteria2);
				break;
			case 13:
				System.out.println("Introduce el c�digo de proveedor para listar sus frutas");
				String codigoProveedor =input.nextLine();
				miFruteria.listarFrutasPorProveedor(codigoProveedor);
				break;
			case 14:
				System.out.println("Introduce el c�digo de proveedor para listar sus verduras");
				String codigoProveedor2 =input.nextLine();
				miFruteria.listarVerdurasPorProveedor(codigoProveedor2);
				break;
			case 15:
				salir=true;
				System.out.println("Se acab�");
				break;
			default:
				System.out.println("Introduce un n�mero entre 1 y 15");
			}
		}
		input.close();
	}

}
