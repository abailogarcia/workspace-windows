package trivial;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// Juego de trivial
		Scanner input = new Scanner(System.in);
		// creo los vectores de preguntas
		// preguntas cultura general
		String pregunta0101 = "�En qu� a�o lleg� el hombre a la Luna?";
		String pregunta0102 = "1969";
		String pregunta0103 = "2001";
		String pregunta0104 = "No ha llegado, la Tierra es plana";
		String pregunta0105 = "1";
		String pregunta0201 = "�Qui�n fue el primer presidente de la democracia espa�ola despu�s del franquismo?";
		String pregunta0202 = "Felipe Gonz�lez";
		String pregunta0203 = "Adolfo Su�rez";
		String pregunta0204 = "Mariano Rajoy";
		String pregunta0205 = "2";
		String pregunta0301 = " �Qu� presidente de la Uni�n Sovi�tica instaur� la Perestroika?";
		String pregunta0302 = "Stalin";
		String pregunta0303 = "Molotov";
		String pregunta0304 = "Gorbachov";
		String pregunta0305 = "3";
		String pregunta0401 = "�En qu� pa�s se encuentra el pico Aconcagua?";
		String pregunta0402 = "Per�";
		String pregunta0403 = "Argentina";
		String pregunta0404 = "Bolivia";
		String pregunta0405 = "2";
		String pregunta0501 = "�En qu� pa�s de Europa se habla el magyar?";
		String pregunta0502 = "Rusia";
		String pregunta0503 = "Eslovenia";
		String pregunta0504 = "Hungr�a";
		String pregunta0505 = "3";
		// Preguntas de arte
		String pregunta0601 = "�Qui�n pint� el Guernica?";
		String pregunta0602 = "Van Gogh";
		String pregunta0603 = "Picasso";
		String pregunta0604 = "Goya";
		String pregunta0605 = "2";
		String pregunta0701 = "�Qui�n escribi� Poeta en Nueva York?";
		String pregunta0702 = "Federico Garc�a Lorca";
		String pregunta0703 = "Rafael Alberti";
		String pregunta0704 = "D�maso Alonso";
		String pregunta0705 = "1";
		String pregunta0801 = "�Qu� pintor pint� El grito?";
		String pregunta0802 = "Van Gogh";
		String pregunta0803 = "Kandinski";
		String pregunta0804 = "Munch";
		String pregunta0805 = "3";
		String pregunta0901 = "�Qu� tipo de instrumento es una c�tara?";
		String pregunta0902 = "Viento";
		String pregunta0903 = "Percusi�n";
		String pregunta0904 = "Cuerda";
		String pregunta0905 = "3";
		String pregunta1001 = "�Qui�n escribi� La colmena?";
		String pregunta1002 = "Camilo Jos� Cela";
		String pregunta1003 = "Arturo P�rez-Reverte";
		String pregunta1004 = "Eduardo Mendoza";
		String pregunta1005 = "1";
		// Preguntas de m�sica
		String pregunta1101 = "�Qu� banda form� el m�sico ingl�s Jimmy Page en 1968?";
		String pregunta1102 = "Rolling Stones";
		String pregunta1103 = "Led Zeppelin";
		String pregunta1104 = "Los Ronaldos";
		String pregunta1105 = "2";
		String pregunta1201 = "�C�mo se llama el l�der de la banda Radio Futura?";
		String pregunta1202 = "Santiago Auser�n";
		String pregunta1203 = "Paco de Luc�a";
		String pregunta1204 = "Antonio Molina";
		String pregunta1205 = "1";
		String pregunta1301 = "�A qu� grupo espa�ol le dol�a la cara de ser tan guapo?";
		String pregunta1302 = "Los Ilegales";
		String pregunta1303 = "Los Inhumanos";
		String pregunta1304 = "Los Inmorales";
		String pregunta1305 = "2";
		String pregunta1401 = "Jos� Monje Cruz fue un cantaor flamenco m�s conocido como...";
		String pregunta1402 = "Tomatito";
		String pregunta1403 = "Caracol";
		String pregunta1404 = "Camar�n";
		String pregunta1405 = "3";
		String pregunta1501 = "Janis Joplin, Jimi Hendrix, Jim Morrison y Kurt Cobain ten�an todos la misma edad cuando murieron, una cifra gafe entre los m�sicos, �cu�l? ";
		String pregunta1502 = "27";
		String pregunta1503 = "47";
		String pregunta1504 = "32";
		String pregunta1505 = "1";
		// creo los vectores
		String[][] preguntasGeneral = { { pregunta0101, pregunta0102, pregunta0103, pregunta0104, pregunta0105 },
				{ pregunta0201, pregunta0202, pregunta0203, pregunta0204, pregunta0205 },
				{ pregunta0301, pregunta0302, pregunta0303, pregunta0304, pregunta0305 },
				{ pregunta0401, pregunta0402, pregunta0403, pregunta0404, pregunta0405 },
				{ pregunta0501, pregunta0502, pregunta0503, pregunta0504, pregunta0505 }, };
		String[][] preguntasArte = { { pregunta0601, pregunta0602, pregunta0603, pregunta0604, pregunta0605 },
				{ pregunta0701, pregunta0702, pregunta0703, pregunta0704, pregunta0705 },
				{ pregunta0801, pregunta0802, pregunta0803, pregunta0804, pregunta0805 },
				{ pregunta0901, pregunta0902, pregunta0903, pregunta0904, pregunta0905 },
				{ pregunta1001, pregunta1002, pregunta1003, pregunta1004, pregunta1005 }, };
		String[][] preguntasMusica = { { pregunta1101, pregunta1102, pregunta1103, pregunta1104, pregunta1105 },
				{ pregunta1201, pregunta1202, pregunta1203, pregunta1204, pregunta1205 },
				{ pregunta1301, pregunta1302, pregunta1303, pregunta1304, pregunta1305 },
				{ pregunta1401, pregunta1402, pregunta1403, pregunta1404, pregunta1405 },
				{ pregunta1501, pregunta1502, pregunta1503, pregunta1504, pregunta1505 }, };
		// System.out.println(preguntasMusica[0][0]);
		int opcion = 0;
		boolean salir = false;
		int puntuacionGlobal = 0;
		while (!salir) {
			System.out.println("Comenzamos el juego...");
			System.out.println("Selecciona una de las siguientes categor�as");
			System.out.println("1.- General");
			System.out.println("2.- Arte");
			System.out.println("3.- M�sica");
			System.out.println("4.- Salir");
			opcion = input.nextInt();
			input.nextLine();
			switch (opcion) {
			case 1:
				System.out.println("Has elegido General. Tienes que responder a las siguientes preguntas");
				String eleccion;
				int puntuacionGeneral=0;
				int aleatorioGeneral = (int) Math.round((Math.random() * (5 - 1) + 1));
				for (int i = 0; i < 5; i++) {
					System.out.println(preguntasGeneral[i][0]);
					System.out.println("Las respuestas son: ");
					System.out.println("1.- " + preguntasGeneral[i][1]);
					System.out.println("2.- " + preguntasGeneral[i][2]);
					System.out.println("3.- " + preguntasGeneral[i][3]);
					eleccion = input.nextLine();
					if(Integer.parseInt(eleccion)>=1 && Integer.parseInt(eleccion)<=3) {
						if (eleccion.equals(preguntasGeneral[i][4])) {
							System.out.println("Bravo, has acertado. Siguiente pregunta");
							puntuacionGeneral++;
						} else {
							System.out.println("Has fallado, siguiente pregunta");
						}
					}else {
						System.out.println("Tienes que introducir un n�mero entre 1 y 3");
						i--;
					}
				}
				System.out.println("Tu puntuaci�n en este apartado ha sido " + puntuacionGeneral);
				puntuacionGlobal+=puntuacionGeneral;
				System.out.println("Tu puntuaci�n global es " + puntuacionGlobal);
				break;
			case 2:
				System.out.println("Has elegido Arte. Tienes que responder a las siguientes preguntas");
				int puntuacionArte=0;
				aleatorioGeneral = (int) Math.round((Math.random() * (5 - 1) + 1));
				for (int i = 0; i < 5; i++) {
					System.out.println(preguntasArte[i][0]);
					System.out.println("Las respuestas son: ");
					System.out.println("1.- " + preguntasArte[i][1]);
					System.out.println("2.- " + preguntasArte[i][2]);
					System.out.println("3.- " + preguntasArte[i][3]);
					eleccion = input.nextLine();
					if(Integer.parseInt(eleccion)>=1 && Integer.parseInt(eleccion)<=3) {
						if (eleccion.equals(preguntasArte[i][4])) {
							System.out.println("Bravo, has acertado. Siguiente pregunta");
							puntuacionArte++;
						} else {
							System.out.println("Has fallado, siguiente pregunta");
						}
					}else {
						System.out.println("Tienes que introducir un n�mero entre 1 y 3");
						i--;
					}
				}
				System.out.println("Tu puntuaci�n en este apartado ha sido " + puntuacionArte);
				puntuacionGlobal+=puntuacionArte;
				System.out.println("Tu puntuaci�n global es " + puntuacionGlobal);
				break;
			case 3:
				System.out.println("Has elegido M�sica. Tienes que responder a las siguientes preguntas");
				int puntuacionMusica=0;
				aleatorioGeneral = (int) Math.round((Math.random() * (5 - 1) + 1));
				for (int i = 0; i < 5; i++) {
					System.out.println(preguntasMusica[i][0]);
					System.out.println("Las respuestas son: ");
					System.out.println("1.- " + preguntasMusica[i][1]);
					System.out.println("2.- " + preguntasMusica[i][2]);
					System.out.println("3.- " + preguntasMusica[i][3]);
					eleccion = input.nextLine();
					if(Integer.parseInt(eleccion)>=1 && Integer.parseInt(eleccion)<=3) {
						if (eleccion.equals(preguntasMusica[i][4])) {
							System.out.println("Bravo, has acertado. Siguiente pregunta");
							puntuacionMusica++;
						} else {
							System.out.println("Has fallado, siguiente pregunta");
						}
					}else {
						System.out.println("Tienes que introducir un n�mero entre 1 y 3");
						i--;
					}
				}
				System.out.println("Tu puntuaci�n en este apartado ha sido " + puntuacionMusica);
				puntuacionGlobal+=puntuacionMusica;
				System.out.println("Tu puntuaci�n global es " + puntuacionGlobal);
				break;
			case 4:
				System.out.println("Se acab�");
				salir = true;
				break;
			default:
				System.out.println("Introduce un n�mero entre 1 y 4");
				break;
			}
		}
		input.close();
	}

}
